package it.unibo.bitpantarei;

import alice.tucson.network.exceptions.DialogInitializationException;
import alice.tucson.service.TucsonNodeService;

import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

import static alice.tucson.service.TucsonNodeService.*;
import static java.lang.System.*;
import static java.lang.System.out;
import static java.lang.Thread.*;
import static java.util.stream.Collectors.toList;

/**
 * Created by giova on 07/10/2015.
 */
public class NNodesStarter {

    public static final int DEF_PORT = 20504;
    public static final int DEF_TIMEOUT = 5000;
    public static boolean asPorts = false;

    public static void main(String[] args) throws Exception {
        int nNodes;
        if (args.length > 0) {
            try {
                if (args.length == 1) {
                    nNodes = Integer.parseInt(args[0]);
                    asPorts = nNodes >= 1024;
                } else {
                    nNodes = args.length;
                    asPorts = true;
                }
            } catch (NumberFormatException e) {
                nNodes = 0;
                e.printStackTrace();
            }
        } else {
            nNodes = 1;
        }

        out.println(String.format("Starting %d nodes", nNodes));

        final IntStream stream;
        if (asPorts) {
            stream = Arrays.stream(args)
                    .mapToInt(Integer::parseInt);
        } else {
            stream = IntStream.range(0, nNodes);
        }

        final List<TusconNodeThread> toJoin = stream
                .map(i -> i + (asPorts ? 0 : DEF_PORT))
                .mapToObj(TusconNodeThread::new)
                .peek(Thread::start)
                .collect(toList());

        long now = currentTimeMillis();

        toJoin.forEach(it -> {
            try {
                isInstalled(it.port, DEF_TIMEOUT);

            } catch (Exception e) {
                e.printStackTrace();
            }

        });

        long elapsed = currentTimeMillis() - now;

        if (elapsed < DEF_TIMEOUT) {
            sleep(DEF_TIMEOUT - elapsed);
        }

        out.println(String.format("Done", nNodes));
    }

    private static class TusconNodeThread extends Thread {
        final int port;
        final TucsonNodeService service;

        public TusconNodeThread(int port) {
            super(String.format("Node[localhost:%d]", port));
            this.port = port;
            this.service = new TucsonNodeService(port);
        }

        @Override
        public void run(){
           try {
               service.install();
           } catch (Exception e) {
               e.printStackTrace();
               System.exit(1);
           }
        }
    }
}
