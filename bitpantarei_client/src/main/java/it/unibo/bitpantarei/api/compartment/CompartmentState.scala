package it.unibo.bitpantarei.api.compartment

import it.unibo.bitpantarei.api.topic.Topic

import scala.concurrent.duration.Duration

/**
 * Created by giova on 18/10/2015.
 */
trait CompartmentState {
  def period: Duration

  def topic: Topic

  def evaporationProb: Double

  def lastTick: (Long, Duration)

  def initialConcentration: Int

  override def toString =
    s"state(period=$period, " +
      s"initialConcentration=$initialConcentration, " +
      s"evaporationProb=$evaporationProb, " +
      s"lastTick=$lastTick)"

}
