package it.unibo.bitpantarei.api.compartment.impl

import it.unibo.bitpantarei.api.compartment.CompartmentState
import it.unibo.bitpantarei.api.topic.{NoTopic, Topic}

import scala.concurrent.duration.Duration

/**
 * Created by giova on 18/10/2015.
 */
class CompartmentStateImpl(
                            var mPeriod: Duration,
                            var mInitialConcentration: Int,
                            var mEvaporationProb: Double,
                            var mLastTick: (Long, Duration)
                            ) extends CompartmentState {

  var mTopic: Topic = NoTopic

  def this() =
    this(
      Duration.fromNanos(0l),
      0,
      0d,
      (0, Duration.fromNanos(0l))
    )

  override def period: Duration = mPeriod

  def period_=(dur: Duration) = mPeriod = dur

  override def initialConcentration: Int = mInitialConcentration

  def initialConcentration_=(init: Int) = mInitialConcentration = init

  override def evaporationProb: Double = mEvaporationProb

  def evaporationProb_=(ep: Double) = mEvaporationProb = ep

  override def lastTick: (Long, Duration) = mLastTick

  def lastTick_=(lt: (Long, Duration)) = mLastTick = lt

  override def topic: Topic = mTopic

  def topic_=(t: Topic) = mTopic = t
}
