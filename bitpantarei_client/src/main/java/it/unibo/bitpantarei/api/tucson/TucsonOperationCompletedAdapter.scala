package it.unibo.bitpantarei.api.tucson

import alice.tucson.api.{ITucsonOperation, TucsonOperationCompletionListener}
import alice.tuplecentre.core.AbstractTupleCentreOperation

/**
 * Created by giova on 01/10/2015.
 */
class TucsonOperationCompletedAdapter(f: ITucsonOperation => Any) extends TucsonOperationCompletionListener {

  override def operationCompleted(op: AbstractTupleCentreOperation): Unit =
    operationCompleted(op.asInstanceOf[ITucsonOperation])

  override def operationCompleted(op: ITucsonOperation): Unit =
    f(op)
}

object TucsonOperationCompletedAdapter {

  def apply(f: ITucsonOperation => Any) =
    new TucsonOperationCompletedAdapter(f)
}
