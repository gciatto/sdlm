package it.unibo.bitpantarei.api.service.impl

import java.util.concurrent.Executors

import alice.logictuple.LogicTuple
import alice.tucson.api.{EnhancedACC, ITucsonOperation, TucsonMetaACC, TucsonOperationCompletionListener}
import alice.tucson.service.TucsonNodeService
import it.unibo.bitpantarei.api.Conversions._
import it.unibo.bitpantarei.api.compartment.Compartment
import it.unibo.bitpantarei.api.id.impl.CompartmentIdImpl
import it.unibo.bitpantarei.api.id.{CompartmentId, ServiceId}
import it.unibo.bitpantarei.api.molecule.Molecule
import it.unibo.bitpantarei.api.service.ServiceApi
import it.unibo.bitpantarei.api.service.ServiceApi.DEFAULT_TIMEOUT
import it.unibo.bitpantarei.api.topic.{NoTopic, Topic}
import it.unibo.bitpantarei.api.tucson.Tucson
import it.unibo.bitpantarei.api.tucson.Tucson.{COMPARTMENT_SPEC_STRING, DESTROY}

import scala.collection.JavaConversions.asScalaBuffer
import scala.collection.parallel.mutable.ParHashMap
import scala.concurrent.ExecutionContext
import scala.concurrent.duration.Duration
import scala.util.{Failure, Success, Try}

/**
  * Created by giova on 17/10/2015.
  */
class ServiceApiImpl(mId: ServiceId) extends ServiceApi {
  val mEventLoop = ExecutionContext.fromExecutorService(Executors.newSingleThreadExecutor())
  val mDefaultCompartmentId = CompartmentId(mId.address, mId.port)
  val mInspected = new ParHashMap[CompartmentId, CompartmentInspector]()
  var mTucsonService: ScalaTucsonService = null
  var mContext: EnhancedACC = null
  var mContext2: EnhancedACC = null

  def this() = this(ServiceId())

  override def createCompartment(name: String, f: Callback[CompartmentId]) = {
    async(() => {
      val tid = CompartmentId(name, id)

      mContext.setS(
        tid,
        COMPARTMENT_SPEC_STRING,
        (op: ITucsonOperation) => {
          async(() => {
            mContext.out(
              tid,
              Tucson.CONFIG,
              response((op: ITucsonOperation) => {
                f(Success(tid))
              })
            )
          })
        }
      )
    })
  }

  def response(f: (ITucsonOperation) => Any): TucsonOperationCompletionListener =
    toTucsonOperationCompletedAdapter(op => {
      async(() => {
        f(op)
      })
    })

  override def stop(f: Callback[ServiceId]) = {
    async(() => {
      if (isInternal) {
        mTucsonService.shutdown()
        mTucsonService = null
      }
      mContext = null
      f(Success(id))
    })
    mEventLoop.shutdown()
  }

  protected def isInternal: Boolean =
    mTucsonService != null

  override def compartments(f: Callback[(ServiceId, List[CompartmentId])]) = {
    async(() => {
      mContext.rdAll(
        CompartmentId(id),
        Tucson.NEIGHBOR_TEMPLATE,
        (op: ITucsonOperation) => {
          f(Try(
            (id, op.getLogicTupleListResult.map(CompartmentId(_)).toList)
          ))
        }
      )
    })
  }

  override def checkInstalled(f: Callback[Boolean]) = {
    async(() => {
      f(Try(TucsonNodeService.isInstalled(
        id.address,
        id.port,
        DEFAULT_TIMEOUT)
      ))
    })
  }

  override def destroyCompartment(name: String, f: Callback[CompartmentId]) = {
    async(() => {
      val tid = CompartmentId(name, id.address, id.port)

      mContext.out(
        tid,
        DESTROY,
        (op: ITucsonOperation) => f(Success(tid))
      )
    })
  }

  override def setCompartmentTopic(name: String, topic: Topic, f: Callback[(CompartmentId, Topic)]): Unit = {
    async(() => {
      val tid = CompartmentId(name, id.address, id.port)

      topic match {
        case NoTopic =>
          //TODO hanle this
          ???

        case _ =>
          mContext.out(
            tid,
            new LogicTuple(Tucson.TOPIC, topic.toTupleArg),
            (op: ITucsonOperation) => f(Success((tid, topic)))
          )
      }
    })
  }

  override def addMolecule(name: String, molecule: Molecule, f: Callback[(CompartmentId, Molecule)]): Unit = {
    async(() => {
      val tid = CompartmentId(name, id.address, id.port)
      var count = molecule.concentration
      var success = true

      val f1: Callback[(CompartmentId, Molecule)] =
        it => {
          count -= 1
          it match {
            case s: Success[_] =>
              if (count == 0 && success) {
                f(Success(tid, molecule))
              }
            case fail: Failure[_] =>
              success = false
              f(fail)
          }
        }

      for (i <- 1 to molecule.concentration) {
        mContext.out(
          tid,
          molecule.atoms(0).toLogicTuple,
          (op: ITucsonOperation) => f1(Success((tid, molecule)))
        )
      }

    })
  }

  override def start(f: Callback[ServiceId]) = {
    //TODO handle non-localhost case
    async(() => {
      if (id.isDefaultAddress) {
        if (TucsonNodeService.isInstalled(id.port, DEFAULT_TIMEOUT)) {
          obtainAcc()
          configureAndConnect(f)
        } else {
          install(_ match {
            case s: Success[ServiceId] => {
              obtainAcc()
              configureAndConnect(f)
            }
            case err: Failure[_] =>
              f(err)
          })
        }
      } else {
        obtainAcc()
        configureAndConnect(f)
      }
    })
  }

  protected def configureAndConnect(f: Callback[ServiceId]): Unit = {
    val defCid = CompartmentId(id)

    try {
      mContext.setS(
        defCid,
        Tucson.DEFAULT_SPEC_STRING,
        (op: ITucsonOperation) => {

          mContext.out(
            defCid,
            Tucson.ping_agent,
            (op1: ITucsonOperation) => {

              mContext.inp(
                defCid,
                Tucson.ping_agent_ok,
                (op2: ITucsonOperation) => {
                  if (op2.isResultSuccess) {
                    f(Success(id))
                  } else {
                    f(Failure(new IllegalStateException(s"$id didn't respond to ping")))
                  }

                }
              )


            }
          )

        }
      )
    } catch {
      case t: Throwable =>
        f(Failure(t))
    }
  }

  protected def install(f: Callback[ServiceId]): Unit = {
    mTucsonService = new ScalaTucsonService(id.port)
    mTucsonService.install(f)
  }

  protected def obtainAcc(): Unit = {
    mContext = TucsonMetaACC.getContext(s"service${System.currentTimeMillis()}")
    mContext2 = TucsonMetaACC.getContext(s"service2${System.currentTimeMillis()}")
  }

  override def isStarted: Boolean =
    mContext != null

  override def isLocal: Boolean =
    mId.isDefaultAddress

  override def startCompartmentInspection(name: String, f: Callback[Compartment]) = {
    async(() => {
      val cid = CompartmentId(name, id)
      val insp = new CompartmentInspector(cid, this, f)
      mInspected += cid -> insp
      insp.start()
    })
  }

  override def connectCompartment(name: String, to: CompartmentId, f: Callback[(CompartmentId, CompartmentId)]) =
    async(() => {
      val from = CompartmentId(name, id)
      mContext.out(
        from,
        Tucson.connectTo(to),
        (op: ITucsonOperation) => f(Success((from, to)))
      )
    })

  override def stopCompartmentInspection(name: String, f: Callback[CompartmentId]) = {
    async(() => {
      val cid: CompartmentIdImpl = CompartmentId(name, id)
      mInspected.get(cid)
        .foreach(_.quit(f))
      mInspected -= cid
    })
  }

  override def out(name: String, tuple: String, duration: Duration): Try[ITucsonOperation] = {
    val cid = CompartmentId(name, id)
    try {
      val res = mContext2.synchronized {
        mContext2.out(cid.toTupleCentreId, LogicTuple.parse(tuple), duration.toMillis)
      }
      Success(res)
    } catch {
      case t: Throwable =>
        Failure(t)
    }
  }

  override def in(name: String, template: String, duration: Duration): Try[ITucsonOperation] = {
    val cid = CompartmentId(name, id)
    try {
      val res = mContext2.synchronized {
        mContext2.in(cid.toTupleCentreId, LogicTuple.parse(template), duration.toMillis)
      }
      Success(res)
    } catch {
      case t: Throwable =>
        Failure(t)
    }
  }

  protected def async[T](f: () => T) =
    mEventLoop.execute(f)

  override def id: ServiceId = mId

}

object TestServerImpl extends App {
  val s = new ServiceApiImpl(ServiceId())

  def onStarted(sid: Try[ServiceId]) = {
    println(s"[${Thread.currentThread()}]$sid started")
    s.createCompartment("a", onCreated)
  }

  def onCreated(cid: Try[CompartmentId]) = {
    println(s"[${Thread.currentThread()}]$cid created & configured")
    s.startCompartmentInspection("a", onInspect)
  }

  def onInspect(c: Try[Compartment]) = {
    println(c)
  }

  s.start(onStarted)
}