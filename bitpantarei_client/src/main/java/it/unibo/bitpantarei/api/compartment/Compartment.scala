package it.unibo.bitpantarei.api.compartment

import it.unibo.bitpantarei.api.id.CompartmentId
import it.unibo.bitpantarei.api.molecule.Molecule
import it.unibo.bitpantarei.api.topic.Topic

/**
 * Created by giova on 17/10/2015.
 */
trait Compartment {
  def id: CompartmentId

  def molecules: Set[Molecule]

  def state: CompartmentState

  def neighborhood: Neighborhood

  def size: Int
}

trait MutableCompartment extends Compartment {
  def -=(n: CompartmentId): Unit

  def +=(n: CompartmentId): Unit

  def +=(n: CompartmentId, sp: Double): Unit

  def +=(n: CompartmentId, tp: Topic): Unit

  def +=(elem: Molecule): Unit

  def -=(elem: Molecule): Unit
}