package it.unibo.bitpantarei.api.connections

import it.unibo.bitpantarei.api.id.{CompartmentId, ServiceId}

/**
 * Created by gciatto on 24/10/15.
 */
trait Connection[A, B] {
  implicit def inv: Connection[B, A]

  def from: A

  def to: B

  implicit def asSet: Set[Any]

  override def toString = s"($from) <-> ($to)"

  override def equals(other: Any): Boolean = other match {
    case conn: Connection[_, _] =>
      asSet == conn.asSet
    case _ => false
  }


  override def hashCode(): Int =
    asSet.hashCode
}

object Connection {
  def apply[A, B](a: A, b: B) = Direct(a, b)

  implicit class ConnectionOperator[A](a: A) {
    def <->[B](b: B): Connection[A, B] = Direct[A, B](a, b)
  }

}

case class Direct[A, B](private val a: A, private val b: B) extends Connection[A, B] {
  private lazy val inverse = Inverse(this)
  private lazy val set = Set(a, b)

  override implicit def inv: Connection[B, A] = inverse

  override def to: B = b

  override def from: A = a

  override implicit def asSet: Set[Any] = set
}

case class Inverse[A, B](private val direct: Direct[A, B]) extends Connection[B, A] {
  override implicit def inv: Connection[A, B] = direct

  override def to: A = direct.from

  override def from: B = direct.to

  override implicit def asSet: Set[Any] = direct.asSet
}

object TestConnection extends App {

  import CompartmentId._
  import Connection._
  import ServiceId._

  val x = ("a" at ("l" on 1)) <-> ("b" at ("l" on 1))
  val y = ("b" at ("l" on 1)) <-> ("a" at ("l" on 1))

  println(x)
  println(y)
  println(x == y)
}