package it.unibo.bitpantarei.api.compartment.impl

import it.unibo.bitpantarei.api.compartment.Neighborhood
import it.unibo.bitpantarei.api.id.CompartmentId
import it.unibo.bitpantarei.api.topic.{NoTopic, Topic}

import scala.collection.mutable

/**
 * Created by giova on 18/10/2015.
 */
class NeighborhoodImpl extends Neighborhood {
  val mMap =
    new mutable.HashMap[CompartmentId, (Option[Double], Topic, Set[Topic])]

  override def neighbors: Predef.Set[CompartmentId] =
    mMap.keySet.toSet

  override def spreadingProb(neighbor: CompartmentId): Option[Double] =
    mMap(neighbor) match {
      case (sp: Some[Double], _, _) => sp
      case _ => None
    }

  override def interests(neighbor: CompartmentId): Predef.Set[Topic] =
    mMap(neighbor) match {
      case (_, _, s: Set[Topic]) => s.toSet
      case _ => Predef.Set.empty
    }

  override def topic(neighbor: CompartmentId): Topic =
    mMap(neighbor)._2

  override def spreadingProbs: Map[CompartmentId, Double] =
    mMap
      .filterNot(_._2._1.isEmpty)
      .mapValues(_._1.get)
      .toMap

  override def topics: Map[CompartmentId, Topic] =
    mMap
      .mapValues(_._2)
      .toMap


  override def interests: Map[CompartmentId, Set[Topic]] =
    mMap
      .mapValues(_._3)
      .toMap

  def +=(n: CompartmentId, sp: Double): Unit = {
    if (!mMap.contains(n))
      this += n
    mMap(n) match {
      case (p, t, is) =>
        mMap += ((n, (Some(sp), t, is)))
    }
  }

  def +=(n: CompartmentId): Unit =
    if (!mMap.contains(n)) {
      mMap += ((n, (None, NoTopic, Set.empty)))
    }

  def +=(n: CompartmentId, tp: Topic): Unit = {
    if (!mMap.contains(n))
      this += n
    if (tp.interest < 1d)
      mMap(n) match {
        case (p, t, is) =>
          mMap += ((n, (p, t, is + tp)))
      }
    else
      mMap(n) match {
        case (p, t, is) =>
          mMap += ((n, (p, tp, is)))
      }
  }

  def -=(n: CompartmentId): Unit =
    if (mMap.contains(n))
      mMap -= n


  override def toString =
    s"neighborhood: $mMap"
}
