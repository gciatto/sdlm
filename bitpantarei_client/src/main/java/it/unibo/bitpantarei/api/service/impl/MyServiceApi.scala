package it.unibo.bitpantarei.api.service.impl

import java.util.concurrent.{ExecutorService, Executors}

import it.unibo.bitpantarei.api.discovery4tucson._
import it.unibo.bitpantarei.api.id.{CompartmentId, ServiceId}
import it.unibo.bitpantarei.api.topic.Topic
import it.unibo.mfrancia.discovery._

import scala.collection.mutable

class MyServiceApi(mId: ServiceId) extends ServiceApiImpl(mId) {

  val pool: ExecutorService = Executors.newCachedThreadPool
  var local: Boolean = Syskb.START_WITH_REMOTE_DISC
  // true = remote, false = local discovery
  var tcneigh: Daemon_TCNeighbours = null
  var cleaner: Daemon_Cleaner_Local = null
  var map = mutable.HashMap[String, Daemon]()
  var list = mutable.ArrayBuffer[iTupleCenter]()

  override def start(f: Callback[ServiceId]): Unit = {
    super.start(x => {
      f(x)
      setLocal(!local)
    })
  }

  def setLocal(l: Boolean) = {
    if (local != l) {
      local = l
      clean
      cleaner = new Tucson_Daemon_Cleaner_Local(this, list, Syskb.LOCALDAEMON_CLEAN_MILLIS)
      execute(cleaner)

      if (local) {
        tcneigh = new Daemon_TCNeighbours_LAN(list)
        execute(tcneigh)
        list.foreach(t => execute(new Tucson_Daemon_TCPing_Lan(this, t, Syskb.LOCALDAEMON_PING_MILLIS, Syskb.LOCALDAEMON_RETRIES)))
      } else {
        tcneigh = new Daemon_TCNeighbours_Remote(list, Syskb.DNSSERVER_RETRIEVE_MILLIS)
        execute(tcneigh)
        list.foreach(t => execute(new Tucson_Daemon_TCPing_Remote(this, t, Syskb.LOCALDAEMON_PING_MILLIS, Syskb.LOCALDAEMON_RETRIES)))
      }
    }
  }

  override def stop(f: Callback[ServiceId]): Unit = {
    super.stop(x => {
      f(x)
      clean
    })
  }

  private def clean() = {
    map.values.foreach(d => d.setStop())
    map = mutable.HashMap[String, Daemon]()
  }

  override def createCompartment(name: String, f: Callback[CompartmentId]): Unit = {
    super.createCompartment(name, x => {
      f(x)
      val tc: Tucson_TupleCenter = Tucson_TupleCenter.createTucsonTupleCenter(this, x.get)
      list += tc

      if (local == false)
        execute(new Tucson_Daemon_TCPing_Remote(this, tc, Syskb.DNSSERVER_PING_MILLIS, Syskb.DNSSERVER_RETRIES))
      else
        execute(new Tucson_Daemon_TCPing_Lan(this, tc, Syskb.LOCALDAEMON_PING_MILLIS, Syskb.LOCALDAEMON_RETRIES))

      //cleaner.addTupleCenter(tc)
    })
  }

  def execute(d: Daemon): Unit = {
    map.put(d.id, d)
    pool.execute(d)
  }

  override def destroyCompartment(name: String, f: Callback[CompartmentId]): Unit = {
    super.destroyCompartment(name, x => {
      f(x)
      list -= list.find(t => t.name.equals(x.get.name)).get
      kill(s"ping ${x.get.toString}")
    })
  }

  def kill(id: String): Unit = {
    map.get(id).get.setStop()
    map.remove(id)
  }

  override def setCompartmentTopic(name: String, topic: Topic, f: Callback[(CompartmentId, Topic)]): Unit = {
    super.setCompartmentTopic(name, topic, x => {
      f(x)
      list.find(t => t.name.equals(x.get._1.name)).get.topic = topic.toString
    })
  }
}
