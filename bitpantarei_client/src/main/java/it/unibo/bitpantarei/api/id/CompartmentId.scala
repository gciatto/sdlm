package it.unibo.bitpantarei.api.id

import alice.logictuple.{LogicTuple, TupleArgument}
import alice.tucson.api.TucsonTupleCentreId
import it.unibo.bitpantarei.api.id.ServiceId.{DEFAULT_ADDRESS, DEFAULT_PORT}
import it.unibo.bitpantarei.api.id.impl.CompartmentIdImpl
import it.unibo.bitpantarei.api.tucson.Tucson
import it.unibo.mfrancia.discovery.TupleCenter

abstract class CompartmentId(mName: String, mHost: String, mPort: Int) extends TupleCenter(mName, mHost, mPort) {

  def serverId: ServiceId

  /*  override def toString: String =
      s"$name @ $serverId"*/

  override def equals(other: Any): Boolean = other match {
    case that: CompartmentId =>
      val otherId = other.asInstanceOf[CompartmentId]
      name == otherId.name &&
        serverId == otherId.serverId
    case _ => false
  }

  def toTupleCentreId: TucsonTupleCentreId

  override def hashCode(): Int = {
    val state = Seq(name, serverId)
    state.map(_.hashCode()).foldLeft(0)((a, b) => 31 * a + b)
  }
}

object CompartmentId {
  val DEFAULT_NAME = "default"

  def apply(): CompartmentIdImpl = new CompartmentIdImpl(DEFAULT_NAME, DEFAULT_ADDRESS, DEFAULT_PORT)

  def apply(port: Int) = new CompartmentIdImpl(DEFAULT_NAME, DEFAULT_ADDRESS, port)

  def apply(addr: String, port: Int) = new CompartmentIdImpl(DEFAULT_NAME, addr, port)

  def apply(name: String) = new CompartmentIdImpl(name, DEFAULT_ADDRESS, DEFAULT_PORT)

  def apply(sid: ServiceId) = new CompartmentIdImpl(DEFAULT_NAME, sid.address, sid.port)

  def apply(name: String, sid: ServiceId) = new CompartmentIdImpl(name, sid.address, sid.port)

  def apply(tuple: LogicTuple): CompartmentId = {
    // FIXME Removed template check because of strange 2p bug
    //if ((Tucson.NEIGHBOR_TEMPLATE `match` tuple) || (Tucson.SELF_REF_TEMPLATE `match` tuple)) {
      val name = tuple.getArg(0).toString
      val addr = tuple.getArg(1).toString
      val port = Tucson.parseInt(tuple.getArg(2))
      CompartmentId(name, addr, port)
    /*} else {
      throw new IllegalArgumentException
    }*/
  }

  def apply(name: String, addr: String, port: Int) = new CompartmentIdImpl(name, addr, port)

  def apply(tuple: TupleArgument): CompartmentId = {
    val name = tuple.getArg(0).toString
    val addr = tuple.getArg(1).toString
    val port = Tucson.parseInt(tuple.getArg(2))
    CompartmentId(name, addr, port)
  }

  implicit def toTCId(cid: CompartmentId): TucsonTupleCentreId =
    cid.toTupleCentreId

  implicit class NameString(name: String) {
    def at(sid: ServiceId): CompartmentId = apply(name, sid)
  }

}

