package it.unibo.bitpantarei.api.id.impl

import alice.tucson.api.TucsonTupleCentreId
import it.unibo.bitpantarei.api.id.{CompartmentId, ServiceId}

/**
 * Created by giova on 18/10/2015.
 */
class CompartmentIdImpl(mName: String, mHost: String, mPort: Int) extends CompartmentId(mName, mHost, mPort) {

  private lazy val mTid = new TucsonTupleCentreId(mName, mHost, mPort.toString)

  val mServerId = new ServiceIdImpl(mHost, mPort)

  override def serverId: ServiceId = mServerId

  override def toTupleCentreId: TucsonTupleCentreId = mTid
}
