package it.unibo.bitpantarei.api.compartment.inspector

import it.unibo.bitpantarei.api.compartment.Compartment
import it.unibo.bitpantarei.api.service.CallbackUser

/**
 * Created by giova on 18/10/2015.
 */
trait CompartmentInspector extends CallbackUser {
  def callback: Callback[Compartment]
}
