package it.unibo.bitpantarei.api.compartment

import it.unibo.bitpantarei.api.id.CompartmentId
import it.unibo.bitpantarei.api.topic.Topic

/**
 * Created by giova on 18/10/2015.
 */
trait Neighborhood {
  def neighbors: Set[CompartmentId]

  def spreadingProb(neighbor: CompartmentId): Option[Double]

  def topic(neighbor: CompartmentId): Topic

  def interests(neighbor: CompartmentId): Set[Topic]

  def topics: Map[CompartmentId, Topic]

  def interests: Map[CompartmentId, Set[Topic]]

  def spreadingProbs: Map[CompartmentId, Double]
}
