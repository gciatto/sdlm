package it.unibo.bitpantarei.api.service

import java.util.concurrent.TimeUnit

import alice.tucson.api.ITucsonOperation
import it.unibo.bitpantarei.api.compartment.Compartment
import it.unibo.bitpantarei.api.connections.Connection
import it.unibo.bitpantarei.api.discovery4tucson.Tucson_TupleCenter
import it.unibo.bitpantarei.api.id.{CompartmentId, ServiceId}
import it.unibo.bitpantarei.api.molecule.Molecule
import it.unibo.bitpantarei.api.topic.Topic

import scala.concurrent.duration.Duration
import scala.util.Try

/**
 * Created by giova on 17/10/2015.
 */


trait ServiceApi extends CallbackUser {

  type InterCompartmentConnection = Connection[CompartmentId, CompartmentId]
  type ConnectionsIterable = Iterable[InterCompartmentConnection]

    /**
     * getter restituisce id api corrente, non ho interazione remota
      */

  def id: ServiceId

  /**
   * Controlla se servizio tucson asssociato a id corrente è in funzione
   * @param f
   */
  def checkInstalled(f: Callback[Boolean]): Unit

  /**
   * Read_all sul tuple center di default, riceve neighbour e chiama callback.
   * Serveice id è quello corrente, è utile per utilizzatore
   * @param f
   */
  def compartments(f: Callback[(ServiceId, List[CompartmentId])]): Unit

  /**
   * Crea compartment
   * @param name
   * @param f
   */
  def createCompartment(name: String, f: Callback[CompartmentId]): Unit

  /**
   * Fa partire thread uguale a inspector di mariani + parser per tuple,
   * capisce stato attuale compartment e ne contiene tutte le informazioni.
   * La chiami una volta sola e la callback è chiamata periodicamente ogni
   * volta che arriva un dato. Non so bene cosa succede se si chiama due volte
   * questa funzione.
   * @param name
   * @param f
   */
  def startCompartmentInspection(name: String, f: Callback[Compartment]): Unit

  /**
   * Un compartimente si connette a un altro compartimento qualsiasi
   * @param name
   * @param to
   * @param f
   */
  def connectCompartment(name: String, to: CompartmentId, f: Callback[(CompartmentId, CompartmentId)]): Unit

  /**
    * termina ispezione
    * @param name
    * @param f
    */
  def stopCompartmentInspection(name: String, f: Callback[CompartmentId]): Unit

  /**
    * Cancella a un tuple center tutto il contenuto e tutte le sue reazioni
    * @param name
   * @param f
   */
  def destroyCompartment(name: String, f: Callback[CompartmentId]): Unit

  /**
    * Aggiorna/Crea topic nel compartimento, quando aggiungo il topic a un tuplecenter
    * tutti i vicini (e i vicini dei vicini) "a breve" conosceranno tale informazione.
    * Quando aggiorno il topic (al posto di inserirlo), sicuramente i vicini se ne accorgono,
    * bisogna gestire in modo opportuno i vicini dei vicini
   * @param name
    * @param topic
   * @param f
   */
  def setCompartmentTopic(name: String, topic: Topic, f: Callback[(CompartmentId, Topic)]): Unit

  /**
    * Molecola = tupla estesa, se passo una molecola con concentrazione 50, fa 50 volte la out.
    * E' lenta e computazionalmente onersosa
   * @param name
    * @param molecule
   * @param f
   */
  def addMolecule(name: String, molecule: Molecule, f: Callback[(CompartmentId, Molecule)]): Unit

  /**
    * Fa partire il nodo tucson, operazione complessa, deve gestire molte situazioni anomale:
    * - servizio già esistente (anche su un'altra macchina?)
    * - indirizzo errato
    * - ... gestire le casistiche è molto complesso
   * @param f
   */
  def start(f: Callback[ServiceId]): Unit

  /**
    * Se il servizio tucson è avviato dall'istanza dell'applicazione lo stoppa,
    * altrimenti lo disconnette (non può disattivarlo)
   * @param f
   */
  def stop(f: Callback[ServiceId]): Unit

  /**
    * Controlla che sia partito il servizio, controllo che il context non sia null
    * @return
   */
  def isStarted: Boolean

  /**
    * Controlla che indirizzo del seed sia localhost, va sistemata, non accetta 127.0.0.1
   * @return
   */
  def isLocal: Boolean

  /**
    * Variante delle precedenti
    * @param iterable
    * @param f
   */
  def connectCompartments(iterable: ConnectionsIterable, f: Callback[(CompartmentId, CompartmentId)]): Unit =
    iterable.foreach(connectCompartment(_, f))

  /**
    *
    * @param connection contiene due compartment id, è simmetrica, non importa l'ordine
   * @param f
   */
  def connectCompartment(connection: InterCompartmentConnection, f: Callback[(CompartmentId, CompartmentId)]): Unit = {
    if (connection.from.serverId == id) {
      connectCompartment(connection.from.name, connection.to, f)
    } else if (connection.inv.from.serverId == id) {
      connectCompartment(connection.inv.from.name, connection.inv.to, f)
    } else {
      throw new IllegalArgumentException
    }
  }

  def out(name: String, tuple: String, duration: Duration): Try[ITucsonOperation]

  def in(name: String, template: String, duration: Duration): Try[ITucsonOperation]

  def out(name: String, tuple: String, duration: Long): Try[ITucsonOperation] =
    out(name, tuple, Duration.apply(duration, TimeUnit.MILLISECONDS))

  def in(name: String, template: String, duration: Long): Try[ITucsonOperation] =
    in(name, template, Duration.apply(duration, TimeUnit.MILLISECONDS))
}

object ServiceApi extends CallbackUser {
  val DEFAULT_TIMEOUT = (1, TimeUnit.SECONDS)


  //TODO 4 Matteo! Implement this
  /**
   * Avvia una ricerca 'locale' di altri servizi [tucson] conclusa la quale,
   * viene invocata la callback f cui è passata la lista di sevizi.
   * Nota: Iterable di scala consideralo come il Collection di java.
   * Nota: non ti scervellare per far eseguire f ad un thread specifico:
   * a livello di gui ci ho già pensato io a wrappare tutte le callback nel thread grafico <3
   * @param f
   */
  def discoverServices(f: Callback[Iterable[ServiceId]]): Unit = ???
}