package it.unibo.bitpantarei.api.service

/**
 * Created by giova on 17/10/2015.
 */
trait Failable[T, E <: Throwable] {
  def ok: Boolean

  def fail: Boolean = !ok

  def get: T

  def error: E

  def ifOk(f: T => Any) {
    if (ok) {
      f(get)
    }
  }

  def ifFail(f: E => Any): Unit = {
    if (fail) {
      f(error)
    }
  }

  def ifOkElse(f1: T => Any, f2: E => Any): Unit = {
    if (ok) {
      f1(get)
    } else {
      f2(error)
    }
  }
}

object Failable {
  def apply[T, E <: Throwable](value: T): Value[T, E] =
    new Value[T, E](value)

  def apply[T, E <: Throwable](error: E): Error[T, E] =
    new Error[T, E](error)

  def apply[T, E <: Throwable](value: T, error: E): Problem[T, E] =
    new Problem[T, E](value, error)
}

case class Value[T, E <: Throwable](value: T) extends Failable[T, E] {
  override def ok: Boolean = true

  override def get: T = value

  override def error: E = throw new IllegalStateException

  override def toString: String =
    s"Value[$get]"
}

case class Error[T, E <: Throwable](exception: E) extends Failable[T, E] {
  override def ok: Boolean = true

  override def get: T = throw new IllegalStateException

  override def error: E = exception

  override def toString: String =
    s"Error[$error]"
}

case class Problem[T, E <: Throwable](value: T, exception: E) extends Failable[T, E] {
  override def ok: Boolean = true

  override def fail: Boolean = true

  override def get: T = value

  override def error: E = exception

  override def toString: String =
    s"Problem[value='$get', error='$error']"
}

object FailableTest extends App {
  for (i <- 0 to (args.length + 1)) {
    val a2i = parseInteger(i)

    println(a2i)
  }

  def parseInteger(i: Int): Failable[Int, Exception] = {
    try {
      val a = args(i)
      val v = Integer.parseInt(a)
      if (v < 0) throw new NegativeException(v)
      Failable(v)
    } catch {
      case aioobe: ArrayIndexOutOfBoundsException =>
        Failable(aioobe)
      case nfe: NumberFormatException =>
        Failable(nfe)
      case ne: NegativeException =>
        Failable(ne.cause, ne)
    }
  }
}

class NegativeException(i: Int)
  extends Exception(s"$i") {

  def cause = i
}