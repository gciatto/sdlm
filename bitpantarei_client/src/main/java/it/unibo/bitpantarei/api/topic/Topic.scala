package it.unibo.bitpantarei.api.topic

import alice.logictuple.{LogicTuple, TupleArgument}
import it.unibo.bitpantarei.api.tucson.Tucson

/**
 * Created by giova on 18/10/2015.
 */
trait Topic {
  def toLogicTuple: LogicTuple

  def toTupleArg: TupleArgument

  def toList: List[String]

  def levels: Int =
    toList.length

  def interest: Double

  def isTopic: Boolean = interest >= 1.0

  def isInterest: Boolean = interest >= 0.0 && interest < 1.0

  def isAbsent: Boolean

  override def toString: String =
    toList
      .map(_.toLowerCase)
      .reduceOption(_ + Topic.SEPARATOR + _)
      .getOrElse(Topic.JOLLY)

  override def equals(other: Any): Boolean = {
    if (Option(other).isEmpty)
      false
    else if (!other.isInstanceOf[Topic])
      false
    else {
      val otherTopic = other.asInstanceOf[Topic]
      if (levels != otherTopic.levels)
        false
      else
        toList
          .zip(other.asInstanceOf[Topic].toList)
          .forall(it => it._1 equalsIgnoreCase it._2)
    }
  }

  override def hashCode: Int =
    toList.hashCode()
}

object Topic {
  val SEPARATOR = "."
  val JOLLY = "*"
  val ANONYMOUS = "_"
  val NO_TOPIC = "-"

  def apply(toParse: String): Topic =
    apply(toParse, 1d)

  def apply(toParse: String, interest: Double): Topic = {
    def check(s: Seq[String]): Unit =
      if (!s.forall(LogicTuple.parse(_).toTerm.isAtom))
        throw new IllegalArgumentException

    if (Option(toParse).isEmpty) throw new IllegalArgumentException
    val clean = toParse.replaceAll("\\s+", "_")
    if (NO_TOPIC == clean) {
      NoTopic
    } else {
      val parts = clean.split("[.]")
      var toSeq: Seq[String] = null
      if (parts(parts.length - 1) == JOLLY) {
        toSeq = parts.take(parts.length - 1).toSeq
        check(toSeq)
        GenericTopic(toSeq, interest)
      } else {
        toSeq = parts.toSeq
        check(toSeq)
        SpecificTopic(parts.toSeq, interest)
      }
    }
  }

  def apply(t: LogicTuple): Topic = {
    if (Tucson.TOPIC_TEMPLATE `match` t)
      tupleArgToTopic(t.getArg(0), 1)
    else
      throw new IllegalArgumentException
  }

  def apply(t: TupleArgument): Topic = {
    if (Tucson.TOPIC_TEMPLATE.toTerm `match` t.toTerm)
      tupleArgToTopic(t.getArg(0), 1)
    else if (Tucson.INTEREST_TEMPLATE `match` t)
      tupleArgToTopic(t.getArg(0), t.getArg(1).doubleValue())
    else
      throw new IllegalArgumentException
  }

  def tupleArgToTopic(ta: TupleArgument, int: Double): Topic = {
    def tupleArgToTopic(ta: TupleArgument, l: List[String]): Topic =
      if (ta.isStruct && ta.getArity > 0)
        tupleArgToTopic(ta.getArg(0), l ++ List(ta.getName))
      else {
        if (ta.isVar)
          GenericTopic(l, int)
        else
          SpecificTopic(l ++ List(ta.getName), int)
      }

    tupleArgToTopic(ta, List.empty)
  }

  def seqToTupleString(seq: Seq[String], last: String): String =
    seq.foldRight(
      if (last == JOLLY)
        ANONYMOUS
      else
        last
    )(_.toLowerCase + "(" + _.toLowerCase + ")")


  def seqToTupleString(seq: Seq[String]): String = {
    seq.reduceRight(_.toLowerCase + "(" + _.toLowerCase + ")")
  }


}

case class GenericTopic(seq: Seq[String], int: Double) extends Topic {

  override def toLogicTuple: LogicTuple =
    LogicTuple.parse(Topic.seqToTupleString(seq, Topic.ANONYMOUS))

  override def toList: List[String] =
    (seq ++ Seq(Topic.JOLLY)).toList

  override def toTupleArg: TupleArgument =
    TupleArgument.parse(Topic.seqToTupleString(seq, Topic.ANONYMOUS))

  override def interest: Double = int

  override def isAbsent: Boolean = false
}

case class SpecificTopic(seq: Seq[String], int: Double) extends Topic {
  if (seq.isEmpty) throw new IllegalArgumentException

  override def toLogicTuple: LogicTuple =
    LogicTuple.parse(Topic.seqToTupleString(seq))

  override def toList: List[String] =
    seq.toList

  override def toTupleArg: TupleArgument =
    TupleArgument.parse(Topic.seqToTupleString(seq))

  override def interest: Double = int

  override def isAbsent: Boolean = false
}

case object NoTopic extends Topic {
  override def toLogicTuple: LogicTuple = null

  override def interest: Double = 1.0

  override def toList: List[String] = List(Topic.NO_TOPIC)

  override def toTupleArg: TupleArgument = null

  override def isAbsent: Boolean = true
}

object TestTopic extends App {
  val x = Topic("-")
  println(x)
  println(x.toLogicTuple)
}