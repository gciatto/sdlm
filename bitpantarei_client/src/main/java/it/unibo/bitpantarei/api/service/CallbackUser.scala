package it.unibo.bitpantarei.api.service

import scala.util.Try

/**
 * Created by giova on 20/10/2015.
 */
trait CallbackUser {
  type Callback[T] = Try[T] => Any
}

object CallbackUser extends CallbackUser {
  def doNothing[T](): Try[T] => Any =
    t => {}
}