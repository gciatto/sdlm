package it.unibo.bitpantarei.api.compartment.impl

import it.unibo.bitpantarei.api.compartment.{CompartmentState, MutableCompartment, Neighborhood}
import it.unibo.bitpantarei.api.id.CompartmentId
import it.unibo.bitpantarei.api.molecule.Molecule
import it.unibo.bitpantarei.api.topic.Topic

import scala.collection.mutable
import scala.concurrent.duration.Duration

/**
 * Created by giova on 19/10/2015.
 */
class CompartmentImpl(private var mId: CompartmentId) extends MutableCompartment {
  val mData = new mutable.HashSet[Molecule]

  val mState = new CompartmentStateImpl()
  val mNeighborhood = new NeighborhoodImpl()

  override def id: CompartmentId = mId

  def id_=(id: CompartmentId) = mId = id

  override def molecules: Set[Molecule] = mData.toSet

  override def -=(n: CompartmentId): Unit = mNeighborhood.-=(n)

  override def +=(n: CompartmentId): Unit = mNeighborhood.+=(n)

  override def +=(n: CompartmentId, sp: Double): Unit = mNeighborhood +=(n, sp)

  override def +=(n: CompartmentId, tp: Topic): Unit = mNeighborhood +=(n, tp)

  override def +=(elem: Molecule): Unit = mData += elem

  override def -=(elem: Molecule): Unit = mData -= elem

  override def size: Int = mData.size

  //mData.count(_.concentration > 0)

  def period = mState.period

  def period_=(dur: Duration): Unit = mState.period = dur

  def initialConcentration = mState.initialConcentration

  def initialConcentration_=(init: Int): Unit = mState.initialConcentration = init

  def evaporationProb = mState.evaporationProb

  def evaporationProb_=(ep: Double): Unit = mState.evaporationProb = ep

  def lastTick = mState.lastTick

  def lastTick_=(lt: (Long, Duration)): Unit = mState.lastTick = lt

  def topic = mState.topic

  def topic_=(t: Topic): Unit = mState.topic = t

  override def toString = s"compartment: ($state, $neighborhood){$mData]"

  override def state: CompartmentState = mState

  override def neighborhood: Neighborhood = mNeighborhood
}
