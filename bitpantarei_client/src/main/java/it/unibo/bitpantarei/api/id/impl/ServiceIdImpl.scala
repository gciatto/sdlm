package it.unibo.bitpantarei.api.id.impl

import it.unibo.bitpantarei.api.id.ServiceId
import it.unibo.bitpantarei.api.id.ServiceId.{DEFAULT_ADDRESS, DEFAULT_PORT}

/**
 * Created by giova on 18/10/2015.
 */
class ServiceIdImpl(mAddress: String, mPort: Int) extends ServiceId {

  if (mPort < 0 || mPort > (1 << 16) - 1) {
    throw new IllegalArgumentException
  }

  def this() = this(DEFAULT_ADDRESS, DEFAULT_PORT)

  override def address: String = mAddress

  override def port: Int = mPort
}
