package it.unibo.bitpantarei.api.id

import it.unibo.bitpantarei.api.id.impl.ServiceIdImpl

trait ServiceId {

  def address: String

  def port: Int

  def isDefaultAddress: Boolean =
    address == ServiceId.DEFAULT_ADDRESS

  def isDefaultPort: Boolean =
    port == ServiceId.DEFAULT_PORT

  override def toString: String =
    s"$address : $port"

  override def equals(other: Any): Boolean = other match {
    case that: ServiceId => {
      val otherId = other.asInstanceOf[ServiceId]
      address == otherId.address &&
        port == otherId.port
    }
    case _ => false
  }

  override def hashCode(): Int = {
    val state = Seq(address, port)
    state.map(_.hashCode()).foldLeft(0)((a, b) => 31 * a + b)
  }
}

object ServiceId {
  val DEFAULT_ADDRESS = "localhost"
  val DEFAULT_PORT = 20504

  def apply() = new ServiceIdImpl(DEFAULT_ADDRESS, DEFAULT_PORT)

  def apply(addr: String, port: Int) = new ServiceIdImpl(addr, port)

  def apply(port: Int) = new ServiceIdImpl(DEFAULT_ADDRESS, port)

  implicit class AddrString(addr: String) {
    def on(port: Int): ServiceId = apply(addr, port)
  }
}

