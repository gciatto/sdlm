package it.unibo.bitpantarei.api.discovery4tucson

import it.unibo.bitpantarei.api.service.ServiceApi
import it.unibo.bitpantarei.api.tucson.Tucson
import it.unibo.mfrancia.discovery.{Daemon_Cleaner_Local, iTupleCenter}

import scala.collection.mutable

class Tucson_Daemon_Cleaner_Local(api: ServiceApi, tcs: mutable.ArrayBuffer[iTupleCenter], cleanMillis: Long) extends Daemon_Cleaner_Local(tcs, cleanMillis) {
  def this(api: ServiceApi, cleanMillis: Long) = this(api, mutable.ArrayBuffer[iTupleCenter](), cleanMillis)

  override def clean(tc: iTupleCenter) = api.out(tc.name, Tucson.deleteString, 50)
}