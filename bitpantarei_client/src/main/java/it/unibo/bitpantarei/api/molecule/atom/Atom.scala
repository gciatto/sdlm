package it.unibo.bitpantarei.api.molecule.atom

import alice.logictuple.{LogicTuple, TupleArgument}
import alice.tuprolog.Struct
import it.unibo.bitpantarei.api.tucson.Tucson

/**
 * Created by giova on 18/10/2015.
 */
trait Atom {
  def toTupleArg: TupleArgument

  def toLogicTuple: LogicTuple
}

object Atom {
  def apply(toParse: String) =
    new AtomImpl(TupleArgument.parse(toParse))

  implicit def toTupleArg(a: Atom): TupleArgument =
    a.toTupleArg

  implicit def toAtom(t: TupleArgument): Atom =
    Atom(t)

  def apply(arg: TupleArgument): AtomImpl = //{
  //    if (Tucson.ATOM_TEMPLATE `match` arg) {
      new AtomImpl(arg.getArg(0))

  //    } else
  //      throw new IllegalArgumentException
  //  }
}

class AtomImpl(arg: TupleArgument) extends Atom {

  val mArg = if (Tucson.ATOM_TEMPLATE `match` arg)
    arg
  else
    new TupleArgument(new Struct(Tucson.ATOM, arg.toTerm))

  override def toTupleArg: TupleArgument =
    mArg

  override def toLogicTuple: LogicTuple =
    new LogicTuple(mArg.toTerm)

  override def equals(other: Any): Boolean = other match {
    case that: Atom =>
        mArg `match`  that.toTupleArg
    case _ => false
  }

  override def hashCode(): Int = {
    val state = Seq(mArg)
    state.map(_.hashCode()).foldLeft(0)((a, b) => 31 * a + b)
  }

  override def toString: String =
    mArg.getArg(0).toString
}