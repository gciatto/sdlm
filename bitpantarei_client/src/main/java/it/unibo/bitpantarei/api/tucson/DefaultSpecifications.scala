package it.unibo.bitpantarei.api.tucson

import java.util.{MissingResourceException, ResourceBundle}
;

/**
 * Created by gciatto on 26/10/15.
 */
object DefaultSpecifications {
  private val BUNDLE_NAME: String = "it.unibo.bitpantarei.api.tucson.default_specifications"
  //  private val BUNDLE_NAME: String = "default_specifications"
  private val RESOURCE_BUNDLE: ResourceBundle = ResourceBundle.getBundle(BUNDLE_NAME)

  def getSpecString(key: String): String = {
    try {
      RESOURCE_BUNDLE.getString(key)
    }
    catch {
      case e: MissingResourceException => {
        '!' + key + '!'
      }
    }
  }
}
