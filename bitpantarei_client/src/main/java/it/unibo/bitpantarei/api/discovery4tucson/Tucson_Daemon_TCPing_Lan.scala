package it.unibo.bitpantarei.api.discovery4tucson

import it.unibo.bitpantarei.api.service.ServiceApi
import it.unibo.mfrancia.discovery.{iTupleCenter, TupleCenter, Daemon_TCPing_Lan}

class Tucson_Daemon_TCPing_Lan(api: ServiceApi, tc: iTupleCenter, millisPing: Long, retriesNumber: Int) extends Daemon_TCPing_Lan(tc, millisPing, retriesNumber) {
  override def isTcAlive = {
    api.out(tc.name, "ping", 50)
    api.in(tc.name, "ping", 50)
    true
  }
}
