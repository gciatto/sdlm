package it.unibo.bitpantarei.api.service.impl

import java.util.concurrent.TimeUnit

import alice.logictuple.{LogicTuple, TupleArgument}
import alice.tucson.introspection.{Inspector, InspectorContextEvent, InspectorProtocol}
import it.unibo.bitpantarei.api.Conversions.toAgentId
import it.unibo.bitpantarei.api.compartment.Compartment
import it.unibo.bitpantarei.api.compartment.impl.CompartmentImpl
import it.unibo.bitpantarei.api.id.CompartmentId
import it.unibo.bitpantarei.api.molecule.Molecule
import it.unibo.bitpantarei.api.service.CallbackUser
import it.unibo.bitpantarei.api.topic.Topic
import it.unibo.bitpantarei.api.tucson.Tucson

import scala.collection.JavaConversions.asScalaBuffer
import scala.collection.mutable
import scala.concurrent.duration.Duration
import scala.util.{Failure, Success, Try}

/**
 * Created by giova on 16/10/2015.
 */

class CompartmentInspector(cid: CompartmentId, parent: ServiceApiImpl, f: Try[Compartment] => Any)
  extends Inspector(s"inspectorOf${cid.name}${System.currentTimeMillis()}", cid)
  with CallbackUser {

  val protocol = new InspectorProtocol
  protocol.setTsetObservType(InspectorProtocol.PROACTIVE_OBSERVATION)
  getContext.setProtocol(protocol)

  //TODO: protect me!
  var onTerminated: Option[Callback[CompartmentId]] = Option.empty

  override def onContextEvent(ev: InspectorContextEvent): Unit =
    f(ev)

  override def run() = {
    println("DefaultInspector started")
    var continue = true
    while (continue) {
      try {
        super.run()
        onTerminated.foreach(_ (Success(inspected)))
        continue = false
      } catch {
        case t: Throwable =>
          f(Failure(t))
          t.printStackTrace()
          continue = true
      }
    }
      println("DefaultInspector dead")
  }

  def inspected = cid

  def quit(f: Callback[CompartmentId]) = {
    onTerminated = Option(f)
    super.quit()
  }

  protected implicit def evToCompartment(ev: InspectorContextEvent): Try[Compartment] = {
    def parseTuples(ts: mutable.Buffer[LogicTuple], res: Try[CompartmentImpl]): Try[Compartment] = {
      res match {
        case s: Success[CompartmentImpl] =>
          if (ts.isEmpty) s
          else {
            parseTuple(ts.head, s.get)
            parseTuples(ts.tail, s)
          }
        case f: Failure[CompartmentImpl] => f
      }
    }

    parseTuples(ev.getTuples, Success(new CompartmentImpl(inspected)))
  }

  protected def parseTuple(t: LogicTuple, dest: CompartmentImpl): Unit = {
    if (Tucson.SELF_REF_TEMPLATE `match` t) {
      dest.id = CompartmentId(t)
      //parent.testAndSet(sid => sid != dest.id.serverId, dest.id.serverId)
    } else if (Tucson.MOLECULE_TEMPLATE `match` t)
      dest += Molecule(t)
    else if (Tucson.EVAPORATION_PROB_TEMPLATE `match` t)
      dest.evaporationProb = toEvapProb(t)
    else if (Tucson.INFO_INTEREST_TEMPLATE `match` t)
      dest +=(CompartmentId(t.getArg(0)), Topic(t.getArg(1)))
    else if (Tucson.INFO_SP_TEMPLATE `match` t)
      dest +=(CompartmentId(t.getArg(0)), toEvapProb(t.getArg(1)))
    else if (Tucson.INFO_TOPIC_TEMPLATE `match` t)
      dest +=(CompartmentId(t.getArg(0)), Topic(t.getArg(1)))
    else if (Tucson.MOLECULE_TEMPLATE `match` t)
      dest += Molecule(t)
    else if (Tucson.NEIGHBOR_TEMPLATE `match` t)
      dest += CompartmentId(t)
    else if (Tucson.PERIOD_TEMPLATE `match` t)
      dest.period = Duration.create(Tucson.parseLong(t.getArg(0)), TimeUnit.MILLISECONDS)
    else if (Tucson.INITIAL_CONCENTRATION_TEMPLATE `match` t)
      dest.initialConcentration = Tucson.parseInt(t.getArg(0))
    else if (Tucson.TICK_TEMPLATE `match` t)
      dest.lastTick = (
        Tucson.parseLong(t.getArg(0)),
        Duration.create(Tucson.parseLong(t.getArg(1)), TimeUnit.MILLISECONDS)
        )
    else if (Tucson.TOPIC_TEMPLATE `match` t)
      dest.topic = Topic(t)
  }

  protected def toEvapProb(t: LogicTuple): Double =
    t.getArg(0).doubleValue()

  protected def toEvapProb(t: TupleArgument): Double =
    t.getArg(0).doubleValue()

}
