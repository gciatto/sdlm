package it.unibo.bitpantarei.api.molecule

import alice.logictuple.LogicTuple
import it.unibo.bitpantarei.api.molecule.atom.Atom
import it.unibo.bitpantarei.api.tucson.Tucson

/**
 * Created by giova on 18/10/2015.
 */
trait Molecule {
  def concentration: Int

  def atoms: Seq[Atom]

  def countAtoms: Int

  def toLogicTuple: LogicTuple

}

object Molecule {
  def apply(a: Atom, c: Int) =
    new Monoatomic(a, c)

  def apply(t: LogicTuple): Monoatomic = {
    //    if (Tucson.MOLECULE_TEMPLATE `match` t) {
      new Monoatomic(Atom(t.getArg(1)), t.getArg(0).intValue())
    //    } else {
    //      throw new IllegalArgumentException
    //    }
  }
}

case class Monoatomic(private val mAtom: Atom, private val mConcentration: Int) extends Molecule {

  override def countAtoms: Int = 1

  override def toString: String =
    s"'${atoms.map(_.toString).reduceLeft(_ + ", " + _)}':$concentration"

  override def atoms: Seq[Atom] = List(mAtom)

  override def toLogicTuple: LogicTuple =
    new LogicTuple(Tucson.MOLECULE, new alice.logictuple.Value(concentration), mAtom.toTupleArg)

  override def concentration: Int = mConcentration

  override def equals(other: Any): Boolean = other match {
    case that: Monoatomic =>
      mAtom ==  that.mAtom
    case _ => false
  }

  override def hashCode(): Int = {
    val state = Seq(mAtom)
    state.map(_.hashCode()).foldLeft(0)((a, b) => 31 * a + b)
  }
}

object TestMolecule extends App {
  val template = "extTuple(5, tuple(aaaaa))"
  val templateTuple = LogicTuple.parse(template)
  val mol = Molecule(templateTuple)
  println(s"{${mol.concentration}[$mol]")
  val molTuple = mol.toLogicTuple
  println(molTuple `match` templateTuple)
}