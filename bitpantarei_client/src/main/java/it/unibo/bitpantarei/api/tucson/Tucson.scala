package it.unibo.bitpantarei.api.tucson

import java.io.{BufferedReader, File, FileReader}

import alice.logictuple.LogicTuple.parse
import alice.logictuple.TupleArgument
import it.unibo.bitpantarei.api.Conversions.toConsumer
import it.unibo.bitpantarei.api.id.CompartmentId

/**
 * Created by giova on 18/10/2015.
 */
object Tucson {
  lazy val DELETE_DEAD_NEIGHS_TEMPLATE =
    parse(s"$DELETE_DEAD_NEIGHS(_)")
  lazy val NEIGHBOR_TEMPLATE =
    parse(s"$NEIGHBOR(_,_,_)")
  lazy val MOLECULE_TEMPLATE =
    parse(s"$MOLECULE(_,$ATOM(_))")
  lazy val EVAPORATION_PROB_TEMPLATE =
    parse(s"$EVAPORATION_PROB(_)")
  lazy val SELF_REF_TEMPLATE =
    parse(s"$SELF_REF(_,_,_)")
  lazy val INFO_SP_TEMPLATE =
    parse(s"$INFO($NEIGHBOR(_,_,_),$SPREADING_PROB(_))")
  lazy val INFO_TOPIC_TEMPLATE =
    parse(s"$INFO($NEIGHBOR(_,_,_),$TOPIC(_))")
  lazy val TOPIC_TEMPLATE =
    parse(s"$TOPIC(_)")
  lazy val INTEREST_TEMPLATE =
    TupleArgument.parse(s"$INTEREST(_, _)")
  lazy val INFO_INTEREST_TEMPLATE =
    parse(s"$INFO($NEIGHBOR(_,_,_),$INTEREST(_, _))")
  lazy val PERIOD_TEMPLATE =
    parse(s"$PERIOD(_)")
  lazy val TICK_TEMPLATE =
    parse(s"$TICK_COUNT(_, _)")
  lazy val ATOM_TEMPLATE =
    MOLECULE_TEMPLATE.getArg(1)
  lazy val INITIAL_CONCENTRATION_TEMPLATE =
    parse(s"$INITIAL_CONCENTRATION(_)")
  lazy val COMPARTMENT_SPEC_STRING =
    if (DEBUG) specString(COMPARTMENT_SPEC_FILE)
    else DefaultSpecifications.getSpecString("spec_compartment")
  lazy val DEFAULT_SPEC_STRING =
    if (DEBUG) specString(DEFAULT_SPEC_FILE)
    else DefaultSpecifications.getSpecString("spec_default")
  lazy val ping_agent =
    parse("ping_ag")
  lazy val ping_agent_ok =
    parse("ping_ag_ok")
  val DEBUG = true
  val COMPARTMENT_SPEC_FILE = new File("res/compartment.rsp").getAbsoluteFile
  val DEFAULT_SPEC_FILE = new File("res/default.rsp").getAbsoluteFile
  val NEIGHBOR = "neighbor"
  val INITIAL_CONCENTRATION = "initial_concentration"
  val EVAPORATION_PROB = "evaporation_prob"
  val INFO = "info"
  val SPREADING_PROB = "spread_prob"
  val TOPIC = "topic"
  val MOLECULE = "extTuple"
  val ATOM = "tuple"
  val INTEREST = "interest"
  val DESTROY = "destroy"
  val CONFIG = "config"
  val CONNECT = "connect_to"
  val SELF_REF = "self"
  val PERIOD = "period"
  val TICK_COUNT = "count_tick"
  val DELETE_DEAD_NEIGHS = "delete_dead_neighbors"

  def deleteString =
    s"$DELETE_DEAD_NEIGHS(${System.currentTimeMillis()})"

  def specString(f: File): String = {
    val r = new BufferedReader(
      new FileReader(f)
    )

    val b = new StringBuffer(f.length().toInt)

    r.lines().forEach((it: String) => {
      b.append(it).append("\n")
    })

    b.toString
  }

  def parseInt(ta: TupleArgument): Int =
    parseLong(ta).toInt

  def parseLong(ta: TupleArgument): Long = {
    if (ta.isNumber) {
      if (ta.isLong)
        ta.longValue()
      else if (ta.isInt)
        ta.intValue().toLong
      else if (ta.isDouble)
        ta.doubleValue().toLong
      else if (ta.isFloat)
        ta.floatValue().toLong
      else
        ???
    } else {
      var s = ta.toString
      if (s.contains("'")) s = s.replaceAll("'", "")
      if (s.contains("\"")) s = s.replaceAll("\"", "")
      java.lang.Long.parseLong(s)
    }
  }

  def connectTo(cid: CompartmentId) =
    parse(s"$CONNECT(${cid.name},${cid.address},'${cid.port}')")
}
