package it.unibo.bitpantarei.api.service.impl

import alice.tucson.service.TucsonNodeService
import alice.tuplecentre.api.Tuple
import it.unibo.bitpantarei.api.Conversions._
import it.unibo.bitpantarei.api.id.ServiceId
import it.unibo.bitpantarei.api.service.CallbackUser

import scala.util.{Failure, Success}

/**
 * Created by gciatto on 26/10/15.
 */
class ScalaTucsonService(private val conf: String,
                         private val portNumber: Int,
                         private val persistTempl: Tuple)
  extends TucsonNodeService(conf, portNumber, persistTempl)
  with CallbackUser {

  private lazy val serviceThread: Thread = new Thread(() => {
    try {
      install()
      callCallbackIfPreset()
    } catch {
      case t: Throwable =>
        callCallbackIfPresetWithError(t)
    }

  })

  private var installCallback: Option[Callback[ServiceId]] = None

  def this(portNumber: Int) = this(null, portNumber, null)

  def this() = this(ServiceId.DEFAULT_PORT)

  def install(f: Callback[ServiceId]): Unit = {
    installCallback = Option(f)
    if (serviceThread.isAlive) {
      callCallbackIfPresetWithError(new IllegalStateException("Service already installed"))
    } else {
      serviceThread.start()
    }
  }

  def callCallbackIfPresetWithError(t: Throwable): Unit = {
    installCallback.foreach(_ (Failure(t)))
  }

  def callCallbackIfPreset(): Unit = {
    installCallback.foreach(_ (Success(ServiceId(portNumber))))
  }
}
