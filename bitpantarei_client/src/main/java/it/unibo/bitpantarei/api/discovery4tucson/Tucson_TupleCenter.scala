package it.unibo.bitpantarei.api.discovery4tucson

import it.unibo.bitpantarei.api.service.ServiceApi
import it.unibo.mfrancia.discovery.{iTupleCenter, TupleCenter}

/**
  * Created by w4bo on 24/11/15.
  */
class Tucson_TupleCenter(var api: ServiceApi, tc: TupleCenter)
  extends iTupleCenter {

  def name: String = tc.name

  def address: String = tc.address

  def address_=(address: String) = tc._address = address

  def topic: String = tc.topic

  def topic_=(topic: String) = tc._topic = topic

  def timestamp: Long = tc.timestamp

  def port: Int = tc.port

  def location: String = tc.location

  def location_=(location: String) = tc._location = location

  override def id: String = tc.id

  def topicCriteria(topic: String): Boolean = tc.topicCriteria(topic)

  def locationCriteria(location: String): Boolean = tc.locationCriteria(location)

  def neighborCriteria(topic: String, location: String): Boolean = tc.neighborCriteria(topic, location)

  def rejectOther(tc: iTupleCenter) = tc.rejectOther(tc)

  def updateTimeStamp: Unit = tc.updateTimeStamp

  override def addOther(tc: iTupleCenter) = api.out(name, Tucson_TupleCenter.toNeighbourTuple(tc), 1000L)

  override def addSelf(tc: iTupleCenter) = api.out(name, Tucson_TupleCenter.toSelfTuple(tc), 1000L)

}

object Tucson_TupleCenter {
  def createTucsonTupleCenter(api: ServiceApi, tc: TupleCenter): Tucson_TupleCenter = {
    new Tucson_TupleCenter(api, tc)
  }

  def toNeighbourTuple(t: iTupleCenter): String =
    s"neighbor('${t.id}',${t.name},'${t.address}','${t.port}',${t.topic},${t.location},${t.timestamp})"

  def toSelfTuple(t: iTupleCenter): String =
    s"self(${t.name},'${t.address}','${t.port}')"
}