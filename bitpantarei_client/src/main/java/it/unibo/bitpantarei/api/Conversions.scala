package it.unibo.bitpantarei.api

import java.util.concurrent.{Callable, TimeUnit}
import java.util.function.Consumer

import alice.logictuple.LogicTuple
import alice.tucson.api.{ITucsonOperation, TucsonAgentId, TucsonOperationCompletionListener}
import it.unibo.bitpantarei.api.tucson.TucsonOperationCompletedAdapter

/**
 * Created by giova on 17/10/2015.
 */
object Conversions {
  implicit def toRunnable(f: () => Any): Runnable =
    new Runnable {
      override def run(): Unit = f()
    }

  implicit def toCallable[T](f: () => T): Callable[T] =
    new Callable[T] {
      override def call(): T = f()
    }

  implicit def toAgentId(s: String): TucsonAgentId =
    new TucsonAgentId(s)

  implicit def toMSecInt(t: (Int, TimeUnit)): Int =
    toMSecLong(t).toInt

  implicit def toMSecLong(t: (Int, TimeUnit)): Long =
    t._2.toMillis(t._1)

  implicit def toTucsonOperationCompletedAdapter(f: ITucsonOperation => Any): TucsonOperationCompletionListener =
    TucsonOperationCompletedAdapter(f)

  implicit def toLogicTuple(s: String): LogicTuple =
    LogicTuple.parse(s)

  implicit def toConsumer[T](f: T => Any): Consumer[T] =
    new Consumer[T] {
      override def accept(t: T): Unit = f(t)
    }

  implicit def toFunction1[T, R](f: java.util.function.Function[T, R]): T => R =
    (t: T) =>
      f.apply(t)
}
