package it.unibo.mfrancia.discovery

import java.net.{DatagramPacket, DatagramSocket, InetAddress}

/**
 * Http local daemon. This daemon is linked to a specified tuple center.
 * If the tuple center is alive then ping the (LAN) server. This daemon is NOT
 * shutdown until the tuple center is not available after "n" retries
 */

class Daemon_TCPing_Lan(tc: iTupleCenter, millisPing: Long, retriesNumber: Int) extends Daemon_TCPing("ping_lan " + tc.toString, tc, millisPing, retriesNumber) {
  val address: InetAddress = InetAddress.getByName(Syskb.UDP_BROADCAST)
  val datagramSocket: DatagramSocket = new DatagramSocket()

  override def ping = {
    val buffer: Array[Byte] = tc.toJson.getBytes
    val packet: DatagramPacket = new DatagramPacket(buffer, buffer.length, address, Syskb.LOCALDAEMON_UDP_PORT)
    datagramSocket.send(packet)
  }

  override def isTcAlive = true

}
