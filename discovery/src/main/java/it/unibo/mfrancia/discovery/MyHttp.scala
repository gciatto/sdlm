package it.unibo.mfrancia.discovery

/**
 * Utilities to make Http actions
 */

import scalaj.http._

class MyHttp[T <: Serializable](address: String) {
  def this() = this("")

  def sendGet: String = sendGet(address)

  def sendGet(address: String): String = Http(address).asString.body

  def sendGet(params: Seq[(String, String)]): String = Http(address).params(params).asString.body

  def sendGet(address: String, params: Seq[(String, String)]): String = Http(address).params(params).asString.body

  def sendPost(value: T): String = sendPost(address, value)

  def sendPost(address: String, value: T): String =
    Http(address).postData(value.toJson.stripMargin)
      .header("content-type", "application/json")
      .header("charset", "utf-8")
      .option(HttpOptions.readTimeout(10000)).asString.body

  def sendPost(value: T, param: (String, String)): String = sendPost(address, value, param)

  def sendPost(address: String, value: T, param: (String, String)): String =
    Http(address).postData(value.toJson.stripMargin).param(param._1, param._2)
      .header("content-type", "application/json")
      .header("charset", "utf-8")
      .option(HttpOptions.readTimeout(10000)).asString.body


  def sendPost(address: String, value: T, params: Seq[(String, String)]): String =
    Http(address).postData(value.toJson.stripMargin).params(params)
      .header("content-type", "application/json")
      .header("charset", "utf-8")
      .option(HttpOptions.readTimeout(10000)).asString.body

  def sendDelete: String = sendDelete(address)

  def sendDelete(address: String): String = Http(address).method("DELETE").asString.body
}
