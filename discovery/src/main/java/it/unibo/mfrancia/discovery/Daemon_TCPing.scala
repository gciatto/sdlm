package it.unibo.mfrancia.discovery

/**
 * Http local daemon. This daemon is linked to a specified tuple center.
 * If the tuple center is alive then ping the server. This daemon is NOT
 * shutdown until the tuple center is not available after "n" retries
 * @param tc tuple center
 * @param millisPing ping after "millisPing"
 * @param retriesNumber numbers of retry
 */

abstract class Daemon_TCPing(id: String, tc: iTupleCenter, millisPing: Long, retriesNumber: Int) extends Daemon("ping" + tc.toString) {
  var failures: Int = 0
  var alive = false;

  def isTcAlive: Boolean

  override def run = {
    var stop = false
    while (!isStopped && !stop) {
      if (isTcAlive /* set the right condition here */ ) {
        /* if the tc is alive then ping the server */
        tc.updateTimeStamp
        println(s"[${tc.toString},${tc.topic},${tc.location},${tc.timestamp}}] pinging")
        ping
      } else
        failures += 1
      if (failures == retriesNumber)
        stop = true
      Thread.sleep(millisPing)
    }
  }

  def ping
}
