package it.unibo.mfrancia.discovery

/**
 * Created by w4bo on 13/10/15.
 */
object Syskb {

  val START_WITH_REMOTE_DISC = true
  val REMOTE = false
  val LOCAL = true

  val UDP_BROADCAST = "255.255.255.255"

  val LOCALDAEMON_PING_MILLIS = 2000
  val LOCALDAEMON_RETRIES = 10
  val LOCALDAEMON_UDP_PORT = 20000
  val LOCALDAEMON_UDP_BUFSIZE = 1024
  val LOCALDAEMON_CLEAN_MILLIS = LOCALDAEMON_PING_MILLIS * 3 // delete after 6s

  val DNSSERVER = "http://127.0.0.1:3000/"
  val DNSSERVER_PING_MILLIS = 2000
  val DNSSERVER_RETRIES = 10
  val DNSSERVER_CLEAN_MILLIS = DNSSERVER_PING_MILLIS * 3
  // delete after 6s
  val DNSSERVER_RETRIEVE_MILLIS = DNSSERVER_PING_MILLIS * 2
  val DNSSERVER_TUPLECENTER = DNSSERVER + "tuplecenters/"

  def DNSSERVER_PING(id: String) = DNSSERVER_TUPLECENTER + id

  def DNSSERVER_SEARCH(tc: iTupleCenter) = Syskb.DNSSERVER_TUPLECENTER +
    tc.id + "/" +
    tc.topic + "/" +
    tc.location + "/"
}
