package it.unibo.bitpantarei.client.gui;

import it.unibo.bitpantarei.api.service.ServiceApi;
import it.unibo.bitpantarei.api.service.impl.MyServiceApi;

import javax.swing.*;

/**
 * Created by gciatto on 17/11/15.
 */
public class ActionServiceSetRemote extends ActionServiceSetLocalRemote {

    public ActionServiceSetRemote(ClientMainFrame clientGui) {
        super(Strings.getString("L_MODE_REMOTE"), clientGui);
    }

    @Override
    protected String message(ServiceApi obj) {
        return Strings.getString("F_SERVICE_SETTING_REMOTE", obj);
    }

    @Override
    protected void businessLogic(/*Surely not null*/ MyServiceApi api) {
        api.setLocal(false);
    }

    @Override
    protected JRadioButtonMenuItem getOther() {
        return clientGui().rdbtnmntmLocal;
    }

    @Override
    protected JRadioButtonMenuItem getThis() {
        return clientGui().rdbtnmntmRemote;
    }
}

