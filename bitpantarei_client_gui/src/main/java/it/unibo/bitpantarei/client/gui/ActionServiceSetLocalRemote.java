package it.unibo.bitpantarei.client.gui;

import it.unibo.bitpantarei.api.id.ServiceId;
import it.unibo.bitpantarei.api.service.ServiceApi;
import it.unibo.bitpantarei.api.service.impl.MyServiceApi;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.util.Optional;

/**
 * Created by gciatto on 17/11/15.
 */
public abstract class ActionServiceSetLocalRemote extends ClientAction<ServiceApi> {

    public ActionServiceSetLocalRemote(String name, ClientMainFrame clientGui) {
        super(name, clientGui);
    }

    protected abstract JRadioButtonMenuItem getThis();

    protected abstract JRadioButtonMenuItem getOther();

    @Override
    public void actionPerformed(ActionEvent e) {
        if (getThis().isSelected() && clientGui().isAnyNodeSelected()) {
            final Object selected = clientGui().getSelectedNode().getUserObject();
            if (selected instanceof ServiceId) {
                final Optional<ServiceApi> optApi = clientGui().getServerApi((ServiceId) selected);
                optApi.ifPresent(api -> {
                    if (api instanceof MyServiceApi) {
                        clientGui().updateRadioSelection(getOther(), false);
                        businessLogic((MyServiceApi) api);
                    } else {
                        clientGui().updateRadioSelection(getThis(), false);
                    }
                });
                if (!optApi.isPresent()) {
                    clientGui().updateRadioSelection(getThis(), false);
                }
            } else {
                clientGui().updateRadioSelection(getThis(), false);
            }
        } else {
            clientGui().updateRadioSelection(getThis(), true);
        }
    }

    protected abstract void businessLogic(MyServiceApi api);
}
