package it.unibo.bitpantarei.client.gui;

import it.unibo.bitpantarei.api.id.CompartmentId;
import it.unibo.bitpantarei.api.topic.Topic;
import scala.Tuple2;

import java.awt.event.ActionEvent;
import java.util.Optional;

import static it.unibo.bitpantarei.client.gui.SwingCallback.callbackWithDefaultErrorHandler;

/**
 * Created by gciatto on 25/10/15.
 */
public class ActionCompartmentSetTopic extends CompartmentAction<Topic> {

    public ActionCompartmentSetTopic(CompartmentFrame compartmentFrame) {
        super(Strings.getString("A_COMPARTMENT_SET_TOPIC"), compartmentFrame);
    }

    @Override
    protected String message(Topic obj) {
        return String.format(Strings.getString("F_SETTING_TOPIC"), compartmentGui().compartmentId, obj);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Optional<Topic> topicFromTextField = getTopicFromTextField();
        topicFromTextField.ifPresent(t -> {
            CompartmentId cid = compartmentGui().compartmentId;
            clientGui().getServerApi(cid.serverId()).ifPresent(api -> {
                putMessage(t);
                api.setCompartmentTopic(cid.name(), t, callbackWithDefaultErrorHandler(
                        this::onTopicSet,
                        th -> removeMessage(t)
                ));
            });
        });
        if (!topicFromTextField.isPresent())
            showWrongStringAlert(getRawTopicString());
    }

    protected void onTopicSet(Tuple2<CompartmentId, Topic> t) {
        removeMessage(t._2());
    }

    @Override
    protected String correctSyntax() {
        return "- | [*] | [a-z][<everyChar>]*(.[a-z][<everyChar>]*)*";
    }
}
