package it.unibo.bitpantarei.client.gui;

import it.unibo.bitpantarei.api.Conversions;
import scala.util.Try;

import javax.swing.*;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * Created by gciatto on 23/10/15.
 */
public class SwingCallback<T> implements Function<Try<T>, Object> {

    private final Consumer<T> onSuccess;
    private final Consumer<Throwable> onFailure;
    private final boolean useDefaultErrorHandler;

    public SwingCallback() {
        this(t -> {
        });
    }

    public SwingCallback(Consumer<T> onSuccess) {
        this(onSuccess, null, true);
    }

    public SwingCallback(Consumer<T> onSuccess, Consumer<Throwable> onFailure, boolean def) {
        this.onSuccess = Objects.requireNonNull(onSuccess);
        this.onFailure = onFailure;
        useDefaultErrorHandler = def;
    }

    public static <T> scala.Function1<Try<T>, Object> callback(Consumer<T> onSuccess) {
        return Conversions.toFunction1(new SwingCallback<>(onSuccess));
    }

    public static <T> scala.Function1<Try<T>, Object> callback(Consumer<T> onSuccess, Consumer<Throwable> onFailure) {
        return Conversions.toFunction1(new SwingCallback<>(onSuccess, onFailure, false));
    }

    public static <T> scala.Function1<Try<T>, Object> callbackWithDefaultErrorHandler(Consumer<T> onSuccess, Consumer<Throwable> onFailure) {
        return Conversions.toFunction1(new SwingCallback<>(onSuccess, onFailure, true));
    }

    @Override
    public Object apply(final Try<T> v1) {
        SwingUtilities.invokeLater(() -> {
            if (v1.isSuccess()) {
                onSuccess.accept(v1.get());
            } else {
                if (useDefaultErrorHandler) {
                    onUnhandledFailure(v1.failed().get());
                }
                if (onFailure != null) {
                    onFailure.accept(v1.failed().get());
                }
            }
        });

        return null;
    }

    protected void onUnhandledFailure(Throwable t) {
        t.printStackTrace();
        JOptionPane.showMessageDialog(
                null,
                t.getMessage(),
                Strings.getString("L_UNHANDLED_EXCEPTION"),
                JOptionPane.ERROR_MESSAGE
        );
    }
}
