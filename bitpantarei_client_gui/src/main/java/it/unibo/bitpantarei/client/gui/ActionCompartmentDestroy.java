package it.unibo.bitpantarei.client.gui;

import it.unibo.bitpantarei.api.id.CompartmentId;
import it.unibo.bitpantarei.api.service.ServiceApi;

import java.awt.event.ActionEvent;
import java.util.Optional;

import static it.unibo.bitpantarei.client.gui.SwingCallback.callbackWithDefaultErrorHandler;

/**
 * Created by gciatto on 23/10/15.
 */
public class ActionCompartmentDestroy extends ClientAction<CompartmentId> {

    public ActionCompartmentDestroy(ClientMainFrame clientGui) {
        super(Strings.getString("A_COMPARTMENT_DESTROY"), clientGui);
    }

    @Override
    protected String message(CompartmentId obj) {
        return String.format(Strings.getString("F_DESTROYING_COMPARTMENT"), obj);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (clientGui().isAnyNodeSelected()) {
            final Object nodeContent = clientGui().getSelectedNode().getUserObject();
            if (nodeContent instanceof CompartmentId) {
                final CompartmentId cid = (CompartmentId) nodeContent;
                final Optional<ServiceApi> serverApi = clientGui().getServerApi(cid.serverId());
                serverApi.ifPresent(api -> {
                    if (showReallySureConfirmation("destroy", cid)) {
                        putMessage(cid);
                        api.destroyCompartment(cid.name(), callbackWithDefaultErrorHandler(
                                this::onCompartmentDestroyed,
                                t -> removeMessage(cid)
                        ));
                    }
                });
            } else {
                showEmptySelectionAlert("compartment");
            }
        } else {
            showEmptySelectionAlert("compartment");
        }
    }

    private void onCompartmentDestroyed(CompartmentId cid) {
        removeMessage(cid);
        clientGui().unregisterCid(cid);
    }
}
