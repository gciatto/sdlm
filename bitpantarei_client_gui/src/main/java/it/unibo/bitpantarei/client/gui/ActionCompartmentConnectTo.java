package it.unibo.bitpantarei.client.gui;

import it.unibo.bitpantarei.api.id.CompartmentId;
import it.unibo.bitpantarei.api.service.ServiceApi;
import scala.Tuple2;

import java.awt.event.ActionEvent;
import java.util.Optional;

import static it.unibo.bitpantarei.client.gui.Strings.getString;
import static it.unibo.bitpantarei.client.gui.SwingCallback.callback;

/**
 * Created by gciatto on 26/10/15.
 */
public class ActionCompartmentConnectTo extends CompartmentAction<CompartmentId> {
    public ActionCompartmentConnectTo(CompartmentFrame compartmentFrame) {
        super(getString("L_COMPARTMENT_CONNECT_TO"), compartmentFrame);
    }

    @Override
    protected String message(CompartmentId obj) {
        return String.format(getString("F_COMPARTMENT_CONNECTING_TO"), compartmentGui().compartmentId, obj);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        CompartmentId cid = compartmentGui().compartmentId;
        Optional<CompartmentId> candidate = compartmentGui().getConnectToCandidate();
        candidate.ifPresent(other -> {
            Optional<ServiceApi> optApi = clientGui().getServerApi(cid.serverId());
            optApi.ifPresent(api -> {
                putMessage(other);
                api.connectCompartment(cid.name(), other, callback(
                        this::onConnected,
                        t -> removeMessage(other)
                ));
            });
        });
    }

    protected void onConnected(Tuple2<CompartmentId, CompartmentId> t) {
        removeMessage(t._2());
        compartmentGui().registerNewNeighbor(t._2());
    }
}
