package it.unibo.bitpantarei.client.gui;

import it.unibo.bitpantarei.api.compartment.Compartment;
import it.unibo.bitpantarei.api.compartment.CompartmentState;
import it.unibo.bitpantarei.api.compartment.MutableCompartment;
import it.unibo.bitpantarei.api.compartment.Neighborhood;
import it.unibo.bitpantarei.api.compartment.impl.CompartmentImpl;
import it.unibo.bitpantarei.api.id.CompartmentId;
import it.unibo.bitpantarei.api.id.CompartmentId$;
import it.unibo.bitpantarei.api.molecule.Molecule;
import it.unibo.bitpantarei.api.topic.Topic;
import scala.collection.JavaConversions;
import scala.collection.immutable.Set;

import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.*;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;

import static java.util.Arrays.asList;
import static java.util.Collections.unmodifiableList;
import static java.util.stream.Collectors.toList;

/**
 * Created by gciatto on 24/10/15.
 */
public class CompartmentTreeModel implements TreeModel {
    private static final String topicSection = "Topic";
    private static final String spreadProbSection = "Spreading prob.";
    private static final String interestsSection = "Interest";
    private static final List<MutableTreeNode> sections = unmodifiableList(
            asList(topicSection, spreadProbSection, interestsSection)
                    .stream()
                    .map(DefaultMutableTreeNode::new)
                    .collect(toList())
    );
    private final DefaultMutableTreeNode rootNode =
            new DefaultMutableTreeNode("Neighborhood");
    private final List<TreeModelListener> listeners = new LinkedList<>();
    private MutableCompartment compartment;

    CompartmentTreeModel() {
        this(new CompartmentImpl(CompartmentId$.MODULE$.apply()));
    }

    public CompartmentTreeModel(MutableCompartment compartment) {
        this.compartment = compartment;
    }

    private static boolean needsLoading(TreeNode n) {
        return n.isLeaf() && n.getAllowsChildren();
    }

    private void loadNeighbors(DefaultMutableTreeNode root) {
        JavaConversions.asJavaIterable(compartment.neighborhood().neighbors())
                .forEach(n -> {
                    DefaultMutableTreeNode nNode = new DefaultMutableTreeNode(n);
                    root.insert(nNode, root.getChildCount());

                    loadNeighbor(nNode);
                });
    }

    private void loadNeighbor(DefaultMutableTreeNode nNode) {
        CompartmentId n = (CompartmentId) nNode.getUserObject();

        DefaultMutableTreeNode tSNode = new DefaultMutableTreeNode(topicSection);
        tSNode.add(new DefaultMutableTreeNode(compartment.neighborhood().topic(n), false));
        nNode.add(tSNode);

        DefaultMutableTreeNode sSNode = new DefaultMutableTreeNode(spreadProbSection);
        sSNode.add(new DefaultMutableTreeNode(compartment.neighborhood().spreadingProb(n), false));
        nNode.add(sSNode);

        DefaultMutableTreeNode iSNode = new DefaultMutableTreeNode(interestsSection);
        nNode.add(iSNode);
    }

    private void invalidate(DefaultMutableTreeNode node) {
        if (!isLeaf(node)) {
            node.removeAllChildren();
        }
    }

    private void loadInterests(DefaultMutableTreeNode node) {
        CompartmentId cid = (CompartmentId) ((DefaultMutableTreeNode) node.getParent())
                .getUserObject();

        JavaConversions.asJavaCollection(compartment.neighborhood().interests(cid))
                .forEach(i -> {
                    DefaultMutableTreeNode iNode = new DefaultMutableTreeNode(i, false);
                    node.add(iNode);
                });
    }

    private void load(DefaultMutableTreeNode node) {
        if (node.equals(rootNode)) {
            loadNeighbors(rootNode);
        } else {
            Object userObj = node.getUserObject();
            if (userObj instanceof String) {
                if (userObj.equals(interestsSection)) {
                    loadInterests(node);
                } else {
                    throw new IllegalStateException();
                }
            } else if (userObj instanceof CompartmentId) {
                loadNeighbor(node);
            } else {
                throw new IllegalStateException();
            }
        }

    }

    @Override
    public Object getRoot() {
        return rootNode;
    }

    @Override
    public Object getChild(Object parent, int index) {
        DefaultMutableTreeNode pNode = ((DefaultMutableTreeNode) parent);
        loadIfNecessary(pNode);
        try {
            return pNode.getChildAt(index);
        } catch (ArrayIndexOutOfBoundsException e) {
            return null;
        }
    }

    @Override
    public int getChildCount(Object parent) {
        DefaultMutableTreeNode pNode = ((DefaultMutableTreeNode) parent);
        loadIfNecessary(pNode);
        return pNode.getChildCount();
    }

    @Override
    public boolean isLeaf(Object node) {
        DefaultMutableTreeNode pNode = ((DefaultMutableTreeNode) node);
        return pNode.isLeaf() && !pNode.getAllowsChildren();
    }

    @Override
    public void valueForPathChanged(TreePath path, Object newValue) {
        /*
        does nothing
         */
    }

    private void loadIfNecessary(DefaultMutableTreeNode node) {
        if (needsLoading(node))
            load(node);
    }

    @Override
    public int getIndexOfChild(Object parent, Object child) {
        DefaultMutableTreeNode pNode = ((DefaultMutableTreeNode) parent);
        loadIfNecessary(pNode);
        if (child instanceof TreeNode) {
            return pNode.getIndex((TreeNode) child);
        } else {
            if (isLeaf(pNode)) {
                return -1;
            } else {
                Enumeration e = pNode.children();
                for (int i = 0; e.hasMoreElements(); i++) {
                    if (((DefaultMutableTreeNode) e.nextElement()).getUserObject().equals(child))
                        return i;
                }
                return -1;
            }
        }
    }

    @Override
    public void addTreeModelListener(TreeModelListener l) {
        listeners.add(l);
    }

    protected void fireTreeStructureChanged(TreeModelEvent e) {
        listeners.forEach(l -> l.treeStructureChanged(e));
    }

    protected void fireTreeStructureChanged() {
        fireTreeStructureChanged(new TreeModelEvent(rootNode, rootNode.getUserObjectPath()));
    }

    protected void fireTreeNodesChanged(TreeModelEvent e) {
        listeners.forEach(l -> l.treeNodesChanged(e));
    }

    protected void fireTreeNodesChanged(DefaultMutableTreeNode n) {
        fireTreeNodesChanged(new TreeModelEvent(n, n.getPath()));
    }

    protected void fireTreeNodesInserted(TreeModelEvent e) {
        listeners.forEach(l -> l.treeNodesInserted(e));
    }

    protected void fireTreeNodesInserted(DefaultMutableTreeNode n) {
        fireTreeNodesInserted(new TreeModelEvent(n, n.getPath()));
    }

    protected void fireTreeNodesRemoved(TreeModelEvent e) {
        listeners.forEach(l -> l.treeNodesRemoved(e));
    }

    protected void fireTreeNodesRemoved(DefaultMutableTreeNode n, Object[] path) {
        fireTreeNodesRemoved(new TreeModelEvent(n, path == null ? n.getPath() : path));
    }

    protected List<TreeNode> treePath(TreeNode node, boolean nodeToRoot, boolean putNode) {
        final List<TreeNode> path = new LinkedList<>();
        for (TreeNode n = putNode ? node : node.getParent(); n != null; n = n.getParent()) {
            if (nodeToRoot) {
                path.add(n);
            } else {
                path.add(0, n);
            }
        }
        return path;
    }

    protected List<?> userObjectPath(TreeNode node, boolean nodeToRoot, boolean putNode) {
        return treePath(node, nodeToRoot, putNode).stream()
                .map(it -> {
                    if (it instanceof DefaultMutableTreeNode) {
                        return (DefaultMutableTreeNode) it;
                    } else {
                        throw new IllegalStateException();
                    }
                }).map(DefaultMutableTreeNode::getUserObject)
                .collect(toList());
    }

    @Override
    public void removeTreeModelListener(TreeModelListener l) {
        listeners.remove(l);
    }

    private DefaultMutableTreeNode getCompartmentNodeById(CompartmentId n) {
        return (DefaultMutableTreeNode) getChild(rootNode, getIndexOfChild(rootNode, n));
    }

    public void $minus$eq(CompartmentId cid) {
        compartment.$minus$eq(cid);
        DefaultMutableTreeNode node = getCompartmentNodeById(cid);
        Object[] path = node.getUserObjectPath();
        rootNode.remove(node);
        fireTreeNodesRemoved(node, path);
    }

    public void $minus$eq(Molecule elem) {
        compartment.$minus$eq(elem);
    }

    public void $plus$eq(CompartmentId cid) {
        compartment.$plus$eq(cid);
        rootNode.add(new DefaultMutableTreeNode(cid));
        fireTreeNodesInserted(getCompartmentNodeById(cid));
    }

    public Set<Molecule> molecules() {
        return compartment.molecules();
    }

    private DefaultMutableTreeNode getCompartmentTopicSectionNodeById(CompartmentId n) {
        return (DefaultMutableTreeNode) getCompartmentNodeById(n).getChildAt(0);
    }

    private DefaultMutableTreeNode getCompartmentSProbSectionNodeById(CompartmentId n) {
        return (DefaultMutableTreeNode) getCompartmentNodeById(n).getChildAt(1);
    }

    private DefaultMutableTreeNode getCompartmentInterestsSectionNodeById(CompartmentId n) {
        return (DefaultMutableTreeNode) getCompartmentNodeById(n).getChildAt(2);
    }

    public void $plus$eq(CompartmentId n, Topic tp) {
        compartment.$plus$eq(n, tp);
        fireTreeNodesInserted(getCompartmentNodeById(n));
        //TODO handle this
    }

    public CompartmentState state() {
        return compartment.state();
    }

    public int size() {
        return compartment.size();
    }

    public Neighborhood neighborhood() {
        return compartment.neighborhood();
    }

    public void $plus$eq(Molecule elem) {
        compartment.$plus$eq(elem);
    }

    public void $plus$eq(CompartmentId n, double sp) {
        compartment.$plus$eq(n, sp);
        //TODO handle this
    }

    public CompartmentId id() {
        return compartment.id();
    }

    public void adaptTo(Compartment c) {
        compartment = (MutableCompartment) c;
        invalidate(rootNode);
        fireTreeStructureChanged();
    }
}
