package it.unibo.bitpantarei.client.gui;

import it.unibo.bitpantarei.api.compartment.Compartment;
import it.unibo.bitpantarei.api.compartment.CompartmentState;
import it.unibo.bitpantarei.api.compartment.MutableCompartment;
import it.unibo.bitpantarei.api.compartment.Neighborhood;
import it.unibo.bitpantarei.api.compartment.impl.CompartmentImpl;
import it.unibo.bitpantarei.api.id.CompartmentId;
import it.unibo.bitpantarei.api.id.CompartmentId$;
import it.unibo.bitpantarei.api.molecule.Molecule;
import it.unibo.bitpantarei.api.molecule.Monoatomic;
import it.unibo.bitpantarei.api.molecule.atom.Atom;
import it.unibo.bitpantarei.api.topic.Topic;
import scala.collection.JavaConversions;
import scala.collection.immutable.Set;

import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import static it.unibo.bitpantarei.client.gui.Strings.getString;

/**
 * Created by gciatto on 23/10/15.
 */
public class CompartmentTableModel implements MutableCompartment, TableModel {
    private static final long serialVersionUID = 3055475651250584794L;
    private final Class[] columnTypes = new Class[]{String.class, Integer.class};
    private final List<TableModelListener> listeners = new LinkedList<>();
    private MutableCompartment compartment;
    private boolean[] columnEditables = new boolean[]{false, false};
    private String[] columnNames = new String[]{getString("L_HEADER_MOLECULE"), getString("L_HEADER_CONCENTRATION")};

    public CompartmentTableModel() {
        this(new CompartmentImpl(CompartmentId$.MODULE$.apply()));
    }

    public CompartmentTableModel(MutableCompartment compartment) {
        this.compartment = compartment;
    }

    public Class getColumnClass(int columnIndex) {
        return columnTypes[columnIndex];
    }

    public boolean isCellEditable(int row, int column) {
        return columnEditables[column];
    }

    @Override
    public void $minus$eq(CompartmentId n) {
        compartment.$minus$eq(n);
        fireTableUpdate();
    }

    @Override
    public void $plus$eq(CompartmentId n) {
        compartment.$plus$eq(n);
        fireTableUpdate();
    }

    @Override
    public void $plus$eq(CompartmentId n, double sp) {
        compartment.$plus$eq(n, sp);
        fireTableUpdate();
    }

    @Override
    public void $plus$eq(CompartmentId n, Topic tp) {
        compartment.$plus$eq(n, tp);
        fireTableUpdate();
    }

    @Override
    public void $plus$eq(Molecule elem) {
        compartment.$plus$eq(elem);
        fireTableUpdate();
    }

    @Override
    public void $minus$eq(Molecule elem) {
        compartment.$minus$eq(elem);
        fireTableUpdate();
    }

    @Override
    public CompartmentId id() {
        return compartment.id();
    }

    @Override
    public Set<Molecule> molecules() {
        return compartment.molecules();
    }

    @Override
    public CompartmentState state() {
        return compartment.state();
    }

    @Override
    public Neighborhood neighborhood() {
        return compartment.neighborhood();
    }

    @Override
    public int size() {
        return compartment.size();
    }

    @Override
    public int getRowCount() {
        return size();
    }

    @Override
    public int getColumnCount() {
        return columnTypes.length;
    }

    @Override
    public String getColumnName(int columnIndex) {
        return columnNames[columnIndex];
    }

    protected Optional<Molecule> getAt(int rowIndex) {
        return JavaConversions.asJavaCollection(compartment.molecules())
                .stream().skip(rowIndex).findFirst();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Optional<Molecule> m = getAt(rowIndex);

        if (m.isPresent()) {
            return columnIndex == 0 ?
                    m.get().atoms().head() :
                    m.get().concentration();
        } else {
            throw new IllegalStateException();
        }
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        Optional<Molecule> m = getAt(rowIndex);
        Monoatomic m1;
        if (m.isPresent()) {
            if (columnIndex == 0) {
                m1 = new Monoatomic((Atom) aValue, m.get().concentration());
            } else {
                m1 = new Monoatomic(m.get().atoms().head(), (Integer) aValue);
            }
            compartment.$minus$eq(m.get());
            compartment.$plus$eq(m1);
            fireTableUpdate();
        } else {
            throw new IllegalStateException();
        }
    }

    @Override
    public void addTableModelListener(TableModelListener l) {
        listeners.add(l);
    }

    public void fireTableUpdate() {
        fireTableChanged(new TableModelEvent(this));
    }

    public void fireRowUpdate(int rowIndex) {
        fireTableChanged(new TableModelEvent(this, rowIndex));
    }

    public void fireRowsRangeUpdate(int fromRowIndex, int toRowIndex) {
        fireTableChanged(new TableModelEvent(this, fromRowIndex, toRowIndex));
    }

    public void fireColumnSectionUpdate(int fromRowIndex, int toRowIndex, int column) {
        fireTableChanged(new TableModelEvent(this, fromRowIndex, toRowIndex, column));
    }

    public void fireTableChanged(TableModelEvent e) {
        listeners.forEach(it -> it.tableChanged(e));
    }

    @Override
    public void removeTableModelListener(TableModelListener l) {
        listeners.remove(l);
    }

    public void adaptTo(Compartment compartment) {
        this.compartment = (MutableCompartment) compartment;
        fireTableUpdate();
    }
}
