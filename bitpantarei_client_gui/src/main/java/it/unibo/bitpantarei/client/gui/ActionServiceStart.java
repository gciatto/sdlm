package it.unibo.bitpantarei.client.gui;

import it.unibo.bitpantarei.api.id.ServiceId;

import javax.swing.*;
import java.awt.event.ActionEvent;

import static it.unibo.bitpantarei.client.gui.SwingCallback.callbackWithDefaultErrorHandler;

/**
 * Created by gciatto on 23/10/15.
 */
public class ActionServiceStart extends ClientAction<ServiceId> {

    public ActionServiceStart(ClientMainFrame clientGui) {
        super(Strings.getString("A_SERVICE_START"), clientGui);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        IdDialog.showForSid(clientGui()).ifPresent(sid -> {
            if (clientGui().getServerApi(sid).isPresent()) {
                showServerAlreadyExistAlert(sid);
            } else {
                putMessage(sid);
                clientGui().newServerApi(sid)
                        .start(callbackWithDefaultErrorHandler(
                                this::onServerStarted,
                                t -> removeMessage(sid)
                        ));
            }
        });
    }

    private void onServerStarted(ServiceId sid) {
        clientGui().registerSid(sid);
        removeMessage(sid);
    }

    @Override
    protected String message(ServiceId sid) {
        return String.format(Strings.getString("F_CONNETTING_TO"), sid);
    }

    protected void showServerAlreadyExistAlert(ServiceId sid) {
        JOptionPane.showMessageDialog(
                clientGui(),
                String.format(Strings.getString("F_SERVICE_ALREADY_EXISTS"), sid),
                Strings.getString("L_SERVICE_ALREADY_EXISTS"),
                JOptionPane.ERROR_MESSAGE
        );
    }
}
