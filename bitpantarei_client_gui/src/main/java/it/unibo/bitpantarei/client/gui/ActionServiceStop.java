package it.unibo.bitpantarei.client.gui;

import it.unibo.bitpantarei.api.id.ServiceId;

import java.awt.event.ActionEvent;

import static it.unibo.bitpantarei.client.gui.SwingCallback.callbackWithDefaultErrorHandler;

/**
 * Created by gciatto on 23/10/15.
 */
public class ActionServiceStop extends ClientAction<ServiceId> {

    public ActionServiceStop(ClientMainFrame clientGui) {
        super(Strings.getString("A_SERVICE_STOP"), clientGui);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (clientGui().isAnyNodeSelected()) {
            final Object nodeContent = clientGui().getSelectedNode().getUserObject();
            if (nodeContent instanceof ServiceId) {
                final ServiceId sid = (ServiceId) nodeContent;
                if (showReallySureConfirmation("stop", sid)) {
                    putMessage(sid);
                    clientGui().getServerApi(sid).ifPresent(api ->
                            api.stop(callbackWithDefaultErrorHandler(
                                    this::onServerStopped,
                                    t -> removeMessage(sid)
                            ))
                    );
                }
            } else {
                showEmptySelectionAlert("service");
            }
        } else {
            showEmptySelectionAlert("service");
        }
    }

    private void onServerStopped(ServiceId sid) {
        clientGui().unregisterSid(sid);
        removeMessage(sid);
    }

    @Override
    protected String message(ServiceId sid) {
        return String.format(Strings.getString("F_DISCONNETTING_FROM"), sid);
    }
}
