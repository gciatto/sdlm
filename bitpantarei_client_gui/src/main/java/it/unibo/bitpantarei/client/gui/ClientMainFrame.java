package it.unibo.bitpantarei.client.gui;

import it.unibo.bitpantarei.api.compartment.Compartment;
import it.unibo.bitpantarei.api.id.CompartmentId;
import it.unibo.bitpantarei.api.id.ServiceId;
import it.unibo.bitpantarei.api.service.ServiceApi;
import it.unibo.bitpantarei.api.service.impl.MyServiceApi;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.tree.*;
import java.awt.*;
import java.util.*;

import static it.unibo.bitpantarei.client.gui.Strings.getString;

public class ClientMainFrame extends JFrame {

    /**
     *
     */
    private static final long serialVersionUID = -5759159867975246955L;
    final Map<ServiceId, DefaultMutableTreeNode> servicesDirectAccess = new HashMap<>();
    final Map<CompartmentId, DefaultMutableTreeNode> compartmentsDirectAccess = new HashMap<>();
    private final JMenuItem mntmStartInspection;
    public Map<CompartmentId, CompartmentFrame> compartmentFrames = new HashMap<>();
    JPanel contentPane;
    JTree trServices;
    JMenuItem mntmStart;
    JMenuItem mntmDiscovery;
    JMenuItem mntmCompartments;
    JMenuItem mntmStop;
    JMenuItem mntmExit;
    JMenuItem mntmCreate;
    JMenuItem mntmShow;
    JMenuItem mntmStopInspection;
    JMenuItem mntmDestroy;
    JRadioButtonMenuItem rdbtnmntmLocal;
    JRadioButtonMenuItem rdbtnmntmRemote;
    Panel statusBar;
    JLabel lblStatusMessage;
    JProgressBar progressBar;
    MessagesSet messages = new MessagesSet();
    DefaultMutableTreeNode trRoot = new DefaultMutableTreeNode(getString("L_SERVICES"));
    DefaultTreeModel trModel = new DefaultTreeModel(trRoot);
    DefaultTreeSelectionModel trSelectModel = new DefaultTreeSelectionModel();
    Map<ServiceId, ServiceApi> serverApis = new HashMap<>();

    /**
     * Create the frame.
     */
    public ClientMainFrame() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 450, 300);

        setTitle(getString("L_TITLE"));

        JMenuBar menuBar = new JMenuBar();
        setJMenuBar(menuBar);

        JMenu mnService = new JMenu(getString("L_SERVICES")); //$NON-NLS-1$
        menuBar.add(mnService);

        mntmStart = new JMenuItem(getString("A_SERVICE_START")); //$NON-NLS-1$
        mntmStart.setAction(new ActionServiceStart(this));
        mnService.add(mntmStart);

        JSeparator separator_3 = new JSeparator();
        mnService.add(separator_3);

        mntmDiscovery = new JMenuItem(getString("A_SERVICES_DISCOVERY")); //$NON-NLS-1$
        mntmDiscovery.setEnabled(false);
        mnService.add(mntmDiscovery);

        final JMenu mnMode = new JMenu(getString("L_MODE")); //$NON-NLS-1$
        mnService.add(mnMode);

        rdbtnmntmLocal = new JRadioButtonMenuItem(getString("L_MODE_LOCAL")); //$NON-NLS-1$
        rdbtnmntmLocal.setAction(new ActionServiceSetLocal(this));
//        rdbtnmntmLocal.setSelected(!Syskb.START_WITH_REMOTE_DISC());
        mnMode.add(rdbtnmntmLocal);

        rdbtnmntmRemote = new JRadioButtonMenuItem(getString("L_MODE_REMOTE")); //$NON-NLS-1$
        rdbtnmntmRemote.setAction(new ActionServiceSetRemote(this));
//        rdbtnmntmRemote.setSelected(Syskb.START_WITH_REMOTE_DISC());
        mnMode.add(rdbtnmntmRemote);

        mntmCompartments = new JMenuItem(getString("A_SERVICE_GET_COMPARTMENTS")); //$NON-NLS-1
        mntmCompartments.setAction(new ActionServiceGetCompartments(this));
        mnService.add(mntmCompartments);

        JSeparator separator_4 = new JSeparator();
        mnService.add(separator_4);

        mntmStop = new JMenuItem(getString("A_SERVICE_STOP")); //$NON-NLS-1$
        mntmStop.setAction(new ActionServiceStop(this));
        mnService.add(mntmStop);

        JSeparator separator = new JSeparator();
        mnService.add(separator);

        mntmExit = new JMenuItem(getString("A_EXIT")); //$NON-NLS-1$
        mntmExit.setAction(new ExitAction(this));
        mnService.add(mntmExit);

        JMenu mnCompartment = new JMenu(getString("L_COMPARTMENT")); //$NON-NLS-1$
        menuBar.add(mnCompartment);

        mntmCreate = new JMenuItem(getString("A_COMPARTMENT_CREATE")); //$NON-NLS-1$
        mntmCreate.setAction(new ActionCompartmentCreate(this));
        mnCompartment.add(mntmCreate);

        JSeparator separator_1 = new JSeparator();
        mnCompartment.add(separator_1);

        mntmStartInspection = new JMenuItem(getString("A_COMPARTMENT_START_INSPECTION"));
        mntmStartInspection.setAction(new ActionCompartmentStartInspection(this));
        mnCompartment.add(mntmStartInspection);

        mntmShow = new JMenuItem(getString("A_COMPARTMENT_SHOW")); //$NON-NLS-1$
        mntmShow.setAction(new ActionCompartmentShowFrame(this));
        mnCompartment.add(mntmShow);

        mntmStopInspection = new JMenuItem(getString("A_COMPARTMENT_STOP_INSPECTION")); //$NON-NLS-1$
        mnCompartment.add(mntmStopInspection);

        JSeparator separator_2 = new JSeparator();
        mnCompartment.add(separator_2);

        mntmDestroy = new JMenuItem(getString("A_COMPARTMENT_DESTROY")); //$NON-NLS-1$
        mntmDestroy.setAction(new ActionCompartmentDestroy(this));
        mnCompartment.add(mntmDestroy);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        contentPane.setLayout(new BorderLayout(0, 0));
        setContentPane(contentPane);

        trServices = new JTree(trModel);
        trSelectModel.setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
        trServices.setSelectionModel(trSelectModel);
        contentPane.add(new JScrollPane(trServices), BorderLayout.CENTER);
        trServices.addMouseListener(new ServiceTreeMouseAdapter(this));

        statusBar = new Panel();
        contentPane.add(statusBar, BorderLayout.SOUTH);
        statusBar.setLayout(new BorderLayout(0, 0));

        JSeparator separator_5 = new JSeparator();
        statusBar.add(separator_5, BorderLayout.NORTH);

        lblStatusMessage = new JLabel(Strings.getString("L_READY")); //$NON-NLS-1$
        statusBar.add(lblStatusMessage, BorderLayout.CENTER);

        progressBar = new JProgressBar();
        progressBar.setIndeterminate(true);
        progressBar.setVisible(false);
        statusBar.add(progressBar, BorderLayout.WEST);
    }

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(() -> {
            try {
                ClientMainFrame frame = new ClientMainFrame();
                frame.setVisible(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    public static TreePath treePath(Object... nodes) {
        return new TreePath(nodes);
    }

    void updateRadioSelection(JRadioButtonMenuItem item, boolean selected) {
        item.setEnabled(false);
        item.setSelected(selected);
        item.setEnabled(true);
    }

    void expandServiceTree() {
        for (int i = 0; i < trServices.getRowCount(); i++) {
            trServices.expandRow(i);
        }
    }

    public void registerSid(ServiceId sid) {
        DefaultMutableTreeNode newSid = new DefaultMutableTreeNode(sid);
        trModel.insertNodeInto(newSid, trRoot, 0);
        servicesDirectAccess.put(sid, newSid);
        expandServiceTree();
    }

    public void registerCid(CompartmentId cid) {
        DefaultMutableTreeNode parentNode = servicesDirectAccess.get(cid.serverId());
        DefaultMutableTreeNode newCid = new DefaultMutableTreeNode(cid, false);
        trModel.insertNodeInto(newCid, parentNode, 0);
        compartmentsDirectAccess.put(cid, newCid);
        expandServiceTree();
    }

    public void unregisterSid(ServiceId sid) {
        getCompartments().stream()
                .filter(c -> c.serverId().equals(sid))
                .forEach(this::unregisterCid);
        trModel.removeNodeFromParent(servicesDirectAccess.get(sid));
        servicesDirectAccess.remove(sid);
        serverApis.remove(sid);
        expandServiceTree();
    }

    public void unregisterCid(CompartmentId cid) {
        trModel.removeNodeFromParent(compartmentsDirectAccess.get(cid));
        compartmentsDirectAccess.remove(cid);
        this.compartmentFrames.remove(cid);
        expandServiceTree();
    }

    public void setSidCompartments(ServiceId sid, Iterable<CompartmentId> cids) {
        DefaultMutableTreeNode parentNode = servicesDirectAccess.get(sid);
        parentNode.removeAllChildren();
        cids.forEach(cid -> {
            if (sid.equals(cid.serverId()))
                registerCid(cid);
            else
                throw new IllegalStateException();
        });
        expandServiceTree();
    }

    public Collection<CompartmentId> getCompartments() {
        return compartmentsDirectAccess.keySet();
    }

    public ServiceApi newServerApi(ServiceId sid) {
        final ServiceApi newApi = /*new ServiceApiImpl(sid); */new MyServiceApi(sid);
        serverApis.put(sid, newApi);
        return newApi;
    }

    public DefaultMutableTreeNode getSelectedNode() {
        TreePath selectionPath = trSelectModel.getSelectionPath();
        return (DefaultMutableTreeNode) selectionPath.getLastPathComponent();
    }

    public boolean isAnyNodeSelected() {
        return !trSelectModel.isSelectionEmpty();
    }

    public Optional<ServiceApi> getServerApi(ServiceId sid) {
        return Optional.ofNullable(serverApis.get(sid));
    }

    public Optional<CompartmentFrame> newCompartmentFrame(CompartmentId cid) {
        CompartmentFrame cf = new CompartmentFrame(this, cid);
        if (compartmentFrames.put(cid, cf) != null)
            return Optional.empty();
        return Optional.of(cf);
    }

    public boolean isCompartmentInspected(CompartmentId cid) {
        return compartmentFrames.containsKey(cid);
    }

    public Optional<CompartmentFrame> getCompartmentFrame(CompartmentId cid) {
        return Optional.ofNullable(compartmentFrames.get(cid));
    }

    public Optional<CompartmentFrame> removeCompartmentFrame(Compartment id) {
        return Optional.ofNullable(compartmentFrames.remove(id));
    }

    class MessagesSet {
        final Deque<String> messages = new LinkedList<>();

        boolean put(String msg) {
            messages.addFirst(msg);
            onEdit();
            return messages.size() > 0;
        }

        boolean done(String msg) {
            messages.removeFirstOccurrence(msg);
            onEdit();
            return messages.size() == 0;
        }

        String get() {
            return messages.getFirst();
        }

        void onEdit() {
            if (messages.size() > 0) {
                progressBar.setVisible(true);
                lblStatusMessage.setText(get());
            } else {
                progressBar.setVisible(false);
                lblStatusMessage.setText(getString("L_READY"));
            }
        }
    }
}