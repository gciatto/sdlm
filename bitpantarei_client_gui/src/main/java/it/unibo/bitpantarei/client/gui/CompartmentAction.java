package it.unibo.bitpantarei.client.gui;

import it.unibo.bitpantarei.api.molecule.Molecule;
import it.unibo.bitpantarei.api.molecule.Monoatomic;
import it.unibo.bitpantarei.api.molecule.atom.Atom$;
import it.unibo.bitpantarei.api.topic.Topic;

import javax.swing.*;
import java.lang.ref.WeakReference;
import java.util.Optional;

/**
 * Created by gciatto on 23/10/15.
 */
public abstract class CompartmentAction<T> extends ClientAction<T> {
    protected final WeakReference<CompartmentFrame> compartmentGui;

    public CompartmentAction(String name, CompartmentFrame compartmentFrame) {
        super(name, compartmentFrame.parent.get());
        compartmentGui = new WeakReference<>(compartmentFrame);
    }

    protected CompartmentFrame compartmentGui() {
        return compartmentGui.get();
    }

    protected Optional<Molecule> getMoleculeFromTextFiled(boolean mulplicity) {
        final String raw = compartmentGui().txtInsert.getText();
        if (raw == null || raw.isEmpty()) {
            return Optional.empty();
        } else {
            if (CompartmentFrame.verifyStringForPut(raw)) {
                return Optional.of(new Monoatomic(
                        Atom$.MODULE$.apply(raw),
                        mulplicity ? getMoleculeMulplicityFromSpinner() : 1
                ));
            } else {
                return Optional.empty();
            }
        }
    }

    protected Optional<Topic> getTopicFromTextField() {
        final String raw = getRawTopicString();
        if (raw == null || raw.isEmpty()) {
            return Optional.empty();
        } else {
            return CompartmentFrame.verifyStringForTopic(raw);
        }
    }

    protected String getRawTopicString() {
        return compartmentGui().txtTopic.getText();
    }

    protected String getRawMoleculeString() {
        return compartmentGui().txtInsert.getText();
    }

    protected int getMoleculeMulplicityFromSpinner() {
        return (Integer) compartmentGui().spinnerMoleculeAmount.getValue();
    }

    protected void showWrongStringAlert(String str) {
        JOptionPane.showMessageDialog(
                clientGui(),
                String.format(Strings.getString("F_INVALID_STRING"), str, correctSyntax()),
                Strings.getString("L_INVALID_STRING"),
                JOptionPane.ERROR_MESSAGE
        );
    }

    protected String correctSyntax() {
        return "NOT DEFINED";
    }
}
