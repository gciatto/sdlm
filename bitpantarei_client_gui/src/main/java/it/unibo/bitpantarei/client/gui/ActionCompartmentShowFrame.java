package it.unibo.bitpantarei.client.gui;

import it.unibo.bitpantarei.api.id.CompartmentId;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.util.Optional;

/**
 * Created by gciatto on 23/10/15.
 */
public class ActionCompartmentShowFrame extends ClientAction<CompartmentId> {

    public ActionCompartmentShowFrame(ClientMainFrame clientGui) {
        super(Strings.getString("A_COMPARTMENT_SHOW"), clientGui);
    }

    @Override
    protected String message(CompartmentId obj) {
        return String.format("");
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (clientGui().isAnyNodeSelected()) {
            final Object nodeContent = clientGui().getSelectedNode().getUserObject();
            if (nodeContent instanceof CompartmentId) {
                final CompartmentId cid = (CompartmentId) nodeContent;
                Optional<CompartmentFrame> optFrame = clientGui().getCompartmentFrame(cid);
                optFrame.ifPresent(frame -> frame.setVisible(true));
                if (!optFrame.isPresent()) {
                    showNotInspectedAlert(cid);
                }
            } else {
                showEmptySelectionAlert("compartment");
            }
        } else {
            showEmptySelectionAlert("compartment");
        }
    }

    protected void showNotInspectedAlert(CompartmentId cid) {
        JOptionPane.showMessageDialog(
                clientGui(),
                String.format(Strings.getString("F_NOT_INSPECTED"), cid),
                Strings.getString("L_NOT_INSPECTED"),
                JOptionPane.ERROR_MESSAGE
        );
    }
}
