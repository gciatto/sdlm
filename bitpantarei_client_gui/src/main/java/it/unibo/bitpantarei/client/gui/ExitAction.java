package it.unibo.bitpantarei.client.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

/**
 * Created by gciatto on 23/10/15.
 */
public class ExitAction extends AbstractAction {

    final Window window;

    public ExitAction(Window window) {
        super(Strings.getString("A_EXIT"));
        this.window = window;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        window.setVisible(false);
        window.dispose();
        System.exit(0);
    }
}
