package it.unibo.bitpantarei.client.gui;

import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.lang.ref.WeakReference;
import java.util.EventObject;

/**
 * Created by gciatto on 25/10/15.
 */
public abstract class ClientMouseAdapter extends MouseAdapter {
    protected final WeakReference<ClientMainFrame> clientGui;

    public ClientMouseAdapter(ClientMainFrame clientGui) {
        this.clientGui = new WeakReference<>(clientGui);
    }

    protected ClientMainFrame clientGui() {
        return clientGui.get();
    }

    protected ActionEvent toActionEvent(EventObject e) {
        return new ActionEvent(e.getSource(), 0, "");
    }
}
