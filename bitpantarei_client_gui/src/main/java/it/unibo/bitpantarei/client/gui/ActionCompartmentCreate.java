package it.unibo.bitpantarei.client.gui;

import it.unibo.bitpantarei.api.id.CompartmentId;
import it.unibo.bitpantarei.api.id.ServiceId;
import it.unibo.bitpantarei.api.service.ServiceApi;

import java.awt.event.ActionEvent;
import java.util.Optional;

import static it.unibo.bitpantarei.client.gui.SwingCallback.callbackWithDefaultErrorHandler;

/**
 * Created by gciatto on 23/10/15.
 */
public class ActionCompartmentCreate extends ClientAction<CompartmentId> {

    public ActionCompartmentCreate(ClientMainFrame clientGui) {
        super(Strings.getString("A_COMPARTMENT_CREATE"), clientGui);
    }

    @Override
    protected String message(CompartmentId obj) {
        return String.format(Strings.getString("F_CREATING_COMPARTMENT"), obj);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (clientGui().isAnyNodeSelected()) {
            final Object nodeContent = clientGui().getSelectedNode().getUserObject();
            if (nodeContent instanceof ServiceId) {
                final ServiceId sid = (ServiceId) nodeContent;
                createCompartment(sid);
            } else {
                createCompartment(null);
            }
        } else {
            createCompartment(null);
        }
    }

    private void createCompartment(ServiceId sid) {
        IdDialog.showForCid(clientGui(), sid).ifPresent(cid -> {
            final ServiceId actualSid = cid.serverId();
            final Optional<ServiceApi> serverApi = clientGui().getServerApi(actualSid);
            serverApi.ifPresent(api -> {
                        putMessage(cid);
                        api.createCompartment(cid.name(), callbackWithDefaultErrorHandler(
                                this::onCompartmentCreated,
                                t -> removeMessage(cid)
                        ));
                    }
            );
            if (!serverApi.isPresent()) {
                showServerNotFoundAlert(actualSid);
            }
        });

    }

    private void onCompartmentCreated(CompartmentId cid) {
        removeMessage(cid);
        clientGui().registerCid(cid);
    }
}
