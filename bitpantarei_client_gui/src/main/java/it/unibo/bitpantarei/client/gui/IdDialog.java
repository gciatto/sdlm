package it.unibo.bitpantarei.client.gui;

import it.unibo.bitpantarei.api.id.CompartmentId;
import it.unibo.bitpantarei.api.id.ServiceId;
import it.unibo.bitpantarei.api.id.ServiceId$;
import it.unibo.bitpantarei.api.id.impl.CompartmentIdImpl;
import it.unibo.bitpantarei.api.id.impl.ServiceIdImpl;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.util.Optional;

import static it.unibo.bitpantarei.client.gui.Strings.getString;

public class IdDialog extends JDialog {

    /**
     *
     */
    private static final long serialVersionUID = 5140733488604109157L;
    private static int cidCount = 0;

    private final JPanel contentPanel = new JPanel();
    private final boolean isCid;
    private JTextField txtName;
    private JTextField txtAddress;
    private ServiceId sid;
    private CompartmentId cid;
    private JSpinner txtPort;

    protected IdDialog(Frame owner, boolean cid, ServiceId sid) {
        super(owner);

        isCid = cid;
        setBounds(100, 100, 544, 106);
        setTitle(cid ? getString("L_CID_DIALOG_TITLE") : getString("L_SID_DIALOG_TITLE"));
        setModalityType(ModalityType.TOOLKIT_MODAL);
        getContentPane().setLayout(new BorderLayout());
        contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        getContentPane().add(contentPanel, BorderLayout.CENTER);
        contentPanel.setLayout(new BoxLayout(contentPanel, BoxLayout.X_AXIS));
        {
            txtName = new JTextField();
            txtName.setHorizontalAlignment(SwingConstants.CENTER);
            txtName.setText(String.format(getString("F_COMPARTMENT_NAME"), cidCount));
            contentPanel.add(txtName);
            txtName.setVisible(cid);
        }
        {
            JLabel lblAt = new JLabel(getString("L_AT"));
            lblAt.setHorizontalAlignment(SwingConstants.CENTER);
            contentPanel.add(lblAt);
            lblAt.setVisible(cid);
        }
        {
            txtAddress = new JTextField();
            txtAddress.setHorizontalAlignment(SwingConstants.CENTER);
            txtAddress.setText(
                    sid == null ? ServiceId$.MODULE$.DEFAULT_ADDRESS() : sid.address()
            );
            contentPanel.add(txtAddress);
        }
        {
            JLabel lblOn = new JLabel(getString("L_ON"));
            lblOn.setHorizontalAlignment(SwingConstants.CENTER);
            contentPanel.add(lblOn);
        }
        {
            txtPort = new JSpinner();
            txtPort.setModel(new SpinnerNumberModel(
                    sid == null ? ServiceId$.MODULE$.DEFAULT_PORT() : sid.port(),
                    1 << 10,
                    (1 << 16) - 1,
                    1
            ));
            contentPanel.add(txtPort);
        }
        {
            JPanel buttonPane = new JPanel();
            buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
            getContentPane().add(buttonPane, BorderLayout.SOUTH);
            {
                JButton okButton = new JButton(getString("L_OK"));
                okButton.setActionCommand(getString("L_OK"));
                buttonPane.add(okButton);
                okButton.addActionListener(arg -> onOk());
                getRootPane().setDefaultButton(okButton);
            }
            {
                JButton cancelButton = new JButton(getString("L_CANCEL"));
                cancelButton.setActionCommand(getString("L_CANCEL"));
                cancelButton.addActionListener(arg -> onCancel());
                buttonPane.add(cancelButton);
            }
        }
    }

    protected static IdDialog show(Frame parent, boolean cid, ServiceId sid) {
        IdDialog dialog = new IdDialog(parent, cid, sid);
        dialog.pack();
        dialog.setVisible(true);
        return dialog;
    }

    public static Optional<ServiceId> showForSid(Frame parent, ServiceId sid) {
        return show(parent, false, sid).getSid();
    }

    public static Optional<ServiceId> showForSid(Frame parent) {
        return showForSid(parent, null);
    }

    public static Optional<CompartmentId> showForCid(Frame parent, ServiceId sid) {
        return show(parent, true, sid).getCid();
    }

    public static Optional<CompartmentId> showForCid(Frame parent) {
        return showForCid(parent, null);
    }

    private void onOk() {
        String name = txtName.getText();
        String addr = txtAddress.getText();
        int port = (Integer) txtPort.getValue();

        cid = new CompartmentIdImpl(name, addr, port);

        try {
            cid.toTupleCentreId();
            sid = new ServiceIdImpl(addr, port);

            if (isCid && name.equals(String.format(getString("F_COMPARTMENT_NAME"), cidCount)))
                cidCount++;

            setVisible(false);
        } catch (Exception e) {
            showInvalidCidAlert();
        }
    }

    protected void showInvalidCidAlert() {
        JOptionPane.showMessageDialog(
                this,
                String.format(Strings.getString("L_INVALID_CID")),
                Strings.getString("L_INVALID_CID"),
                JOptionPane.ERROR_MESSAGE
        );
    }

    @Override
    public void setVisible(boolean b) {
        if (b) {
            cid = null;
            sid = null;
        }
        super.setVisible(b);
    }

    private void onCancel() {
        setVisible(false);
    }

    public Optional<ServiceId> getSid() {
        return Optional.ofNullable(sid);
    }

    public Optional<CompartmentId> getCid() {
        return Optional.ofNullable(cid);
    }
}