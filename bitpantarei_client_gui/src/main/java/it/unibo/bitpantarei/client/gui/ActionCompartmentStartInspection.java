package it.unibo.bitpantarei.client.gui;

import it.unibo.bitpantarei.api.id.CompartmentId;
import it.unibo.bitpantarei.api.service.ServiceApi;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.util.Optional;

/**
 * Created by gciatto on 23/10/15.
 */
public class ActionCompartmentStartInspection extends ClientAction<CompartmentId> {

    public ActionCompartmentStartInspection(ClientMainFrame clientGui) {
        super(Strings.getString("A_COMPARTMENT_START_INSPECTION"), clientGui);
    }

    @Override
    protected String message(CompartmentId obj) {
        return String.format(Strings.getString("F_STARTING_INSPECTION"), obj);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (clientGui().isAnyNodeSelected()) {
            final Object nodeContent = clientGui().getSelectedNode().getUserObject();
            if (nodeContent instanceof CompartmentId) {
                final CompartmentId cid = (CompartmentId) nodeContent;
                final Optional<ServiceApi> serverApi = clientGui().getServerApi(cid.serverId());
                if (clientGui().isCompartmentInspected(cid)) {
                    showAlreadyInspectedAler(cid);
                } else {
                    serverApi.ifPresent(api -> {
                                Optional<CompartmentFrame> optFrame = clientGui().newCompartmentFrame(cid);
                                optFrame.ifPresent(frame -> {
                                    api.startCompartmentInspection(cid.name(), frame.callback);
                                    frame.setVisible(true);
                                });
                                if (!optFrame.isPresent()) {
                                    throw new IllegalStateException();
                                }
                            }
                    );
                }
            } else {
                showEmptySelectionAlert("compartment");
            }
        } else {
            showEmptySelectionAlert("compartment");
        }
    }

    protected void showAlreadyInspectedAler(CompartmentId cid) {
        JOptionPane.showMessageDialog(
                clientGui(),
                String.format(Strings.getString("F_COMPARTMENT_ALREADY_INSPECTED"), cid),
                Strings.getString("L_COMPARTMENT_ALREADY_INSPECTED"),
                JOptionPane.ERROR_MESSAGE
        );
    }
}
