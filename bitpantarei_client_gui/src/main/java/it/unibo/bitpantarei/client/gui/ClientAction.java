package it.unibo.bitpantarei.client.gui;

import it.unibo.bitpantarei.api.id.ServiceId;

import javax.swing.*;
import java.lang.ref.WeakReference;

/**
 * Created by gciatto on 23/10/15.
 */
public abstract class ClientAction<T> extends AbstractAction {
    protected final WeakReference<ClientMainFrame> clientGui;

    public ClientAction(String name, ClientMainFrame clientGui) {
        super(name);
        this.clientGui = new WeakReference<>(clientGui);
    }

    protected ClientMainFrame clientGui() {
        return clientGui.get();
    }

    protected void putMessage(T obj) {
        clientGui().messages.put(message(obj));
    }

    protected void removeMessage(T obj) {
        clientGui().messages.done(message(obj));
    }

    protected abstract String message(T obj);

    protected void showEmptySelectionAlert(String what) {
        JOptionPane.showMessageDialog(
                clientGui(),
                String.format(Strings.getString("F_EMPTY_SELECTED"), what),
                Strings.getString("L_EMPTY_SELECTION"),
                JOptionPane.ERROR_MESSAGE
        );
    }

    protected void showServerNotFoundAlert(ServiceId sid) {
        JOptionPane.showMessageDialog(
                clientGui(),
                String.format(Strings.getString("F_INVALID_SERVICE"), sid),
                Strings.getString("L_INVALID_SERVICE"),
                JOptionPane.ERROR_MESSAGE
        );
    }

    protected boolean showReallySureConfirmation(String verb, Object obj) {
        return JOptionPane.showConfirmDialog(
                clientGui(),
                String.format(Strings.getString("F_REALLY_SURE"), verb, obj),
                Strings.getString("L_REALLY_SURE"),
                JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION;
    }
}
