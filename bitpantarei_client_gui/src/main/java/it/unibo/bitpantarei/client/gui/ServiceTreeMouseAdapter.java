package it.unibo.bitpantarei.client.gui;

import it.unibo.bitpantarei.api.id.CompartmentId;
import it.unibo.bitpantarei.api.id.ServiceId;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;
import java.awt.event.MouseEvent;

/**
 * Created by gciatto on 25/10/15.
 */
public class ServiceTreeMouseAdapter extends ClientMouseAdapter {

    private final ActionCompartmentCreate createCompartment = new ActionCompartmentCreate(clientGui());
    private final ActionCompartmentDestroy destroyCompartment = new ActionCompartmentDestroy(clientGui());
    private final ActionServiceStart startService = new ActionServiceStart(clientGui());
    private final ActionServiceStop stopService = new ActionServiceStop(clientGui());
    private final ActionCompartmentStartInspection inspectCompartment = new ActionCompartmentStartInspection(clientGui());
    private final ActionCompartmentShowFrame showCompartmentFrame = new ActionCompartmentShowFrame(clientGui());

    public ServiceTreeMouseAdapter(ClientMainFrame clientGui) {
        super(clientGui);
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        TreePath trPath = clientGui().trServices.getPathForLocation(e.getX(), e.getY());
        if (trPath == null) {
            clientGui().trSelectModel.clearSelection();
        } else {
            clientGui().trSelectModel.setSelectionPath(trPath);
        }
        if (clientGui().isAnyNodeSelected()) {
            DefaultMutableTreeNode node = clientGui().getSelectedNode();
            Object userObject = node.getUserObject();
            if (node.isRoot()) {
                if (e.getButton() == MouseEvent.BUTTON1 && e.getClickCount() >= 2)
                    onServicesFirstButtonDoubleClick(e);
            } else if (userObject instanceof ServiceId) {
                if (e.getButton() == MouseEvent.BUTTON1 && e.getClickCount() >= 2)
                    onServiceFirstButtonDoubleClick(e, (ServiceId) userObject);
                else if (e.getButton() == MouseEvent.BUTTON3 && e.getClickCount() >= 2)
                    onServiceSecondButtonDoubleClick(e, (ServiceId) userObject);
            } else if (userObject instanceof CompartmentId) {
                if (e.getButton() == MouseEvent.BUTTON1 && e.getClickCount() >= 2)
                    onCompartmentFirstButtonDoubleClick(e, (CompartmentId) userObject);
                else if (e.getButton() == MouseEvent.BUTTON3 && e.getClickCount() >= 2)
                    onCompartmentSecondButtonDoubleClick(e, (CompartmentId) userObject);
            } else {
                throw new IllegalStateException();
            }
        }
    }

    protected void onServicesFirstButtonDoubleClick(MouseEvent e) {
        startService.actionPerformed(toActionEvent(e));
    }

    protected void onServiceFirstButtonDoubleClick(MouseEvent e, ServiceId sid) {
        createCompartment.actionPerformed(toActionEvent(e));
    }

    protected void onServiceSecondButtonDoubleClick(MouseEvent e, ServiceId sid) {
        stopService.actionPerformed(toActionEvent(e));
    }

    protected void onCompartmentFirstButtonDoubleClick(MouseEvent e, CompartmentId cid) {
        if (clientGui().isCompartmentInspected(cid)) {
            showCompartmentFrame.actionPerformed(toActionEvent(e));
        } else {
            inspectCompartment.actionPerformed(toActionEvent(e));
        }
    }

    protected void onCompartmentSecondButtonDoubleClick(MouseEvent e, CompartmentId cid) {
        destroyCompartment.actionPerformed(toActionEvent(e));
    }
}
