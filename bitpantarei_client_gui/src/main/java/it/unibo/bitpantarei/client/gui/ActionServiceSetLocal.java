package it.unibo.bitpantarei.client.gui;

import it.unibo.bitpantarei.api.service.ServiceApi;
import it.unibo.bitpantarei.api.service.impl.MyServiceApi;

import javax.swing.*;

/**
 * Created by gciatto on 17/11/15.
 */
public class ActionServiceSetLocal extends ActionServiceSetLocalRemote {

    public ActionServiceSetLocal(ClientMainFrame clientGui) {
        super(Strings.getString("L_MODE_LOCAL"), clientGui);
    }

    @Override
    protected String message(ServiceApi obj) {
        return Strings.getString("F_SERVICE_SETTING_LOCAL", obj);
    }

    @Override
    protected void businessLogic(/*Surely not null*/ MyServiceApi api) {
        api.setLocal(true);
    }

    @Override
    protected JRadioButtonMenuItem getOther() {
        return clientGui().rdbtnmntmRemote;
    }

    @Override
    protected JRadioButtonMenuItem getThis() {
        return clientGui().rdbtnmntmLocal;
    }
}
