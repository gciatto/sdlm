package it.unibo.bitpantarei.client.gui;

import alice.logictuple.LogicTuple;
import it.unibo.bitpantarei.api.compartment.Compartment;
import it.unibo.bitpantarei.api.id.CompartmentId;
import it.unibo.bitpantarei.api.topic.Topic;
import it.unibo.bitpantarei.api.topic.Topic$;
import scala.util.Try;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.text.JTextComponent;
import javax.swing.tree.DefaultTreeSelectionModel;
import java.awt.*;
import java.lang.ref.WeakReference;
import java.util.Optional;

import static it.unibo.bitpantarei.client.gui.Strings.getString;

public class CompartmentFrame extends JFrame {

    /**
     *
     */
    private static final long serialVersionUID = 6480320877412436678L;

    final WeakReference<ClientMainFrame> parent;
    final CompartmentId compartmentId;
    JFormattedTextField txtInsert;
    JPanel contentPane;
    JTextField txtCid;
    JTextField txtCountMol;
    JTextField txtBasePeriod;
    JTextField txtCompTime;
    JTextField txtLastTick;
    JTextField txtEvapProb;
    JFormattedTextField txtTopic;
    JTextField txtInitialConcentration;
    JTable tbMolecules;
    JTree trNeighborhood;
    CompartmentTableModel tbModel = new CompartmentTableModel();
    DefaultTreeSelectionModel trSelectModel = new DefaultTreeSelectionModel();
    CompartmentTreeModel trModel = new CompartmentTreeModel();
    JSpinner spinnerMoleculeAmount;
    JButton btnPutAll;
    JButton btnPut;
    Color originalColor, errorColor = Color.RED;
    JComboBox<CompartmentId> cmbConnectTo;
    JButton btnConnect;
    final scala.Function1<Try<Compartment>, Object> callback = SwingCallback.callback(this::onInspection);

    public CompartmentFrame(CompartmentId myId) {
        this(null, myId);
    }

    /**
     * Create the frame.
     */
    public CompartmentFrame(ClientMainFrame parent, CompartmentId myId) {
        this.parent = new WeakReference<>(parent);
        this.compartmentId = myId;
        setTitle(compartmentId.toString());
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        setBounds(100, 100, 777, 398);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        contentPane.setLayout(new BorderLayout(0, 0));
        setContentPane(contentPane);

        JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
        contentPane.add(tabbedPane, BorderLayout.CENTER);

        JPanel tabCompartment = new JPanel();
        tabbedPane.addTab(getString("L_COMPARTMENT"), null, tabCompartment, null);
        tabCompartment.setLayout(new BorderLayout(0, 0));

        JPanel panelCompartment = new JPanel();
        tabCompartment.add(panelCompartment, BorderLayout.NORTH);
        panelCompartment.setLayout(new GridLayout(0, 2, 0, 0));

        JLabel lblCid = new JLabel(getString("L_CID"));
        panelCompartment.add(lblCid);
        lblCid.setHorizontalAlignment(SwingConstants.LEFT);

        txtCid = new JTextField();
        panelCompartment.add(txtCid);
        txtCid.setEditable(false);
        txtCid.setHorizontalAlignment(SwingConstants.CENTER);
        txtCid.setColumns(10);

        JLabel lblTopic = new JLabel(getString("L_TOPIC"));
        panelCompartment.add(lblTopic);

        CompartmentInputVerifier civ = new CompartmentInputVerifier();
        txtTopic = new JFormattedTextField();
        txtTopic.setHorizontalAlignment(SwingConstants.CENTER);
        txtTopic.setText("-");
        panelCompartment.add(txtTopic);
        txtTopic.setColumns(10);
        txtTopic.setInputVerifier(civ);
        txtTopic.setAction(new ActionCompartmentSetTopic(this));

        JLabel lblBasePeriod = new JLabel(getString("L_BASE_PERIOD"));
        panelCompartment.add(lblBasePeriod);

        txtBasePeriod = new JTextField();
        txtBasePeriod.setEditable(false);
        panelCompartment.add(txtBasePeriod);
        txtBasePeriod.setHorizontalAlignment(SwingConstants.CENTER);
        txtBasePeriod.setText("0 ms");
        txtBasePeriod.setColumns(10);

        JLabel lblNewLabel = new JLabel(getString("L_EVAP_PROB"));
        panelCompartment.add(lblNewLabel);

        txtEvapProb = new JTextField();
        txtEvapProb.setEditable(false);
        txtEvapProb.setHorizontalAlignment(SwingConstants.CENTER);
        panelCompartment.add(txtEvapProb);
        txtEvapProb.setColumns(10);

        JLabel lblInitialConcentration = new JLabel(getString("L_INITIAL_CONCENTRATION"));
        panelCompartment.add(lblInitialConcentration);

        txtInitialConcentration = new JTextField();
        txtInitialConcentration.setEditable(false);
        txtInitialConcentration.setHorizontalAlignment(SwingConstants.CENTER);
        panelCompartment.add(txtInitialConcentration);
        txtInitialConcentration.setColumns(10);

        BorderLayout bl_panelMolecules = new BorderLayout();
        JPanel panelMolecules = new JPanel(bl_panelMolecules);

        tabCompartment.add(panelMolecules, BorderLayout.CENTER);

        JPanel panelInsert = new JPanel();
        panelMolecules.add(panelInsert, BorderLayout.NORTH);
        panelInsert.setLayout(new BorderLayout(0, 0));

        JLabel lblNewLabel_1 = new JLabel(getString("L_PUT"));
        panelInsert.add(lblNewLabel_1, BorderLayout.WEST);

        txtInsert = new JFormattedTextField();
        txtInsert.setInputVerifier(civ);
        panelInsert.add(txtInsert, BorderLayout.CENTER);
        txtInsert.setColumns(10);
        txtInsert.setAction(new ActionCompartmentPutMolecule(this, false));

        JPanel panelInsertButtons = new JPanel();
        panelInsert.add(panelInsertButtons, BorderLayout.EAST);
        panelInsertButtons.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

        btnPut = new JButton(getString("L_PUT_BUTTON"));
        btnPut.setAction(new ActionCompartmentPutMolecule(this, false));
        panelInsertButtons.add(btnPut);

        JSeparator separator_1 = new JSeparator();
        separator_1.setOrientation(SwingConstants.VERTICAL);
        panelInsertButtons.add(separator_1);

        spinnerMoleculeAmount = new JSpinner();
        spinnerMoleculeAmount.setModel(new SpinnerNumberModel(1, 1, Integer.MAX_VALUE, 1));
        panelInsertButtons.add(spinnerMoleculeAmount);

        btnPutAll = new JButton(getString("L_PUT_ALL_BUTTON"));
        btnPutAll.setAction(new ActionCompartmentPutMolecule(this, true));
        panelInsertButtons.add(btnPutAll);

        JSeparator separator_2 = new JSeparator();
        panelInsert.add(separator_2, BorderLayout.NORTH);

        JSeparator separator_3 = new JSeparator();
        panelInsert.add(separator_3, BorderLayout.SOUTH);

        JScrollPane scrollPane = new JScrollPane();
        panelMolecules.add(scrollPane, BorderLayout.CENTER);

        tbMolecules = new JTable();
        tbMolecules.setModel(tbModel);
        scrollPane.setViewportView(tbMolecules);

        JPanel tabNeighbors = new JPanel();
        tabbedPane.addTab(getString("L_NEIGHBORHOOD_TAB"), null, tabNeighbors, null);
        tabNeighbors.setLayout(new BorderLayout(0, 0));

        trNeighborhood = new JTree(trModel);
        trNeighborhood.setSelectionModel(trSelectModel);
        tabNeighbors.add(new JScrollPane(trNeighborhood), BorderLayout.CENTER);

        JPanel panel_4 = new JPanel();
        tabNeighbors.add(panel_4, BorderLayout.NORTH);
        panel_4.setLayout(new BorderLayout(0, 0));

        JLabel lblNewLabel_2 = new JLabel("Connect to:");
        panel_4.add(lblNewLabel_2, BorderLayout.WEST);

        cmbConnectTo = new JComboBox();
        panel_4.add(cmbConnectTo, BorderLayout.CENTER);
//        cmbModel = cmbConnectTo.getModel();
        getParent().getCompartments()
                .stream()
                .filter(it -> !it.equals(getCompartmentId()))
                .forEach(cmbConnectTo::addItem);

        btnConnect = new JButton("Connect");
        panel_4.add(btnConnect, BorderLayout.EAST);
        btnConnect.setEnabled(false);
        btnConnect.setAction(new ActionCompartmentConnectTo(this));

        JSeparator separator_4 = new JSeparator();
        panel_4.add(separator_4, BorderLayout.SOUTH);

        JPanel statusBar = new JPanel();
        contentPane.add(statusBar, BorderLayout.SOUTH);
        statusBar.setLayout(new BorderLayout(0, 0));

        JSeparator separator = new JSeparator();
        statusBar.add(separator, BorderLayout.NORTH);

        JPanel panel = new JPanel();
        statusBar.add(panel, BorderLayout.SOUTH);
        panel.setLayout(new GridLayout(0, 3, 0, 0));

        JPanel panel_1 = new JPanel();
        panel.add(panel_1);
        panel_1.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

        JLabel lblCountMol = new JLabel(getString("L_N_OF_MOLECULES"));
        panel_1.add(lblCountMol);

        txtCountMol = new JTextField();
        panel_1.add(txtCountMol);
        txtCountMol.setHorizontalAlignment(SwingConstants.CENTER);
        txtCountMol.setEditable(false);
        txtCountMol.setText("0");
        txtCountMol.setColumns(10);

        JPanel panel_2 = new JPanel();
        panel.add(panel_2);
        panel_2.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

        JLabel lblLastTick = new JLabel(getString("L_LAST_TICK"));
        panel_2.add(lblLastTick);

        txtLastTick = new JTextField();
        panel_2.add(txtLastTick);
        txtLastTick.setHorizontalAlignment(SwingConstants.CENTER);
        txtLastTick.setEditable(false);
        txtLastTick.setColumns(10);

        JPanel panel_3 = new JPanel();
        panel.add(panel_3);
        panel_3.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

        JLabel lblCompTime = new JLabel(getString("L_EXECUTION_TIME"));
        panel_3.add(lblCompTime);

        txtCompTime = new JTextField();
        panel_3.add(txtCompTime);
        txtCompTime.setHorizontalAlignment(SwingConstants.CENTER);
        txtCompTime.setEditable(false);
        txtCompTime.setColumns(10);

    }

    public static boolean verifyStringForPut(String raw) {
        try {
            return LogicTuple.parse(raw).toTerm().isGround();
        } catch (Exception e) {
            return false;
        }
    }

    public static Optional<Topic> verifyStringForTopic(String raw) {
        try {
            return Optional.of(Topic$.MODULE$.apply(raw));
        } catch (Exception e) {
            return Optional.empty();
        }
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    CompartmentFrame frame = new CompartmentFrame(null);
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public CompartmentId getCompartmentId() {
        return compartmentId;
    }

    public ClientMainFrame getParent() {
        return parent.get();
    }

    private void onInspection(Compartment c) {
        txtCid.setText(c.id().toString());
        txtCountMol.setText("" + c.size());
        txtBasePeriod.setText(c.state().period().toString());
        txtCompTime.setText(String.format(
                "%s s",
                c.state().lastTick()._2().toSeconds()
        ));
        txtEvapProb.setText(String.format(
                "%s ",
                c.state().evaporationProb() * 100
        ) + "%");
        txtInitialConcentration.setText("" + c.state().initialConcentration());
        txtLastTick.setText("" + c.state().lastTick()._1());
        Topic topic = c.state().topic();
        if (!txtTopic.hasFocus()) {
            txtTopic.setText(topic.toString());
        }
        if (!cmbConnectTo.hasFocus()) {
            cmbConnectTo.removeAllItems();
            getParent().getCompartments().stream()
                    .filter(it -> !it.equals(getCompartmentId()))
                    .filter(cid -> !c.neighborhood().neighbors().contains(cid))
                    .forEach(cmbConnectTo::addItem);
        }
        btnConnect.setEnabled(cmbConnectTo.getItemCount() > 0);
        tbModel.adaptTo(c);
        trModel.adaptTo(c);

        expandNeighborhoodTree();
    }

    private void expandNeighborhoodTree() {
        for (int i = 0; i < trNeighborhood.getRowCount(); i++) {
            trNeighborhood.expandRow(i);
        }
    }

    Optional<CompartmentId> getConnectToCandidate() {
        return Optional.ofNullable((CompartmentId) cmbConnectTo.getSelectedItem());
    }

    void registerNewNeighbor(CompartmentId cid) {
        cmbConnectTo.removeItem(cid);
        trModel.$plus$eq(cid);
    }

    private final class CompartmentInputVerifier extends InputVerifier {
        @Override
        public boolean verify(JComponent input) {
            JTextComponent txt = (JTextComponent) input;
            if (txt.getText().isEmpty()) {
                return true;
            } else {
                if (txtInsert.equals(input)) {
                    return verifyStringForPut(txt.getText());
                } else if (txtTopic.equals(input)) {
                    return verifyStringForTopic(txt.getText()).isPresent();

                } else {
                    throw new IllegalStateException();
                }
            }
        }

        @Override
        public boolean shouldYieldFocus(JComponent input) {
            JTextComponent txt = (JTextComponent) input;
            if (verify(input)) {
                if (originalColor != null) {
                    txt.setForeground(originalColor);
                }
                return true;
            } else {
                if (originalColor == null) {
                    originalColor = txt.getForeground();
                }
                txt.setForeground(errorColor);
                return false;
            }
        }
    }
}
