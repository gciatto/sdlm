package it.unibo.bitpantarei.client.gui;

import it.unibo.bitpantarei.api.id.CompartmentId;
import it.unibo.bitpantarei.api.id.ServiceId;
import scala.Tuple2;
import scala.collection.immutable.List;

import java.awt.event.ActionEvent;

import static it.unibo.bitpantarei.client.gui.SwingCallback.callbackWithDefaultErrorHandler;
import static scala.collection.JavaConversions.asJavaIterable;

/**
 * Created by gciatto on 23/10/15.
 */
public class ActionServiceGetCompartments extends ClientAction<ServiceId> {

    public ActionServiceGetCompartments(ClientMainFrame clientGui) {
        super(Strings.getString("A_SERVICE_GET_COMPARTMENTS"), clientGui);
    }

    @Override
    protected String message(ServiceId obj) {
        return String.format(Strings.getString("F_GETTING_COMPARTMENTS"), obj);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (clientGui().isAnyNodeSelected()) {
            final Object nodeContent = clientGui().getSelectedNode().getUserObject();
            if (nodeContent instanceof ServiceId) {
                final ServiceId sid = (ServiceId) nodeContent;
                putMessage(sid);
                clientGui().getServerApi(sid).ifPresent(api ->
                        api.compartments(callbackWithDefaultErrorHandler(
                                this::onCompartmentsGet,
                                t -> removeMessage(sid)
                        ))
                );
            } else {
                showEmptySelectionAlert("service");
            }
        } else {
            showEmptySelectionAlert("service");
        }
    }

    private void onCompartmentsGet(Tuple2<ServiceId, List<CompartmentId>> t) {
        removeMessage(t._1());
        clientGui().setSidCompartments(t._1(), asJavaIterable(t._2()));
    }
}
