package it.unibo.bitpantarei.client.gui;

import it.unibo.bitpantarei.api.id.CompartmentId;
import it.unibo.bitpantarei.api.molecule.Molecule;
import it.unibo.bitpantarei.api.service.ServiceApi;
import scala.Tuple2;

import java.awt.event.ActionEvent;
import java.util.Optional;

import static it.unibo.bitpantarei.client.gui.Strings.getString;
import static it.unibo.bitpantarei.client.gui.SwingCallback.callbackWithDefaultErrorHandler;

/**
 * Created by gciatto on 25/10/15.
 */
public class ActionCompartmentPutMolecule extends CompartmentAction<Molecule> {
    private final boolean mulplicity;

    public ActionCompartmentPutMolecule(CompartmentFrame compartmentFrame, boolean mulplicity) {
        super(
                mulplicity ? getString("L_PUT_ALL_BUTTON") : getString("L_PUT_BUTTON"),
                compartmentFrame
        );
        this.mulplicity = mulplicity;
    }

    @Override
    protected String message(Molecule obj) {
        return String.format(getString("F_PUTTING_MOLECULE"), obj, compartmentGui().compartmentId);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Optional<Molecule> optMol = getMoleculeFromTextFiled(mulplicity);
        CompartmentId cid = compartmentGui().compartmentId;
        optMol.ifPresent(mol -> {
            Optional<ServiceApi> optApi = clientGui().getServerApi(cid.serverId());
            optApi.ifPresent(api -> {
                putMessage(mol);
                api.addMolecule(cid.name(), mol, callbackWithDefaultErrorHandler(
                        this::onMoleculePut,
                        t -> removeMessage(mol)
                ));
            });
        });
        if (!optMol.isPresent()) {
            showWrongStringAlert(getRawMoleculeString());
        }
    }

    public void onMoleculePut(Tuple2<CompartmentId, Molecule> t) {
        removeMessage(t._2());
    }

    @Override
    protected String correctSyntax() {
        return "<prolog_ground_term>";
    }
}
