# How to setup workspace: #
1. Clone https://bitbucket.org/gciatto/sdlm into folder xxx 
2. Clone https://bitbucket.org/smariani/tucson into folder xxx 
3. Go to folder xxx/tucson and create folder xxx/tucson/libs 
4. Browse to https://bitbucket.org/tuprologteam/tuprolog/downloads and dowloads 2p's last version into folder xxx/tucson/libs
5. Import projects into workspaces
6. Check Tucson project's dependencies
7. Install the following plugins: Scala plugin, Antlr4 plugin
8. Enjoy