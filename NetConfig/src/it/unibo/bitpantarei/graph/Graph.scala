package it.unibo.bitpantarei.graph

import it.unibo.bitpantarei.graph.INode

import scala.collection.mutable.HashSet

/**
 * Created by giova on 27/09/2015.
 */
trait IGraph[N, A] {
  def add(n: INode[N])
  def add(a: IArc[N, A])
  def remove(n: INode[N])
  def remove(a: IArc[N, A])
  def arcsFrom(n: INode[N]): Set[IArc[N, A]]
  def arcsTo(n: INode[N]): Set[IArc[N, A]]
  def nodesFrom(n: INode[N]): Set[INode[N]]
  def nodesTo(n: INode[N]): Set[INode[N]]
  def nodes: Set[INode[N]]
  def arcs: Set[IArc[N, A]]
  def countNodes: Int
  def countArcs: Int
}

class Graph[N, A] extends IGraph[N, A] {
  val mNodes = new HashSet[INode[N]]
  val mArcs = new HashSet[IArc[N, A]]


  override def countNodes: Int = mNodes.size

  override def countArcs: Int = mArcs.size

  override def add(n: INode[N]): Unit =
    mNodes += n

  override def arcsFrom(n: INode[N]): Set[IArc[N, A]] =
    mArcs.filter(_.from == n).toSet


  override def remove(n: INode[N]): Unit = {
    mArcs --= mArcs.filter(it => it.from == n || it.to == n)
    mNodes -= n
  }

  override def remove(a: IArc[N, A]): Unit = {
    mArcs -= a
  }

  override def arcsTo(n: INode[N]): Set[IArc[N, A]] =
    mArcs.filter(_.to == n).toSet

  override def nodesTo(n: INode[N]): Set[INode[N]] =
    mArcs.filter(_.to == n).map(_.from).toSet

  override def add(a: IArc[N, A]): Unit = {
    mNodes ++= Seq(a.from, a.to)
    mArcs += a
  }

  override def nodesFrom(n: INode[N]): Set[INode[N]] =
    mArcs.filter(_.from == n).map(_.to).toSet



  override def nodes: Set[INode[N]] = mNodes.toSet

  override def arcs: Set[IArc[N, A]] = mArcs.toSet

  override def toString =
    s"Graph\n\tN = {${mNodes
      .map(it => s"$it")
      .reduceLeftOption(_ + ", " + _)
      .getOrElse("")
    }}\n\tA = {${mArcs
      .map(it => s"$it")
      .reduceLeftOption(_ + ", " + _)
      .getOrElse("")
    }}"
}

object Graph {
  def apply[N, A](arcs: IArc[N, A]*): IGraph[N, A] = {
    val g = new Graph[N, A]()
    arcs.foreach(g.add(_))
    g
  }

}