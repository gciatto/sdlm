package it.unibo.bitpantarei.graph

/**
 * Created by giova on 28/09/2015.
 */
object Node {
  def apply[T](name: String) = new Node[T](name)
  def apply[T](name: String, content: T) = new Node[T](name, content)
}

class Node[T](mName: String, var mContent: Option[T]) extends INode[T]{

  def this(name: String) =
    this(name, None)

  def this(name: String, content: T) =
    this(name, Option(content))

  override def name: String = mName
  override def content: Option[T] = mContent
  override def content_=(value: T) = mContent = Option(value)

  override def toString = s"$name"
}