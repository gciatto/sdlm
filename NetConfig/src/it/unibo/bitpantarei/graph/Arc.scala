package it.unibo.bitpantarei.graph

import it.unibo.bitpantarei.graph.INode

/**
 * Created by giova on 28/09/2015.
 */
object Arc {
  def apply[N, A](from: INode[N], to: INode[N]) = new Arc[N, A](from, to, "", None)
  def apply[N, A](from: INode[N], to: INode[N], name: String) = new Arc[N, A](from, to, name, None)
  def apply[N, A](from: INode[N], to: INode[N], weight: A) = new Arc[N, A](from, to, "", Option(weight))
  def apply[N, A](from: INode[N], to: INode[N], name: String, weight: A) = new Arc[N, A](from, to, name, Some(weight))
}

class Arc[N, W](mFrom: INode[N], mTo: INode[N], var mName: String, var mWeight: Option[W]) extends IArc[N, W] {
  override def tag: String = mName

  override def tag_=(name: String): Unit = mName = name

  override def from: INode[N] = mFrom

  override def to: INode[N] = mTo

  override def weight: Option[W] = mWeight

  override def weight_=(value: W): Unit = mWeight = Option(value)

  override def toString = s"($mFrom -> $mTo)"
}