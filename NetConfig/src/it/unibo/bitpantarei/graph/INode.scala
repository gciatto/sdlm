package it.unibo.bitpantarei.graph

/**
 * Created by giova on 27/09/2015.
 */
trait INode[T] {
  def name: String
  def content: Option[T]
  def content_=(value: T)
  def canEqual(other: Any): Boolean = other.isInstanceOf[INode[T]]
  override def equals(other: Any): Boolean = other match {
    case that: INode[T] =>
      (that canEqual this) &&
        name == that.name
    case _ => false
  }
  override def hashCode(): Int = {
    val state = Seq(name)
    state.map(_.hashCode()).foldLeft(0)((a, b) => 31 * a + b)
  }
}
