package it.unibo.bitpantarei.graph

/**
 * Created by giova on 27/09/2015.
 */
trait IArc[N, W] {
  def tag: String
  def tag_=(name: String)
  def from: INode[N]
  def to: INode[N]
  def weight: Option[W]
  def weight_=(value: W)
  def canEqual(other: Any): Boolean = other.isInstanceOf[IArc[N, W]]
  override def equals(other: Any): Boolean = other match {
    case that: IArc[N, W] =>
      (that canEqual this) &&
        from == that.from &&
        to == that.to

    case _ => false
  }
  override def hashCode(): Int = {
    val state = Seq(from, to)
    state.map(_.hashCode()).foldLeft(0)((a, b) => 31 * a + b)
  }

}
