package it.unibo.bitpantarei.network

import alice.tucson.api.TucsonTupleCentreId
import alice.tuplecentre.api.TupleCentreId
import it.unibo.bitpantarei.Tucson
import it.unibo.bitpantarei.network.dsl._
import it.unibo.bitpantarei.network.dsl.NetworkParser._
import org.antlr.v4.runtime.ParserRuleContext
import org.antlr.v4.runtime.tree._

/**
 * Created by giova on 26/09/2015.
 */
object SimpleNetworkListener extends NetworkListener {
  override def enterNetwork(ctx: NetworkContext): Unit = {}

  override def exitAddress(ctx: AddressContext): Unit = print(ctx)

  var nodeName: String = Tucson.DefaultNode
  var nodeAddr: String = Tucson.DefaultAddress
  var nodePort: Int = Tucson.DefaultPort
  var nodeId: TupleCentreId = tid(nodeName, nodeAddr, nodePort)
  override def enterNode(ctx: NodeContext): Unit = {
    nodeName = Tucson.DefaultNode
    nodeAddr = Tucson.DefaultAddress
    nodePort = Tucson.DefaultPort
  }

  override def exitNodes(ctx: NodesContext): Unit = print(ctx)

  override def enterArcs(ctx: ArcsContext): Unit = ???

  override def enterName(ctx: NameContext): Unit = ???

  override def enterArc(ctx: ArcContext): Unit = ???

  override def exitPort(ctx: PortContext): Unit = print(ctx)

  override def enterAddress(ctx: AddressContext): Unit = ???

  override def exitNode(ctx: NodeContext): Unit = {
    nodeId = tid(nodeName, nodeAddr, nodePort)
  }

  override def exitNetwork(ctx: NetworkContext): Unit = print(ctx)

  override def exitArcs(ctx: ArcsContext): Unit = print(ctx)

  override def exitArc(ctx: ArcContext): Unit = print(ctx)

  override def enterPort(ctx: PortContext): Unit = ???

  override def enterNodes(ctx: NodesContext): Unit = ???

  override def exitName(ctx: NameContext): Unit = ???

  override def visitTerminal(terminalNode: TerminalNode): Unit = ???

  override def visitErrorNode(errorNode: ErrorNode): Unit = ???

  override def exitEveryRule(parserRuleContext: ParserRuleContext): Unit = ???

  override def enterEveryRule(parserRuleContext: ParserRuleContext): Unit = ???

  protected def tid(name: String, addr: String, port: Int): TupleCentreId = new TucsonTupleCentreId(name, addr, port.toString)
}
