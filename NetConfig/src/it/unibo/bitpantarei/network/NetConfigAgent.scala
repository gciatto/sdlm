package it.unibo.bitpantarei.network

import java.io.{BufferedReader, File, FileReader}
import java.util.concurrent.CountDownLatch
import java.util.function.Consumer
import javax.swing.SwingUtilities
import javax.swing.SwingUtilities.invokeLater

import alice.logictuple.LogicTuple
import alice.tucson.api._
import alice.tucson.introspection.tools.InspectorGUI
import alice.tuplecentre.api.TupleCentreId
import alice.tuplecentre.core.AbstractTupleCentreOperation
import it.unibo.bitpantarei.{NNodesStarter, Tucson}
import it.unibo.bitpantarei.Tucson.connectToString
import it.unibo.bitpantarei.TucsonOperationCompletedAdapter.toTucsonOperationCompletedAdapter
import it.unibo.bitpantarei.network.dsl.parsing.Visitor

/**
 * Created by giova on 26/09/2015.
 */
class NetConfigAgent(aid: String, config: File, input: File, startNodes: Boolean, inspectNodes: Boolean) extends AbstractTucsonAgent(aid) {

  var mConfigured: CountDownLatch = null
  var mConnected: CountDownLatch = null

  def this(config: File, input: File) = 
    this("networkConfiguratorAgent", config, input, false, false)

  def this(config: File, input: File, startNodes: Boolean) = 
    this("networkConfiguratorAgent", config, input, startNodes, false)

  def this(config: File, input: File, startNodes: Boolean, inspectNodes: Boolean) =
    this("networkConfiguratorAgent", config, input, startNodes, inspectNodes)


  override def operationCompleted(op: AbstractTupleCentreOperation): Unit = {
    operationCompleted(op.asInstanceOf[ITucsonOperation])
  }

  override def operationCompleted(op: ITucsonOperation): Unit = {
    /*
     * Do nothing
     */
  }

  override protected def main(): Unit = {
    try {
      println("Hi!\n")

      val confReader = new BufferedReader(new FileReader(config))
      val sb = new StringBuffer(2 * config.length.toInt)
      confReader.lines().forEach((s: String) => sb.append(s).append("\n"))
      confReader.close()
      val configString = sb.toString()
      println(s"Confing string:\n$configString\n")

      val g = Visitor(input)

      if (startNodes) {
        val ports = g.nodes
          .map(_.content.get.getPort)
          .toSeq
          .distinct
          .map(_.toString)
          .toArray


        NNodesStarter.main(ports)
      }

      println(s"$g\n")

      mConfigured = new CountDownLatch(g.countNodes)
      mConnected = new CountDownLatch(g.countArcs)

      println("Setting specifics...\n")

      def err(s: String) = println(s"$s: Damn!")

      g.nodes.foreach(n => {
        val node = n.content.get
        asyncSetSpec(node, configString, setS => {
          if (setS.isResultSuccess) {
            println("set_s: Great!")
            asyncOut(n.content.get, Tucson.ConfigTuple, out => {
              if (out.isResultSuccess) {
                println("out: Great!")
                mConfigured.countDown()
              } else err("out")
            })
          } else err("set_s")
        })
      })

      println("Waiting for answers...\n")
      mConfigured.await(Tucson.Timeout._1, Tucson.Timeout._2)

      //    Console.in.readLine()

      println("Connetting centres...\n")
      g.arcs.foreach(a => {
        val from = a.from.content.get
        val to = a.to.content.get
        asyncOut(from, connectToString(to), out => {
          if (out.isResultSuccess) {
            println("set_s: Great!")
            mConnected.countDown()
          } else err("out")
        })
      })

      println("Waiting for answers...\n")
      mConnected.await(Tucson.Timeout._1, Tucson.Timeout._2)


      if (inspectNodes) {
        g.nodes
          .map(_.content.get)
          .map(it => new InspectorGUI(new TucsonAgentId(s"inspectorOf${it.getName}At${it.getNode}On${it.getPort}"), it, false))
          .foreach(it => {
          invokeLater(() => {
            it.setVisible(true)
          })
          invokeLater(() => {
            it.showTupleCenter()
          })
        })

      }

      println("Done!\n")
    } catch {
      case e: Exception => {
        e.printStackTrace()
        System.exit(1)
      }
    }

  }

  protected implicit def toRunnable(f: () => Any): Runnable =
    new Runnable {
      override def run(): Unit = f()
    }

  protected def asyncOut(tid: TupleCentreId, tuple: String, callback: ITucsonOperation => Unit) = {
    println(s"$tid ? out($tuple)")
    acc.out(tid, LogicTuple.parse(tuple), callback)
  }

  protected def asyncSetSpec(tid: TupleCentreId, tuple: String, callback: ITucsonOperation => Unit) = {
    println(s"$tid ? set_s(spec('$tuple'))")
    acc.setS(tid, tuple, callback)
  }

  protected implicit def toConsumer[T](f: T => Any): Consumer[T] =
    new Consumer[T] {
      override def accept(t: T): Unit =
        try {
          f(t)
        } catch {
          case e: Exception => {
            e.printStackTrace()
            System.exit(1)
          }

        }
    }

  protected def tid: TupleCentreId = tid(Tucson.DefaultNode)

  protected def tid(name: String): TupleCentreId = tid(name, Tucson.DefaultAddress);

  protected def tid(name: String, addr: String): TupleCentreId = tid(name, addr, Tucson.DefaultPort)

  protected def tid(name: String, addr: String, port: Int): TupleCentreId = new TucsonTupleCentreId(name, addr, port.toString)

  protected def tuple(t: String): LogicTuple = LogicTuple.parse(t)

  protected def asyncOut(tid: TupleCentreId, tuple: String) = {
    println(s"$tid ? out($tuple)")
    acc.out(tid, LogicTuple.parse(tuple), this)
  }

  protected def acc: AsynchACC = getContext

  protected def asyncSetSpec(tcId: TupleCentreId, spec: String) = {
    println(s"$tcId ? set_s(spec('$spec'))")
    acc.setS(tcId, spec, this)
  }

  //  protected def syncOut(tcId: TupleCentreId, tuple: String) =
  //    getContext.se

}
