package it.unibo.bitpantarei.network.dsl.parsing

import java.io.File

import alice.tucson.api.TucsonTupleCentreId
import it.unibo.bitpantarei.Tucson
import it.unibo.bitpantarei.Tucson.{DefaultAddress, DefaultNode}
import it.unibo.bitpantarei.graph._
import it.unibo.bitpantarei.network.dsl.NetworkParser._
import it.unibo.bitpantarei.network.dsl.{NetworkBaseVisitor, NetworkLexer, NetworkParser}
import org.antlr.v4.runtime.{ANTLRFileStream, ANTLRInputStream, CommonTokenStream}

import scala.collection.JavaConversions

/**
 * Created by giova on 28/09/2015.
 */

object Visitor {

  def apply(f: File): IGraph[TucsonTupleCentreId, Nothing] =
    visit(new ANTLRFileStream(f.getAbsolutePath))

  def apply(s: String): IGraph[TucsonTupleCentreId, Nothing] =
    visit(new ANTLRInputStream(s))

  def visit(i: ANTLRInputStream): IGraph[TucsonTupleCentreId, Nothing] = {
    val lexer = new NetworkLexer(i)
    val tokenStream = new CommonTokenStream(lexer)
    val parser = new NetworkParser(tokenStream)

    visit(parser)
  }

  def visit(p: NetworkParser): IGraph[TucsonTupleCentreId, Nothing] = NetworkVisitor(p.network())

  object PortVisitor extends NetworkBaseVisitor[Int] {
    def apply(ctx: PortContext): Int = visitPort(ctx)

    override def visitPort(ctx: PortContext): Int = {
      if (Option(ctx).isDefined) {
        super.visitPort(ctx)
        val numberStr = ctx.NUMBER.getSymbol.getText
        Integer.parseInt(numberStr)
      } else Tucson.DefaultPort
    }
  }

  object AddressVisitor extends NetworkBaseVisitor[String] {
    def apply(ctx: AddressContext): String = visitAddress(ctx)

    override def visitAddress(ctx: AddressContext): String = {
      if (Option(ctx).isDefined) {
        super.visitAddress(ctx)
        if (ctx.getChildCount > 1) {
          val ns = JavaConversions.asScalaBuffer(ctx.NUMBER())
          ns.map(_.getSymbol.getText).reduceLeft(_ + "." + _)
        } else DefaultAddress
      } else DefaultAddress
    }
  }

  object NameVisitor extends NetworkBaseVisitor[String] {
    def apply(ctx: NameContext): String = visitName(ctx)

    override def visitName(ctx: NameContext): String = {
      super.visitName(ctx)
      var result: String = ""
      Option(ctx.DEFAULT()).foreach(it => result = DefaultNode)
      Option(ctx.LABEL()).foreach(it => result = it.getSymbol.getText)
      result
    }
  }

  object NodeVisitor extends NetworkBaseVisitor[INode[TucsonTupleCentreId]] {

    def apply(ctx: NodeContext): INode[TucsonTupleCentreId] = visitNode(ctx)

    override def visitNode(ctx: NodeContext): INode[TucsonTupleCentreId] = {
      super.visitNode(ctx)
      val name = NameVisitor(ctx.name())
      val addr = AddressVisitor(ctx.address())
      val port = PortVisitor(ctx.port())
      val tcId = tid(name, addr, port)
      Node(tcId.toString, tcId)
    }

    protected def tid(name: String, addr: String, port: Int): TucsonTupleCentreId = new TucsonTupleCentreId(name, addr, port.toString)

  }

  object NodesVisitor extends NetworkBaseVisitor[Iterable[INode[TucsonTupleCentreId]]] {
    def apply(ctx: NodesContext): Iterable[INode[TucsonTupleCentreId]] = visitNodes(ctx)

    override def visitNodes(ctx: NodesContext): Iterable[INode[TucsonTupleCentreId]] = {
      if (Option(ctx).isDefined) {
        super.visitNodes(ctx)
        JavaConversions
          .asScalaBuffer(ctx.node())
          .map(NodeVisitor(_))
          .toIterable
      } else Iterable.empty
    }
  }

  object NetworkVisitor extends NetworkBaseVisitor[IGraph[TucsonTupleCentreId, Nothing]] {
    def apply(ctx: NetworkContext): IGraph[TucsonTupleCentreId, Nothing] = visitNetwork(ctx)

    override def visitNetwork(ctx: NetworkContext): IGraph[TucsonTupleCentreId, Nothing] = {
      super.visitNetwork(ctx)
      val graph = new Graph[TucsonTupleCentreId, Nothing]
      NodesVisitor(ctx.nodes()).foreach(graph.add(_))
      ArcsVisitor(ctx.arcs()).foreach(graph.add(_))
      graph
    }
  }

  object ArcVisitor extends NetworkBaseVisitor[IArc[TucsonTupleCentreId, Nothing]] {
    def apply(ctx: ArcContext): IArc[TucsonTupleCentreId, Nothing] = visitArc(ctx)

    override def visitArc(ctx: ArcContext): IArc[TucsonTupleCentreId, Nothing] = {
      super.visitArc(ctx)
      Arc(NodeVisitor(ctx.node().get(0)), NodeVisitor(ctx.node().get(1)))
    }
  }


  object ArcsVisitor extends NetworkBaseVisitor[Iterable[IArc[TucsonTupleCentreId, Nothing]]] {
    def apply(ctx: ArcsContext): Iterable[IArc[TucsonTupleCentreId, Nothing]] = visitArcs(ctx)

    override def visitArcs(ctx: ArcsContext): Iterable[IArc[TucsonTupleCentreId, Nothing]] = {
      super.visitArcs(ctx)
      JavaConversions.asScalaBuffer(ctx.arc())
        .map(ArcVisitor(_))
        .toIterable
    }
  }

}