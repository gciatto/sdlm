package it.unibo.bitpantarei

import java.io.File

import it.unibo.bitpantarei.network.NetConfigAgent


object NetConfig extends App {
  var n: NetConfigAgent = null

  if (args.length == 0) {
    println(
      "tucsonNetConf <net_conf> [<net_spec>]\n"
        + "\t<net_conf>\t:\tNetwork configuration script path\n"
        + "\t<net_spec>\t:\tNetwork specification ReSpecT script path"
    )
    System.exit(1)
  } else if (args.length == 1) {
    n = new NetConfigAgent(Tucson.ConfigFile, new File(args(0)).getAbsoluteFile)
  } else if (args.length == 2) {
    n = new NetConfigAgent(new File(args(1)).getAbsoluteFile, new File(args(0)).getAbsoluteFile)
  } else if (args.length > 2) {
    n = new NetConfigAgent(new File(args(1)).getAbsoluteFile, new File(args(0)).getAbsoluteFile,
      args.drop(2).exists(_ equalsIgnoreCase "-start"),
      args.drop(2).exists(_ equalsIgnoreCase "-inspect")
    )
  }

  n.go()



}
