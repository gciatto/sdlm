package it.unibo.bitpantarei

import java.io.File
import java.util.concurrent.TimeUnit

import alice.tucson.api.TucsonTupleCentreId

/**
 * Created by giova on 27/09/2015.
 */
object Tucson {
  val ConfigTuple = "config"
  val DefaultNode = "default"
  val DefaultAddress = "localhost"
  val DefaultPort = 20504
  val Timeout = (3l, TimeUnit.SECONDS)
  val ConfigFile = new File("res/network.rsp").getAbsoluteFile

  def connectToString(tid: TucsonTupleCentreId): String =
    s"connect_to(${tid.getName}, ${tid.getNode}, '${tid.getPort}')"
}
