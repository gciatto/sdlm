package it.unibo.bitpantarei

import alice.tucson.api.{ITucsonOperation, TucsonOperationCompletionListener}
import alice.tuplecentre.core.AbstractTupleCentreOperation

/**
 * Created by giova on 01/10/2015.
 */
class TucsonOperationCompletedAdapter(f: ITucsonOperation => Unit) extends TucsonOperationCompletionListener {

  override def operationCompleted(op: AbstractTupleCentreOperation): Unit =
    operationCompleted(op.asInstanceOf[ITucsonOperation])

  override def operationCompleted(op: ITucsonOperation): Unit =
    f(op)
}

object TucsonOperationCompletedAdapter {
  implicit def toTucsonOperationCompletedAdapter(f: ITucsonOperation => Unit): TucsonOperationCompletionListener =
    TucsonOperationCompletedAdapter(f)

  def apply(f: ITucsonOperation => Unit) =
    new TucsonOperationCompletedAdapter(f)
}
