// Generated from C:/Users/giova/IdeaProjects/BitPantarei/NetConfig/dsl\Network.g4 by ANTLR 4.5.1
package it.unibo.bitpantarei.network.dsl;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link NetworkParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface NetworkVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link NetworkParser#network}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNetwork(NetworkParser.NetworkContext ctx);
	/**
	 * Visit a parse tree produced by {@link NetworkParser#nodes}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNodes(NetworkParser.NodesContext ctx);
	/**
	 * Visit a parse tree produced by {@link NetworkParser#node}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNode(NetworkParser.NodeContext ctx);
	/**
	 * Visit a parse tree produced by {@link NetworkParser#name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitName(NetworkParser.NameContext ctx);
	/**
	 * Visit a parse tree produced by {@link NetworkParser#address}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAddress(NetworkParser.AddressContext ctx);
	/**
	 * Visit a parse tree produced by {@link NetworkParser#port}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPort(NetworkParser.PortContext ctx);
	/**
	 * Visit a parse tree produced by {@link NetworkParser#arcs}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArcs(NetworkParser.ArcsContext ctx);
	/**
	 * Visit a parse tree produced by {@link NetworkParser#arc}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArc(NetworkParser.ArcContext ctx);
}