// Generated from C:/Users/giova/IdeaProjects/BitPantarei/NetConfig/dsl\Network.g4 by ANTLR 4.5.1
package it.unibo.bitpantarei.network.dsl;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class NetworkParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.5.1", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		COMMENT=1, NODES=2, ARCS=3, NUMBER=4, WHITESP=5, LABEL=6, DEFAULT=7, DEFAULT_PORT=8, 
		LOCALHOST=9, ARROW=10, SEPARATOR=11, AT=12, AT_PORT=13, L_BRAC=14, R_BRAC=15, 
		DOT=16;
	public static final int
		RULE_network = 0, RULE_nodes = 1, RULE_node = 2, RULE_name = 3, RULE_address = 4, 
		RULE_port = 5, RULE_arcs = 6, RULE_arc = 7;
	public static final String[] ruleNames = {
		"network", "nodes", "node", "name", "address", "port", "arcs", "arc"
	};

	private static final String[] _LITERAL_NAMES = {
		null, null, "'nodes'", "'arcs'", null, null, null, "'default'", "'default port'", 
		"'localhost'", "'->'", null, "'@'", "':'", "'{'", "'}'", "'.'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, "COMMENT", "NODES", "ARCS", "NUMBER", "WHITESP", "LABEL", "DEFAULT", 
		"DEFAULT_PORT", "LOCALHOST", "ARROW", "SEPARATOR", "AT", "AT_PORT", "L_BRAC", 
		"R_BRAC", "DOT"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "Network.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public NetworkParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class NetworkContext extends ParserRuleContext {
		public ArcsContext arcs() {
			return getRuleContext(ArcsContext.class,0);
		}
		public NodesContext nodes() {
			return getRuleContext(NodesContext.class,0);
		}
		public NetworkContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_network; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof NetworkListener ) ((NetworkListener)listener).enterNetwork(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof NetworkListener ) ((NetworkListener)listener).exitNetwork(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof NetworkVisitor ) return ((NetworkVisitor<? extends T>)visitor).visitNetwork(this);
			else return visitor.visitChildren(this);
		}
	}

	public final NetworkContext network() throws RecognitionException {
		NetworkContext _localctx = new NetworkContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_network);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(17);
			_la = _input.LA(1);
			if (_la==NODES) {
				{
				setState(16);
				nodes();
				}
			}

			setState(19);
			arcs();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NodesContext extends ParserRuleContext {
		public TerminalNode NODES() { return getToken(NetworkParser.NODES, 0); }
		public TerminalNode L_BRAC() { return getToken(NetworkParser.L_BRAC, 0); }
		public List<NodeContext> node() {
			return getRuleContexts(NodeContext.class);
		}
		public NodeContext node(int i) {
			return getRuleContext(NodeContext.class,i);
		}
		public TerminalNode R_BRAC() { return getToken(NetworkParser.R_BRAC, 0); }
		public List<TerminalNode> SEPARATOR() { return getTokens(NetworkParser.SEPARATOR); }
		public TerminalNode SEPARATOR(int i) {
			return getToken(NetworkParser.SEPARATOR, i);
		}
		public NodesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_nodes; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof NetworkListener ) ((NetworkListener)listener).enterNodes(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof NetworkListener ) ((NetworkListener)listener).exitNodes(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof NetworkVisitor ) return ((NetworkVisitor<? extends T>)visitor).visitNodes(this);
			else return visitor.visitChildren(this);
		}
	}

	public final NodesContext nodes() throws RecognitionException {
		NodesContext _localctx = new NodesContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_nodes);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(21);
			match(NODES);
			setState(22);
			match(L_BRAC);
			setState(23);
			node();
			setState(28);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==SEPARATOR) {
				{
				{
				setState(24);
				match(SEPARATOR);
				setState(25);
				node();
				}
				}
				setState(30);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(31);
			match(R_BRAC);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NodeContext extends ParserRuleContext {
		public NameContext name() {
			return getRuleContext(NameContext.class,0);
		}
		public TerminalNode AT() { return getToken(NetworkParser.AT, 0); }
		public AddressContext address() {
			return getRuleContext(AddressContext.class,0);
		}
		public TerminalNode AT_PORT() { return getToken(NetworkParser.AT_PORT, 0); }
		public PortContext port() {
			return getRuleContext(PortContext.class,0);
		}
		public NodeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_node; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof NetworkListener ) ((NetworkListener)listener).enterNode(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof NetworkListener ) ((NetworkListener)listener).exitNode(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof NetworkVisitor ) return ((NetworkVisitor<? extends T>)visitor).visitNode(this);
			else return visitor.visitChildren(this);
		}
	}

	public final NodeContext node() throws RecognitionException {
		NodeContext _localctx = new NodeContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_node);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(33);
			name();
			setState(40);
			_la = _input.LA(1);
			if (_la==AT) {
				{
				setState(34);
				match(AT);
				setState(35);
				address();
				setState(38);
				_la = _input.LA(1);
				if (_la==AT_PORT) {
					{
					setState(36);
					match(AT_PORT);
					setState(37);
					port();
					}
				}

				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NameContext extends ParserRuleContext {
		public TerminalNode DEFAULT() { return getToken(NetworkParser.DEFAULT, 0); }
		public TerminalNode LABEL() { return getToken(NetworkParser.LABEL, 0); }
		public NameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof NetworkListener ) ((NetworkListener)listener).enterName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof NetworkListener ) ((NetworkListener)listener).exitName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof NetworkVisitor ) return ((NetworkVisitor<? extends T>)visitor).visitName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final NameContext name() throws RecognitionException {
		NameContext _localctx = new NameContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_name);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(42);
			_la = _input.LA(1);
			if ( !(_la==LABEL || _la==DEFAULT) ) {
			_errHandler.recoverInline(this);
			} else {
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AddressContext extends ParserRuleContext {
		public TerminalNode LOCALHOST() { return getToken(NetworkParser.LOCALHOST, 0); }
		public List<TerminalNode> NUMBER() { return getTokens(NetworkParser.NUMBER); }
		public TerminalNode NUMBER(int i) {
			return getToken(NetworkParser.NUMBER, i);
		}
		public List<TerminalNode> DOT() { return getTokens(NetworkParser.DOT); }
		public TerminalNode DOT(int i) {
			return getToken(NetworkParser.DOT, i);
		}
		public AddressContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_address; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof NetworkListener ) ((NetworkListener)listener).enterAddress(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof NetworkListener ) ((NetworkListener)listener).exitAddress(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof NetworkVisitor ) return ((NetworkVisitor<? extends T>)visitor).visitAddress(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AddressContext address() throws RecognitionException {
		AddressContext _localctx = new AddressContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_address);
		try {
			setState(52);
			switch (_input.LA(1)) {
			case LOCALHOST:
				enterOuterAlt(_localctx, 1);
				{
				setState(44);
				match(LOCALHOST);
				}
				break;
			case NUMBER:
				enterOuterAlt(_localctx, 2);
				{
				{
				setState(45);
				match(NUMBER);
				setState(46);
				match(DOT);
				setState(47);
				match(NUMBER);
				setState(48);
				match(DOT);
				setState(49);
				match(NUMBER);
				setState(50);
				match(DOT);
				setState(51);
				match(NUMBER);
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PortContext extends ParserRuleContext {
		public TerminalNode DEFAULT_PORT() { return getToken(NetworkParser.DEFAULT_PORT, 0); }
		public TerminalNode NUMBER() { return getToken(NetworkParser.NUMBER, 0); }
		public PortContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_port; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof NetworkListener ) ((NetworkListener)listener).enterPort(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof NetworkListener ) ((NetworkListener)listener).exitPort(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof NetworkVisitor ) return ((NetworkVisitor<? extends T>)visitor).visitPort(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PortContext port() throws RecognitionException {
		PortContext _localctx = new PortContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_port);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(54);
			_la = _input.LA(1);
			if ( !(_la==NUMBER || _la==DEFAULT_PORT) ) {
			_errHandler.recoverInline(this);
			} else {
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ArcsContext extends ParserRuleContext {
		public TerminalNode ARCS() { return getToken(NetworkParser.ARCS, 0); }
		public TerminalNode L_BRAC() { return getToken(NetworkParser.L_BRAC, 0); }
		public List<ArcContext> arc() {
			return getRuleContexts(ArcContext.class);
		}
		public ArcContext arc(int i) {
			return getRuleContext(ArcContext.class,i);
		}
		public TerminalNode R_BRAC() { return getToken(NetworkParser.R_BRAC, 0); }
		public List<TerminalNode> SEPARATOR() { return getTokens(NetworkParser.SEPARATOR); }
		public TerminalNode SEPARATOR(int i) {
			return getToken(NetworkParser.SEPARATOR, i);
		}
		public ArcsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_arcs; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof NetworkListener ) ((NetworkListener)listener).enterArcs(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof NetworkListener ) ((NetworkListener)listener).exitArcs(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof NetworkVisitor ) return ((NetworkVisitor<? extends T>)visitor).visitArcs(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ArcsContext arcs() throws RecognitionException {
		ArcsContext _localctx = new ArcsContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_arcs);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(56);
			match(ARCS);
			setState(57);
			match(L_BRAC);
			setState(58);
			arc();
			setState(63);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==SEPARATOR) {
				{
				{
				setState(59);
				match(SEPARATOR);
				setState(60);
				arc();
				}
				}
				setState(65);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(66);
			match(R_BRAC);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ArcContext extends ParserRuleContext {
		public List<NodeContext> node() {
			return getRuleContexts(NodeContext.class);
		}
		public NodeContext node(int i) {
			return getRuleContext(NodeContext.class,i);
		}
		public TerminalNode ARROW() { return getToken(NetworkParser.ARROW, 0); }
		public ArcContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_arc; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof NetworkListener ) ((NetworkListener)listener).enterArc(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof NetworkListener ) ((NetworkListener)listener).exitArc(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof NetworkVisitor ) return ((NetworkVisitor<? extends T>)visitor).visitArc(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ArcContext arc() throws RecognitionException {
		ArcContext _localctx = new ArcContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_arc);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(68);
			node();
			setState(69);
			match(ARROW);
			setState(70);
			node();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3\22K\4\2\t\2\4\3\t"+
		"\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\3\2\5\2\24\n\2\3\2"+
		"\3\2\3\3\3\3\3\3\3\3\3\3\7\3\35\n\3\f\3\16\3 \13\3\3\3\3\3\3\4\3\4\3\4"+
		"\3\4\3\4\5\4)\n\4\5\4+\n\4\3\5\3\5\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\5\6"+
		"\67\n\6\3\7\3\7\3\b\3\b\3\b\3\b\3\b\7\b@\n\b\f\b\16\bC\13\b\3\b\3\b\3"+
		"\t\3\t\3\t\3\t\3\t\2\2\n\2\4\6\b\n\f\16\20\2\4\3\2\b\t\4\2\6\6\n\nH\2"+
		"\23\3\2\2\2\4\27\3\2\2\2\6#\3\2\2\2\b,\3\2\2\2\n\66\3\2\2\2\f8\3\2\2\2"+
		"\16:\3\2\2\2\20F\3\2\2\2\22\24\5\4\3\2\23\22\3\2\2\2\23\24\3\2\2\2\24"+
		"\25\3\2\2\2\25\26\5\16\b\2\26\3\3\2\2\2\27\30\7\4\2\2\30\31\7\20\2\2\31"+
		"\36\5\6\4\2\32\33\7\r\2\2\33\35\5\6\4\2\34\32\3\2\2\2\35 \3\2\2\2\36\34"+
		"\3\2\2\2\36\37\3\2\2\2\37!\3\2\2\2 \36\3\2\2\2!\"\7\21\2\2\"\5\3\2\2\2"+
		"#*\5\b\5\2$%\7\16\2\2%(\5\n\6\2&\'\7\17\2\2\')\5\f\7\2(&\3\2\2\2()\3\2"+
		"\2\2)+\3\2\2\2*$\3\2\2\2*+\3\2\2\2+\7\3\2\2\2,-\t\2\2\2-\t\3\2\2\2.\67"+
		"\7\13\2\2/\60\7\6\2\2\60\61\7\22\2\2\61\62\7\6\2\2\62\63\7\22\2\2\63\64"+
		"\7\6\2\2\64\65\7\22\2\2\65\67\7\6\2\2\66.\3\2\2\2\66/\3\2\2\2\67\13\3"+
		"\2\2\289\t\3\2\29\r\3\2\2\2:;\7\5\2\2;<\7\20\2\2<A\5\20\t\2=>\7\r\2\2"+
		">@\5\20\t\2?=\3\2\2\2@C\3\2\2\2A?\3\2\2\2AB\3\2\2\2BD\3\2\2\2CA\3\2\2"+
		"\2DE\7\21\2\2E\17\3\2\2\2FG\5\6\4\2GH\7\f\2\2HI\5\6\4\2I\21\3\2\2\2\b"+
		"\23\36(*\66A";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}