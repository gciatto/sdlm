// Generated from C:/Users/giova/IdeaProjects/BitPantarei/NetConfig/dsl\Network.g4 by ANTLR 4.5.1
package it.unibo.bitpantarei.network.dsl;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class NetworkLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.5.1", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		COMMENT=1, NODES=2, ARCS=3, NUMBER=4, WHITESP=5, LABEL=6, DEFAULT=7, DEFAULT_PORT=8, 
		LOCALHOST=9, ARROW=10, SEPARATOR=11, AT=12, AT_PORT=13, L_BRAC=14, R_BRAC=15, 
		DOT=16;
	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	public static final String[] ruleNames = {
		"COMMENT", "NODES", "ARCS", "NUMBER", "WHITESP", "LABEL", "DEFAULT", "DEFAULT_PORT", 
		"LOCALHOST", "ARROW", "SEPARATOR", "AT", "AT_PORT", "L_BRAC", "R_BRAC", 
		"DOT"
	};

	private static final String[] _LITERAL_NAMES = {
		null, null, "'nodes'", "'arcs'", null, null, null, "'default'", "'default port'", 
		"'localhost'", "'->'", null, "'@'", "':'", "'{'", "'}'", "'.'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, "COMMENT", "NODES", "ARCS", "NUMBER", "WHITESP", "LABEL", "DEFAULT", 
		"DEFAULT_PORT", "LOCALHOST", "ARROW", "SEPARATOR", "AT", "AT_PORT", "L_BRAC", 
		"R_BRAC", "DOT"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	public NetworkLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "Network.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	@Override
	public void action(RuleContext _localctx, int ruleIndex, int actionIndex) {
		switch (ruleIndex) {
		case 0:
			COMMENT_action((RuleContext)_localctx, actionIndex);
			break;
		case 4:
			WHITESP_action((RuleContext)_localctx, actionIndex);
			break;
		}
	}
	private void COMMENT_action(RuleContext _localctx, int actionIndex) {
		switch (actionIndex) {
		case 0:
			 skip(); 
			break;
		}
	}
	private void WHITESP_action(RuleContext _localctx, int actionIndex) {
		switch (actionIndex) {
		case 1:
			 skip(); 
			break;
		}
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\2\22\u008d\b\1\4\2"+
		"\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4"+
		"\13\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\3\2\3"+
		"\2\3\2\3\2\7\2(\n\2\f\2\16\2+\13\2\3\2\3\2\3\2\3\2\3\2\3\2\7\2\63\n\2"+
		"\f\2\16\2\66\13\2\3\2\5\29\n\2\3\2\3\2\3\3\3\3\3\3\3\3\3\3\3\3\3\4\3\4"+
		"\3\4\3\4\3\4\3\5\3\5\3\5\7\5K\n\5\f\5\16\5N\13\5\5\5P\n\5\3\6\6\6S\n\6"+
		"\r\6\16\6T\3\6\3\6\3\7\3\7\7\7[\n\7\f\7\16\7^\13\7\3\b\3\b\3\b\3\b\3\b"+
		"\3\b\3\b\3\b\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\n\3"+
		"\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\13\3\13\3\13\3\f\3\f\3\r\3\r\3\16"+
		"\3\16\3\17\3\17\3\20\3\20\3\21\3\21\2\2\22\3\3\5\4\7\5\t\6\13\7\r\b\17"+
		"\t\21\n\23\13\25\f\27\r\31\16\33\17\35\20\37\21!\22\3\2\5\5\2\13\f\17"+
		"\17\"\"\6\2\62;C\\aac|\4\2\f\f..\u0093\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2"+
		"\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2"+
		"\23\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33\3\2\2\2\2\35\3"+
		"\2\2\2\2\37\3\2\2\2\2!\3\2\2\2\38\3\2\2\2\5<\3\2\2\2\7B\3\2\2\2\tO\3\2"+
		"\2\2\13R\3\2\2\2\rX\3\2\2\2\17_\3\2\2\2\21g\3\2\2\2\23t\3\2\2\2\25~\3"+
		"\2\2\2\27\u0081\3\2\2\2\31\u0083\3\2\2\2\33\u0085\3\2\2\2\35\u0087\3\2"+
		"\2\2\37\u0089\3\2\2\2!\u008b\3\2\2\2#$\7\61\2\2$%\7,\2\2%)\3\2\2\2&(\13"+
		"\2\2\2\'&\3\2\2\2(+\3\2\2\2)\'\3\2\2\2)*\3\2\2\2*,\3\2\2\2+)\3\2\2\2,"+
		"-\7,\2\2-9\7\61\2\2./\7\61\2\2/\60\7\61\2\2\60\64\3\2\2\2\61\63\13\2\2"+
		"\2\62\61\3\2\2\2\63\66\3\2\2\2\64\62\3\2\2\2\64\65\3\2\2\2\65\67\3\2\2"+
		"\2\66\64\3\2\2\2\679\7\f\2\28#\3\2\2\28.\3\2\2\29:\3\2\2\2:;\b\2\2\2;"+
		"\4\3\2\2\2<=\7p\2\2=>\7q\2\2>?\7f\2\2?@\7g\2\2@A\7u\2\2A\6\3\2\2\2BC\7"+
		"c\2\2CD\7t\2\2DE\7e\2\2EF\7u\2\2F\b\3\2\2\2GP\7\62\2\2HL\4\63;\2IK\4\62"+
		";\2JI\3\2\2\2KN\3\2\2\2LJ\3\2\2\2LM\3\2\2\2MP\3\2\2\2NL\3\2\2\2OG\3\2"+
		"\2\2OH\3\2\2\2P\n\3\2\2\2QS\t\2\2\2RQ\3\2\2\2ST\3\2\2\2TR\3\2\2\2TU\3"+
		"\2\2\2UV\3\2\2\2VW\b\6\3\2W\f\3\2\2\2X\\\4c|\2Y[\t\3\2\2ZY\3\2\2\2[^\3"+
		"\2\2\2\\Z\3\2\2\2\\]\3\2\2\2]\16\3\2\2\2^\\\3\2\2\2_`\7f\2\2`a\7g\2\2"+
		"ab\7h\2\2bc\7c\2\2cd\7w\2\2de\7n\2\2ef\7v\2\2f\20\3\2\2\2gh\7f\2\2hi\7"+
		"g\2\2ij\7h\2\2jk\7c\2\2kl\7w\2\2lm\7n\2\2mn\7v\2\2no\7\"\2\2op\7r\2\2"+
		"pq\7q\2\2qr\7t\2\2rs\7v\2\2s\22\3\2\2\2tu\7n\2\2uv\7q\2\2vw\7e\2\2wx\7"+
		"c\2\2xy\7n\2\2yz\7j\2\2z{\7q\2\2{|\7u\2\2|}\7v\2\2}\24\3\2\2\2~\177\7"+
		"/\2\2\177\u0080\7@\2\2\u0080\26\3\2\2\2\u0081\u0082\t\4\2\2\u0082\30\3"+
		"\2\2\2\u0083\u0084\7B\2\2\u0084\32\3\2\2\2\u0085\u0086\7<\2\2\u0086\34"+
		"\3\2\2\2\u0087\u0088\7}\2\2\u0088\36\3\2\2\2\u0089\u008a\7\177\2\2\u008a"+
		" \3\2\2\2\u008b\u008c\7\60\2\2\u008c\"\3\2\2\2\n\2)\648LOT\\\4\3\2\2\3"+
		"\6\3";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}