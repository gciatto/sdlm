// Generated from C:/Users/giova/IdeaProjects/BitPantarei/NetConfig/dsl\Network.g4 by ANTLR 4.5.1
package it.unibo.bitpantarei.network.dsl;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link NetworkParser}.
 */
public interface NetworkListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link NetworkParser#network}.
	 * @param ctx the parse tree
	 */
	void enterNetwork(NetworkParser.NetworkContext ctx);
	/**
	 * Exit a parse tree produced by {@link NetworkParser#network}.
	 * @param ctx the parse tree
	 */
	void exitNetwork(NetworkParser.NetworkContext ctx);
	/**
	 * Enter a parse tree produced by {@link NetworkParser#nodes}.
	 * @param ctx the parse tree
	 */
	void enterNodes(NetworkParser.NodesContext ctx);
	/**
	 * Exit a parse tree produced by {@link NetworkParser#nodes}.
	 * @param ctx the parse tree
	 */
	void exitNodes(NetworkParser.NodesContext ctx);
	/**
	 * Enter a parse tree produced by {@link NetworkParser#node}.
	 * @param ctx the parse tree
	 */
	void enterNode(NetworkParser.NodeContext ctx);
	/**
	 * Exit a parse tree produced by {@link NetworkParser#node}.
	 * @param ctx the parse tree
	 */
	void exitNode(NetworkParser.NodeContext ctx);
	/**
	 * Enter a parse tree produced by {@link NetworkParser#name}.
	 * @param ctx the parse tree
	 */
	void enterName(NetworkParser.NameContext ctx);
	/**
	 * Exit a parse tree produced by {@link NetworkParser#name}.
	 * @param ctx the parse tree
	 */
	void exitName(NetworkParser.NameContext ctx);
	/**
	 * Enter a parse tree produced by {@link NetworkParser#address}.
	 * @param ctx the parse tree
	 */
	void enterAddress(NetworkParser.AddressContext ctx);
	/**
	 * Exit a parse tree produced by {@link NetworkParser#address}.
	 * @param ctx the parse tree
	 */
	void exitAddress(NetworkParser.AddressContext ctx);
	/**
	 * Enter a parse tree produced by {@link NetworkParser#port}.
	 * @param ctx the parse tree
	 */
	void enterPort(NetworkParser.PortContext ctx);
	/**
	 * Exit a parse tree produced by {@link NetworkParser#port}.
	 * @param ctx the parse tree
	 */
	void exitPort(NetworkParser.PortContext ctx);
	/**
	 * Enter a parse tree produced by {@link NetworkParser#arcs}.
	 * @param ctx the parse tree
	 */
	void enterArcs(NetworkParser.ArcsContext ctx);
	/**
	 * Exit a parse tree produced by {@link NetworkParser#arcs}.
	 * @param ctx the parse tree
	 */
	void exitArcs(NetworkParser.ArcsContext ctx);
	/**
	 * Enter a parse tree produced by {@link NetworkParser#arc}.
	 * @param ctx the parse tree
	 */
	void enterArc(NetworkParser.ArcContext ctx);
	/**
	 * Exit a parse tree produced by {@link NetworkParser#arc}.
	 * @param ctx the parse tree
	 */
	void exitArc(NetworkParser.ArcContext ctx);
}