grammar Network;

network : nodes? arcs ;
nodes : NODES L_BRAC node (SEPARATOR node)* R_BRAC ;
node : name (AT address (AT_PORT port)?)? ;
name : DEFAULT | LABEL;
address : LOCALHOST | (NUMBER DOT NUMBER DOT NUMBER DOT NUMBER);
port : DEFAULT_PORT | NUMBER;
arcs : ARCS L_BRAC arc (SEPARATOR arc)* R_BRAC;
arc : node ARROW node;

COMMENT  : (('/*' (.)* '*/') | ('//' (.)* '\n')) { skip(); } ;
NODES : 'nodes';
ARCS : 'arcs';
NUMBER : '0' | (('1'..'9')('0'..'9')*) ;
WHITESP : ( '\t' | ' ' | '\r' | '\n')+ { skip(); } ;
LABEL : ('a'..'z')('a'..'z' | 'A'..'Z' | '0'..'9'|'_')* ;
DEFAULT : 'default';
DEFAULT_PORT : 'default port';
LOCALHOST : 'localhost';
ARROW : '->';
SEPARATOR : ',' | '\n' ;
AT : '@';
AT_PORT : ':';
L_BRAC : '{';
R_BRAC : '}';
DOT : '.';