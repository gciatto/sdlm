\contentsline {section}{\numberline {1}ACO in MOK}{3}
\contentsline {subsection}{\numberline {1.1}Analisi e idea principale}{3}
\contentsline {subsection}{\numberline {1.2}Cosa guida la propagazione?}{3}
\contentsline {section}{\numberline {2}Progetto}{3}
\contentsline {subsection}{\numberline {2.1}Nodi vicini}{3}
\contentsline {subsubsection}{\numberline {2.1.1}Discovery di nodi vicini}{3}
\contentsline {subsection}{\numberline {2.2}Feromone}{3}
\contentsline {subsubsection}{\numberline {2.2.1}Evaporazione}{3}
\contentsline {section}{\numberline {3}Implementazione}{3}
\contentsline {subsection}{\numberline {3.1}Nodi vicini}{3}
\contentsline {subsubsection}{\numberline {3.1.1}Discovery di nodi vicini}{4}
\contentsline {subsection}{\numberline {3.2}Spreading example}{4}
\contentsline {subsection}{\numberline {3.3}Evaporation example}{5}
\contentsline {section}{Glossary}{6}
