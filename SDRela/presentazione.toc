\contentsline {section}{\numberline {1}Centro di tuple: definizione}{3}
\contentsline {section}{\numberline {2}Vicinanza: semantica}{4}
\contentsline {section}{\numberline {3}Scenario}{4}
\contentsline {section}{\numberline {4}Driver del progetto}{5}
\contentsline {section}{\numberline {5}Discovery}{5}
\contentsline {subsection}{\numberline {5.1}Scenario 1}{5}
\contentsline {subsection}{\numberline {5.2}Scenario 2}{6}
\contentsline {section}{\numberline {6}Remote discovery}{7}
\contentsline {subsection}{\numberline {6.1}REST}{7}
\contentsline {subsection}{\numberline {6.2}Perch\`e REST?}{7}
\contentsline {subsection}{\numberline {6.3}Document database}{8}
\contentsline {subsection}{\numberline {6.4}MongoDB}{9}
\contentsline {subsubsection}{\numberline {6.4.1}Vantaggi}{9}
\contentsline {subsubsection}{\numberline {6.4.2}Svantaggi}{10}
\contentsline {subsubsection}{\numberline {6.4.3}Perch\`e MongoDB?}{10}
\contentsline {subsection}{\numberline {6.5}NODEJS}{10}
\contentsline {subsubsection}{\numberline {6.5.1}Why NODEJS?}{10}
\contentsline {paragraph}{JAVA vs Node.js}{11}
\contentsline {section}{\numberline {7}Local and remote addresses}{12}
\contentsline {section}{\numberline {8}Deployment del progetto}{13}
\contentsline {section}{\numberline {9}Debug}{14}
\contentsline {section}{\numberline {10}Avvio del sistema}{15}
\contentsline {section}{\numberline {11}Problemi}{15}
\contentsline {section}{\numberline {12}Sviluppi futuri}{15}
\contentsline {section}{\numberline {13}Assunzioni del sistema}{15}
