#! /bin/sh
if [ $# -eq 0 ]
      then java -cp it.unibo.sdmagistrale/lib/2p.jar:it.unibo.sdmagistrale/lib/tucson.jar alice.tucson.service.tools.CommandLineInterpreter -portno 20504
      else java -cp it.unibo.sdmagistrale/lib/2p.jar:it.unibo.sdmagistrale/lib/tucson.jar alice.tucson.service.tools.CommandLineInterpreter -portno $1
fi

