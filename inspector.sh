#! /bin/sh
if [ $# -eq 0 ]
      then java -cp it.unibo.sdmagistrale/lib/2p.jar:it.unibo.sdmagistrale/lib/tucson.jar alice.tucson.introspection.tools.InspectorGUI -portno 20504
      else java -cp it.unibo.sdmagistrale/lib/2p.jar:it.unibo.sdmagistrale/lib/tucson.jar alice.tucson.introspection.tools.InspectorGUI -portno $1
fi

