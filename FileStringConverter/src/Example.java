import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by giova on 25/09/2015.
 */
public class Example {
    public static void main(String... args) {
        final File fi = new File(args[0]);
        final File fo = new File(args[1]);
        try {
            final InputStream r = new FileInputStream(fi);
            final OutputStream w = new FileOutputStream(fo);
            final byte[] buff = new byte[1024];
            System.out.println("Start");
            for (int i = 0, len; (len = r.read(buff)) >= 0; i++) {
                final String s = byteArrayToHex(buff, 0, len);
                w.write(hexToByteArray(s));
                System.out.printf("%d:\t<<%s>>\n\n", i, s);
            }
            r.close();
            w.close();
            System.out.println("End");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String byteArrayToHex(byte[] a, int from, int count) {
        count += from;
        final StringBuilder sb = new StringBuilder(a.length * 2);
        for(int i = from; i < count; i++) {
            sb.append(String.format("%02x", a[i] & 0xff));
        }
        return sb.toString();
    }
    public static byte[] hexToByteArray(String s) {
        final byte[] b = new byte[s.length() / 2];
        for (int i = 0; i < s.length(); i += 2) {
            b[i / 2] = hexToByte(s.charAt(i), s.charAt(i + 1));
        }
        return b;
    }
    public static byte hexToByte(char[] s, int at) {
        return hexToByte(s[at], s[at + 1]);
    }

    public static byte hexToByte(char c1, char c2) {
        return (byte) ((hexCharToByte(c1) << 4) + hexCharToByte(c2));
    }


    public static int hexCharToByte(char s) {
        if (s >= '0' && s <= '9') {
            return s - '0';
        } if (s >= 'a' && s <= 'f') {
            return s - 'a' + 10;
        } else if (s >= 'A' && s <= 'F') {
            return s - 'A' + 10;
        } else {
            throw new IllegalArgumentException(String.format("Illegal char: '%c'", s));
        }
    }
}
