var express = require('express'),
    tuplecenter = require('./routes/tuplecenters');

var app = express();

app.configure(function () {
    app.use(express.logger('dev'));
    /* 'default', 'short', 'tiny', 'dev' */
    app.use(express.bodyParser());
});

app.get('/tuplecenters', tuplecenter.findAll);
app.get('/tuplecenters/:id/:topic/:location', tuplecenter.search);
app.post('/tuplecenters/:id', tuplecenter.addTupleCenter);
app.delete('/tuplecenters/', tuplecenter.deleteAllTupleCenter);
app.delete('/tuplecenters/:time', tuplecenter.deleteOldTupleCenter);

app.listen(3000);
console.log('Listening on port 3000...');